"""
lattice_f generates different lattice in Fourier space
"""

import numpy as np
from matplotlib import path
import matplotlib.pyplot as plt
import lattice_r as lr


def generate_bz_error(a, step):
    """
    Generates a discrete Brillouin zone (BZ) for a hexagonal lattice.

    Parameters:
    a (float): the lattice constant
    step (float): the step size between lattice points

    Returns:
    np.ndarray: an array of lattice points inside the BZ
    np.ndarray: an array of lattice points on the border of the BZ,
    they have to be excluded when computing chern number
    np.ndarray: unitary vector.
    """


    e = 1e-2
    ax = 4 * np.pi / (3 * np.sqrt(3) * a) + e
    bx = 2 * np.pi / (3 * np.sqrt(3) * a) + e
    by = 2 * np.pi / (3 * a) + e * np.sqrt(3)
    ux = step * ax
    uy = np.sqrt(3) * step * ax
    Dk = np.array([ux, uy])

    p = path.Path(
        [
            (ax, 0),
            (bx, by),
            (-bx, by),
            (-ax, 0),
            (-bx, -by),
            (bx, -by),
            (ax, 0),
        ]
    )

    step = ax * step

    x = np.arange(-200, 200) * step
    y = np.arange(-200, 200) * np.sqrt(3) * step

    grid = [(xi, yi) for xi in x for yi in y if p.contains_points([(xi, yi)])]
    xp, _ = np.array(grid).T

    ax = ax - 2 * e
    bx = bx - 2 * e
    by = by - 2 * np.sqrt(3) * e

    p = path.Path([(ax, 0), (bx, by), (-bx, by), (-ax, 0), (ax, 0)])

    grid2 = np.array(
        [
            g
            for g in grid
            if not p.contains_points([g])
            and (g[1] > 0 or g[0] == max(xp) or g[0] == min(xp))
        ]
    )
    grid3 = np.array(
        [
            g
            for g in grid
            if p.contains_points([g])
            or (g[1] <= 0 and g[0] != max(xp) and g[0] != min(xp))
        ]
    )


    return (
        np.array(grid3) + (1e-3, 1e-3),
        np.array(grid2),
        Dk
    )


def generate_bz(a, step):
    N = 900
    ax = 4 * np.pi / (3 * np.sqrt(3) * a)
    bx = 2 * np.pi / (3 * np.sqrt(3) * a)
    by = 2 * np.pi / (3 * a)

    ux = step * ax      
    uy = np.sqrt(3) * step * ax    
    Dk = np.array([ux, uy])

    p = path.Path(
        [
            (ax + 0.01, 0),
            (bx + 0.01, by + 0.01),
            (-bx - 0.01, by + 0.01),
            (-ax - 0.01, 0),
            (-bx - 0.01, -by - 0.01),
            (bx + 0.01, -by - 0.01),
            (ax + 0.01, 0),
        ]
    )
    step = ax * step

    x = np.linspace(-20, 20, N)
    y = np.linspace(-20, 20, N)
    x = np.arange(-200, 200) * step
    y = np.arange(-200, 200) * np.sqrt(3) * step

    grid = []
    xp, yp = [], []

    for xi in x:
        for yi in y:
            if p.contains_points([(xi, yi)]):
                xp.append(xi)
                yp.append(yi)
                grid.append((xi, yi))

    e = 0.01
    ax = 4 * np.pi / (3 * np.sqrt(3) * a) - e
    bx = 2 * np.pi / (3 * np.sqrt(3) * a) - e
    by = 2 * np.pi / (3 * a) - 1.7 * e

    p = path.Path([(ax, 0), (bx, by), (-bx, by), (-ax, 0), (ax, 0)])

    maxxp = max(xp)
    maxyp = max(yp)
    grid2 = []
    grid3 = []
    for g in grid:
        if not (p.contains_points([(g[0], g[1])])) and (
            g[1] > 0 or g[0] == max(xp) or g[0] == min(xp)
        ):
            grid2.append(g)
        else:
            grid3.append(g)

    return np.array(grid3), np.array(grid2),Dk

def generate_bz_6cell(a, step):
    """
    Generates a discrete Brillouin zone (BZ) for a hexagonal lattice
    where the fundamental cell is made of an hexagon

    Parameters:
    a (float): the lattice constant
    step (float): the step size between lattice points

    Returns:
    np.ndarray: an array of lattice points inside the BZ
    np.ndarray: an array of lattice points on the border of the BZ,
    they have to be excluded when computing chern number
    """
    Sigma_tilde = np.array([0, -np.tan(np.pi/6)]) * 4 * np.pi / (3 * np.sqrt(3)) / a
    e = 0.01
    aa = abs(Sigma_tilde[1])

    ax = aa + e
    ay = 0
    bx = aa / 2 + e
    by = np.sqrt(3) / 2 * aa + e

    p = path.Path(
        [(ay, ax), (-by, bx), (-by, -bx), (ay, -ax), (by, -bx), (by, bx), (ay, ax)]
    )

    step = aa * step

    x = np.arange(-200, 200) * step * np.sqrt(3)
    y = np.arange(-200, 200) * step

    grid = [(xi, yi) for xi in x for yi in y if p.contains_points([(xi, yi)])]
    _, yp = np.array(grid).T

    ax = ax - e
    bx = bx - e
    by = by - e

    ax, ay = -ay, ax
    bx, by = -by, bx

    ax = aa - e
    ay = 0
    bx = aa / 2 - e
    by = np.sqrt(3) / 2 * aa - e

    p = path.Path(
        [(ay, ax), (-by, bx), (-by, -bx), (ay, -ax), (by, -bx), (by, bx), (ay, ax)]
    )

    grid2, grid3 = [], []
    for g in grid:
        if not (p.contains_points([(g[0], g[1])])) and (
            g[0] > 0 or g[1] == max(yp) or g[1] == min(yp)
        ):
            grid2.append(g)
        else:
            grid3.append(g)

    return (
        np.array(grid3)  + (1e-4, 1e-4),
        np.array(grid2),
        np.array([step*np.sqrt(3),step]),
    )


def get_px_py(no):
    """Return the (px, py) coordinates corresponding to the given position label."""
    px_py_dict = {"NE": (1, 1), "NW": (-1, 1), "SE": (1, -1), "SW": (-1, -1)}
    return px_py_dict.get(no, (-1, -1))

def generate_path_bz(a, n_dots, no="NE"):
    """
    Generates a significant path in Brillouin zone (BZ) for a hexagonal lattice.

    Parameters:
    a (float): the lattice constant
    n_dots (int): the number of dots required in the path
    no (str): which quadrant: NE, NW, SE, SW

    Returns:
    np.ndarray: an array of points following a path in the BZ
    """
    px, py = get_px_py(no)
    M = [px * np.pi / (np.sqrt(3) * a), py * np.pi / (3 * a)]
    Γ = [1e-3, 1e-3]
    K = [px * 4 * np.pi / (3 * np.sqrt(3) * a), 1e-3]

    kx1 = np.linspace(M[0], Γ[0], n_dots // 3)
    ky1 = np.linspace(M[1], Γ[1], n_dots // 3)
    kx2 = np.linspace(Γ[0], K[0], n_dots // 3)
    ky2 = np.linspace(Γ[1], K[1], n_dots // 3)
    kx3 = np.linspace(K[0], M[0], n_dots // 3)
    ky3 = np.linspace(K[1], M[1], n_dots // 3)

    kx = np.concatenate([kx1, kx2, kx3])
    ky = np.concatenate([ky1, ky2, ky3])

    return np.column_stack((kx, ky))

def generate_path_bz_2(a, path, n_dots, no="NE"):
    px, py = get_px_py(no)
    n_dots_on_segment = n_dots//len(path)
    M = (px * np.pi / (np.sqrt(3) * a), py * np.pi / (3 * a))
    Γ = (1e-3, 1e-3)
    K = (px * 4 * np.pi / (3 * np.sqrt(3) * a), 1e-3)
    K_prime = (px*2*np.pi/(3*np.sqrt(3)*a), 2*np.pi/(3*a))

    points = {
        "M":M,
        "Γ":Γ,
        "K":K,
        "K'":K_prime
    }

    kx_l,ky_l = [],[]
    
    for i in range(len(path)-1):
        kx_segment = np.linspace(points[path[i]][0], points[path[i+1]][0], n_dots_on_segment)
        ky_segment = np.linspace(points[path[i]][1], points[path[i+1]][1], n_dots_on_segment)
        kx_l.append(kx_segment)
        ky_l.append(ky_segment)
    
    kx = np.concatenate(kx_l)
    ky = np.concatenate(ky_l)
    
    return np.column_stack((kx, ky))
    

def generate_path_bz_graphene(a, nb_dots, no="NE"):
    """
    Generates a significant path in Brillouin zone (BZ) for a hexagonal lattice.

    Parameters:
    a (float): the lattice constant
    nb_dots (int): the number of dots required in the path
    no (str): which quadrant: NE, NW, SE, SW

    Returns:
    np.ndarray: an array of points following a path in the BZ
    """
    px, py = get_px_py(no)
    M = [px * np.pi / (np.sqrt(3) * a), py * np.pi / (3 * a)]
    Γ = [1e-3, 1e-3]
    K = [px * 4 * np.pi / (3 * np.sqrt(3) * a), 1e-3]

    kx1 = np.linspace(K[0], Γ[0], nb_dots // 3)
    ky1 = np.linspace(K[1], Γ[1], nb_dots // 3)
    kx2 = np.linspace(Γ[0], M[0], nb_dots // 3)
    ky2 = np.linspace(Γ[1], M[1], nb_dots // 3)
    kx3 = np.linspace(M[0], K[0], nb_dots // 3)
    ky3 = np.linspace(M[1], K[1], nb_dots // 3)

    kx = np.concatenate([kx1, kx2, kx3])
    ky = np.concatenate([ky1, ky2, ky3])

    return np.column_stack((kx, ky))


def generate_bz_tb(a, nb_dots):
    """
    Generates a significant path in Brillouin zone (BZ)
    for a hexagonal lattice and a tight-binding model.

    Parameters:
    a (float): the lattice constant
    nb_dots (int): the number of dots required in the path

    Returns:
    np.ndarray: an array of points following a path in the BZ
    """
    Γ = np.array([1e-4, 1e-4])
    K = np.array([4 * np.pi / 3 / np.sqrt(3) / a, 0])
    M = np.array([np.pi / np.sqrt(3) / a, np.pi / 3 / a])
    kx1 = np.linspace(K[0], Γ[0], nb_dots // 2)
    ky1 = np.linspace(K[1], Γ[1], nb_dots // 2)

    kx2 = np.linspace(Γ[1], M[0], nb_dots // 2)
    ky2 = np.linspace(Γ[1], M[1], nb_dots // 2)

    kx = np.concatenate([kx1, kx2])
    ky = np.concatenate([ky1, ky2])

    return np.stack((kx, ky), axis=1)


def generate_path_bz_6cell(a, nb_dots):
    """
    Generates a significant path in Brillouin zone (BZ) for a hexagonal lattice
    where the fundamental cell is made of hexagons.
    Parameters:
    a (float): the lattice constant
    nb_dots (int): the number of dots required in the path

    Returns:
    np.ndarray: an array of points following a path in the BZ
    """
    Sigma_tilde = np.array([0, -0.58]) * 4 * np.pi / (3 * np.sqrt(3)) / a
    Gamma = np.array([0, 0]) * 4 * np.pi / (3 * np.sqrt(3)) / a
    T_tilde = np.array([0.25, -0.43]) * 4 * np.pi / (3 * np.sqrt(3)) / a

    res_sub_bands = nb_dots // 2
    kx1 = np.linspace(Sigma_tilde[0], Gamma[0], res_sub_bands)
    ky1 = np.linspace(Sigma_tilde[1], Gamma[1], res_sub_bands)

    kx2 = np.linspace(Gamma[0], T_tilde[0], res_sub_bands)
    ky2 = np.linspace(Gamma[1], T_tilde[1], res_sub_bands)

    kx = np.concatenate([kx1, kx2])
    ky = np.concatenate([ky1, ky2])

    return np.column_stack((kx, ky))


def generate_path_bz_folded(nb_dots):
    """This function generates a path in the Brillouin zone of the
    2-cell hexagonal lattice, which can be used to recover the
    corresponding path in the 6-cell hexagonal lattice that has been
    folded 3 times.

    Parameters:
    nb_dots (int): the number of dots required in the path

    Returns:
    np.ndarray: an array of points following a path in the BZ

    """
    K_tilde = np.array([0.5, -np.sqrt(3) / 2]) * 4 * np.pi / (3 * np.sqrt(3))
    Sigma_tilde = np.array([0, -0.58]) * 4 * np.pi / (3 * np.sqrt(3))
    Gamma = np.array([0, 0]) * 4 * np.pi / (3 * np.sqrt(3))
    T_tilde = np.array([0.25, -0.43]) * 4 * np.pi / (3 * np.sqrt(3))
    M_tilde = np.array([0, -0.87]) * 4 * np.pi / (3 * np.sqrt(3))

    nb_dots = nb_dots // 5
    kx1 = np.linspace(K_tilde[0], Sigma_tilde[0], nb_dots)
    ky1 = np.linspace(K_tilde[1], Sigma_tilde[1], nb_dots)

    kx2 = np.linspace(Sigma_tilde[0], Gamma[0], nb_dots)
    ky2 = np.linspace(Sigma_tilde[1], Gamma[1], nb_dots)

    kx3 = np.linspace(Gamma[0], T_tilde[0], nb_dots)
    ky3 = np.linspace(Gamma[1], T_tilde[1], nb_dots)

    kx4 = np.linspace(T_tilde[0], K_tilde[0], nb_dots)
    ky4 = np.linspace(T_tilde[1], K_tilde[1], nb_dots)

    kx5 = np.linspace(K_tilde[0], M_tilde[0], nb_dots)
    ky5 = np.linspace(K_tilde[1], M_tilde[1], nb_dots)

    kx = np.concatenate([kx1, kx2, kx3, kx4, kx5])
    ky = np.concatenate([ky1, ky2, ky3, ky4, ky5])

    return np.column_stack((kx, ky))


if __name__ == "__main__":
    a_test = 2
    step_test = 0.05
    nb_dots_test = 100
    bz, border, _ = generate_bz(a_test, step_test)
    lr.display_lattice(bz)
    lr.display_lattice(border, "red")
    plt.show()
    bz, border = generate_bz_6cell(a_test, step_test)
    lr.display_lattice(bz)
    lr.display_lattice(border, "red")
    plt.show()
    path_bz = generate_path_bz(a_test, nb_dots_test, "NE")
    lr.display_lattice(path_bz)
    plt.show()
    path_bz = generate_path_bz_6cell(a_test, nb_dots_test)
    lr.display_lattice(path_bz)
    plt.show()
    path_bz = generate_path_bz_folded(nb_dots_test)
    lr.display_lattice(path_bz)
    plt.show()
