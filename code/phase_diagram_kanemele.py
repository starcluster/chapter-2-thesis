import matplotlib.pyplot as plt
import numpy as np
import csv

import bott
import lattice_r as lr
import lattice_f as lf
import haldane_real as hr
import tex
from utils import cmap_colorblind
from haldane import Haldane
import kanemele as km
import chern
import spin_bott as sb

tex.useTex()



def phase_diagram_spin_bott(N_cluster_side, a, t1):
    lattice = lr.generate_hex_grid_stretch(N_cluster_side=N_cluster_side,a=a)
    lattice_x2 = np.concatenate((lattice, lattice))
    Ms = np.linspace(-10,10,60)
    t2s = np.linspace(-2.,2,60)
    x = []
    y = []
    bs = []
    for M in Ms:
        for t2 in t2s:
            H = minimal_H_km(lattice, a, t1, t2*1j, M)
            eigenvalues, eigenvectors_r = np.linalg.eigh(H)
            els, evl , evr = sb.sort_vectors_and_listify(eigenvalues, eigenvectors_r, eigenvectors_r)
            threshold_psp = -0.1
            threshold_energy = -0.1

            c_sb = sb.spin_bott(lattice_x2, evl, evr, els, threshold_psp, threshold_energy, H.shape[0])
            x.append(M)
            y.append(t2)
            bs.append(c_sb)
            print(f"{M=},{t2=},{c_sb=}")

    with open(f"../data/phase_diagram_kanemele_spin_bott_{N_cluster_side}.csv", mode="w", newline="") as csvf:
        writer = csv.writer(csvf)

        for k,l,m in zip(x,y,bs):
            writer.writerow([k,l,m])

def plot_phase_diagram_spin_bott(N_cluster_side):
    data = np.genfromtxt(f"../data/phase_diagram_kanemele_spin_bott_{N_cluster_side}.csv", delimiter=',', dtype=float, encoding=None)

    M = data[:, 0]
    t2 = data[:, 1]
    values = data[:, 2]
    
    x_unique = np.sort(np.unique(M))
    y_unique = np.sort(np.unique(t2))
    X, Y = np.meshgrid(x_unique, y_unique)

    # Créer une grille pour les valeurs z
    Z = np.zeros_like(X)
    for i in range(len(M)):
        row, col = np.where((X == M[i]) & (Y == t2[i]))
        Z[row, col] = values[i]

    # Créer la heatmap

    plt.pcolormesh(X, Y, Z, shading='auto', cmap=cmap_colorblind)
    # plt.colorbar(label='Valeurs de z')
    plt.axis((-9, 9, -1.75, 1.75))
    # plt.plot([-10,10],[-10/3/np.sqrt(3),10/3/np.sqrt(3)],color="black",lw=7)
    # plt.plot([-10,10],[10/3/np.sqrt(3),-10/3/np.sqrt(3)],color="black",lw=7)
    plt.xlabel(r"$M$",fontsize=20)
    plt.ylabel(r"$t_2$",fontsize=20)
    plt.title(r"$\textrm{Spin Bott index on Kane-Mele's model}$",fontsize=20)
    plt.xticks(fontsize=20)
    plt.yticks(fontsize=20)
    plt.savefig(f"../figs/phase_diagram_spin_bott_{N_cluster_side}.pdf",format="pdf",bbox_inches='tight')
    plt.show()
            
def phase_diagram_spin_chern(step, a, t1):
    BZ, border, Dk = lf.generate_bz(a, step)
    dim = 4
    no_bands = [0]
    Ms = np.linspace(-10,10,60)
    t2s = np.linspace(-2,2,60)
    x = []
    y = []
    Cs = []
    for M in Ms:
        for t2 in t2s:
            haldane = Haldane(a=a, t1=t1, t2=t2, M=M)
            C = km.spin_chern_km(t1, t2*1j,0, M, BZ , Dk, dim, [0])
            x.append(M)
            y.append(t2)
            Cs.append(C)
            print(M,",",t2,",",C)
            

    with open("../data/phase_diagram_kanemele_spin_chern.csv", mode="w", newline="") as csvf:
        writer = csv.writer(csvf)

        for k,l,m in zip(x,y,Cs):
            writer.writerow([k,l,m])

def plot_phase_diagram_spin_chern():

    data = np.genfromtxt("../data/phase_diagram_kanemele_spin_chern.csv", delimiter=',', dtype=complex, encoding=None)

    M = np.real(data[:, 0])
    t2 = np.real(data[:, 1])
    values = np.real(data[:, 2])

    x_unique = np.sort(np.unique(M))
    y_unique = np.sort(np.unique(t2))
    X, Y = np.meshgrid(x_unique, y_unique)

    # Créer une grille pour les valeurs z
    Z = np.zeros_like(X)
    for i in range(len(M)):
        row, col = np.where((X == M[i]) & (Y == t2[i]))
        Z[row, col] = values[i]

    # Créer la heatmap

    plt.pcolormesh(X, Y, Z, shading='auto', cmap=cmap_colorblind)
    # plt.colorbar(label='Valeurs de z')
    plt.axis((-9, 9, -1.75, 1.75))
    # plt.plot([-10,10],[-10/3/np.sqrt(3),10/3/np.sqrt(3)],color="black",lw=7)
    # plt.plot([-10,10],[10/3/np.sqrt(3),-10/3/np.sqrt(3)],color="black",lw=7)
    plt.xlabel(r"$M$",fontsize=20)
    plt.ylabel(r"$t_2$",fontsize=20)
    plt.title(r"$\textrm{Spin Chern number on Kane-Mele's model}$",fontsize=20)
    plt.xticks(fontsize=20)
    plt.yticks(fontsize=20)
    plt.savefig("../figs/phase_diagram_spin_chern.pdf",format="pdf",bbox_inches='tight')
    plt.show()
    
def minimal_H_km(lattice, a, t1, t2, M):
    # H_haldane_1 = hr.create_hamiltonian_haldane_pbc(lattice, a, t1, t2, M)
    # H_haldane_2 = hr.create_hamiltonian_haldane_pbc(lattice, a, t1, -t2, M)
    H_haldane_1 = hr.create_hamiltonian_haldane(lattice, a, t1, t2, M)
    H_haldane_2 = hr.create_hamiltonian_haldane(lattice, a, t1, -t2, M)
    n = H_haldane_1.shape[0]
    H_km = np.block([[H_haldane_1, np.zeros((n,n))],
                     [np.zeros((n,n)), H_haldane_2]])
    return H_km

if __name__ == "__main__":
    N_cluster_side = 4
    a = 1
    t = 1
    t1 = 1
    step = 0.1
    # phase_diagram_spin_bott(N_cluster_side=N_cluster_side, a=a, t1=t1)
    plot_phase_diagram_spin_bott(N_cluster_side)
    BZ, _, Dk = lf.generate_bz(a, step)
    BZ = BZ + (1e-4, 1e-4)
    dim = 4
    
    # phase_diagram_spin_chern(step, a, t1)
    plot_phase_diagram_spin_chern()
