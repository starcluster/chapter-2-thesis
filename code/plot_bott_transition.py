import matplotlib.pyplot as plt
import numpy as np

import tex

tex.useTex()

if __name__ == "__main__":
    data_6 = np.loadtxt("../data/bott_evolv_6_pbc_False.csv", delimiter=",",dtype=complex)
    data_10 = np.loadtxt("../data/bott_evolv_10_pbc_False.csv", delimiter=",",dtype=complex)
    data_6_pbc = np.loadtxt("../data/bott_evolv_6_pbc_True.csv", delimiter=",",dtype=complex)
    data_chern = np.loadtxt("../data/chern_evolv.csv", delimiter=",",dtype=complex)
    data_chern[:,1] *=(-1j)
    labels = [r"$N=216$",r"$N=600$",r"$N=216\quad\textrm{PBC}$", r"$\textrm{Chern number}$"]
    markers = ["^", "v", ">", "<"]
    for data,color,label,marker in zip([data_6, data_10, data_6_pbc, data_chern],['r','g','b','purple'],labels,markers):
        x = data[:, 0]
        y = data[:, 1]


        plt.plot(x, np.imag(y), label=label, color=color,ls="--",marker=marker)
        # plt.scatter(x, np.imag(y), label=label, color=color,marker=marker)


        plt.xlabel(r"$M$",fontsize=20)
        plt.ylabel(r"$C_B$",fontsize=20)
        plt.xticks(fontsize=20)
        plt.yticks(fontsize=20)
        plt.title(r"$t_2=i$",fontsize=20)


    plt.axvline(x=3*np.sqrt(3),color="black",label=r"$M=3\sqrt{3}$")
    plt.axis((2.5,6,-0.1,1.1))
    plt.legend(fontsize=20)
    plt.savefig("../figs/bott_evolv_haldane.pdf",format="pdf",bbox_inches='tight')
    plt.show()
