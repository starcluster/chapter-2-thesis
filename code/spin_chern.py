import numpy as np
import lattice_f as lf
import lattice_r as lr
import matplotlib.pyplot as plt
import matplotlib
import tex
import chern as ch
from scipy.linalg import eig
from numpy import linalg as LA
from functools import partial
from modele import Modele
from pylatexenc.latex2text import LatexNodes2Text

tex.useTex()


class HoneycombTightBinding(Modele):
    """Describes a tight binding model on a honeycomb lattice such as
    the one in this paper: https://arxiv.org/pdf/1605.08822.pdf"""

    def __init__(self, a, t1, t2):
        super().__init__()
        self.a = a
        self.t1 = t1
        self.t2 = t2

    def compute_ham(self, k):
        self.a1 = np.array([np.sqrt(3), 0]) * self.a
        self.a2 = np.array([np.sqrt(3) / 2, 3 / 2]) * self.a
        self.H1 = np.array(
            [
                [0, 1, 0, 0, 0, 1],
                [1, 0, 1, 0, 0, 0],
                [0, 1, 0, 1, 0, 0],
                [0, 0, 1, 0, 1, 0],
                [0, 0, 0, 1, 0, 1],
                [1, 0, 0, 0, 1, 0],
            ]
        )
        self.H2 = np.array(
            [
                [0, 0, 0, np.exp(1j * np.dot(k, self.a1)), 0, 0],
                [0, 0, 0, 0, np.exp(1j * np.dot(k, self.a2)), 0],
                [0, 0, 0, 0, 0, np.exp(1j * np.dot(k, self.a2 - self.a1))],
                [np.exp(-1j * np.dot(k, self.a1)), 0, 0, 0, 0, 0],
                [0, np.exp(-1j * np.dot(k, self.a2)), 0, 0, 0, 0],
                [0, 0, np.exp(-1j * np.dot(k, self.a2 - self.a1)), 0, 0, 0],
            ]
        )

        self.ham = -self.t1 * self.H1 - self.t2 * self.H2

    def compute_bands(self, path):
        super().compute_bands(path)


def spdf():
    v_s = np.array([1, 1, 1, 1, 1, 1],dtype=complex)
    v_px = np.array([1, 1, 0, -1, -1, 0],dtype=complex)
    v_py = np.array([1, -1, -2, -1, 1, 2],dtype=complex)
    v_dx2y2 = np.array([1, 1, -2, 1, 1, -2],dtype=complex)
    v_dxy = np.array([1, -1, 0, 1, -1, 0],dtype=complex)
    v_f = np.array([1, -1, 1, -1, 1, -1],dtype=complex)

    v_s /= np.linalg.norm(v_s)
    v_px /= np.linalg.norm(v_px)
    v_py /=  np.linalg.norm(v_py)
    v_dx2y2 /=  np.linalg.norm(v_dx2y2)
    v_dxy /=np.linalg.norm(v_dxy)
    v_f /=  np.linalg.norm(v_f)

    v_pp = 1 / np.sqrt(2) * (v_px + 1j * v_py)
    v_pm = 1 / np.sqrt(2) * (v_px - 1j * v_py)

    v_dp = 1 / np.sqrt(2) * (v_dx2y2 + 1j * v_dxy)
    v_dm = 1 / np.sqrt(2) * (v_dx2y2 - 1j * v_dxy)

    basis = [
        v_s,
        v_pp, 
        v_pm,
        v_dp,
        v_dm,
        v_f 
    ]

    return basis

        
def get_sigma():
    basis = spdf()
    S = np.array([basis[i] for i in range(6)]).T
    D = np.diag([0, 1, -1, 1, -1, 0])
    sigma = S @ D @ (LA.inv(S))

    return sigma

def get_sigma_km():
    basis = spdf()
    S = np.array([basis[i] for i in range(6)]).T
    D = np.diag([1, 1, -1, -1]) # ???
    # sigma = S @ D @ (LA.inv(S))

    return D


def make_P(vl,vr,P):
    vi = np.conj(np.expand_dims(vl, axis=1))
    vit = np.transpose(np.expand_dims(vr, axis=1))
    A = np.dot(vit, vi)
    vi = vi / np.sqrt(A)
    vit = vit / np.sqrt(A)
    P += np.dot(vi, vit)
    return P

def get_p_sigma_p(vl, vr, sigma):
    n = vl[0].shape[0]
    Pp = np.zeros((n, n), dtype="complex128")
    Pm = np.zeros((n, n), dtype="complex128")
    for i in range(0,n//2):
        Pp = make_P(vl[i],vr[i],Pp)

    for i in range(n//2,n):
        Pm = make_P(vl[i],vr[i],Pm)

    return Pm @ sigma @ Pm, Pp @ sigma @ Pp


def get_H(k, a, t1, t2):
    htb = HoneycombTightBinding(a, t1, t2)
    htb.compute_ham(k)
    psp = psp_from_H(htb.ham)
    return psp

def psp_from_H(H):
    N = H.shape[0]
    lambdas, vects = np.linalg.eig(H)
    lambdas, vl, vr = eig(H, left=True, right=True)
    omega = [0] * N
    gamma = [0] * N
    vl = [vl[:, i] for i in range(N)]
    vr = [vr[:, i] for i in range(N)]
    for j in range(N):
        gamma[j] = lambdas[j].imag
        omega[j] = -0.5 * lambdas[j].real


    omega = [om + np.random.rand()/100 for om in omega]
    tuples = sorted(zip(omega, vl, vr, gamma))
    omega, vl, vr, gamma = [[t[i] for t in tuples] for i in range(4)]
    
    sigma = get_sigma_km()
    psp = get_p_sigma_p(vl, vr, sigma)
    return psp


def plot_bands_htb(a=1, t1=1, t2=1, res_bands=1000):
    title = f"$a = {a}" + r"\quad" + f"t_1 = {t1}" + r"\quad" + r"t_2 = {t2}$"
    honeycomb_tb = HoneycombTightBinding(a=a, t1=t1, t2=t2)
    path_in_bz = lf.generate_bz_tb(a, res_bands) + (1e-4, 1e-4)

    honeycomb_tb.compute_bands(path_in_bz)
    bands = honeycomb_tb.bands
    pos_xticks = [0, res_bands / 2, res_bands]
    name_xticks = [r"$K$", r"$\Gamma$", r"$M$"]
    plot.plot_bands(path_in_bz, bands, title, pos_xticks, name_xticks)

def plot_psp_on_path(a=1, t1=1, t2=1, res_bands=1000):
    honeycomb_tb = HoneycombTightBinding(a=a, t1=t1, t2=t2)
    path_in_bz = lf.generate_bz_tb(a, res_bands) + (1e-4, 1e-4)
    bands = []
    for k in path_in_bz:
        honeycomb_tb.compute_ham(k)
        H = honeycomb_tb.ham
        psp = psp_from_H(H)
        eigenvalues, _ = LA.eig(psp)
        energy = np.real(eigenvalues)
        gamma = np.imag(eigenvalues)
        id_sorted = energy.argsort()
        energy = energy[id_sorted]
        gamma = gamma[id_sorted]
        bands.append(energy)

    title = r"$\textrm{Band diagram of } P\sigma P \quad" f" t_2 = {t2}$"
    plot.plot_bands(
        path_in_bz,
        bands,
        title,
        [0, res_bands / 2, res_bands],
        [r"$K$", r"$\Gamma$", r"$M$"],
    )


def plot_psp_on_bz(a=1, t1=1, t2=1, res_bands=1000):
    honeycomb_tb = HoneycombTightBinding(a=a, t1=t1, t2=t2)
    bz, _ = lf.generate_bz(a, 0.05)
    bands = []
    for k in bz:
        honeycomb_tb.compute_ham(k)
        H = honeycomb_tb.ham
        psp = psp_from_H(H)
        eigenvalues, _ = LA.eig(psp)
        energy = np.real(eigenvalues)
        bands += energy.tolist()
    bands.sort()

    title = (
        r"$\textrm{Spectrum of } P\sigma P \textrm{ on Brillouin zone - } "
        + f" t_2 = {t2}$"
    )
    plt.scatter(np.linspace(0, 1, len(bands)), bands, color="blue")
    plt.xticks([])
    plt.yticks(fontsize=20)
    plt.title(title, fontsize=20)
    plt.savefig(
        f"{LatexNodes2Text().latex_to_text(title)}.pdf",
        format="pdf",
        bbox_inches="tight",
    )
    plt.show()
    

if __name__ == "__main__":
    a = 1
    res_bands = 1000
    t1 = 1
    t2 = 1.5
    step = 0.1
    BZ, _, Dk = lf.generate_bz(a, step)
    BZ = BZ + (1e-4, 1e-4)
    dim = 6

    print(Dk)

    t2s = np.linspace(0.5, 1.5, 100)
    Cs = []
    for t2 in t2s:
        C = ch.chern(partial(get_H, a=a, t1=t1, t2=t2), BZ , Dk, dim, [0])
        print(C)
        Cs.append(C)
    plt.cla()
    plt.clf()
    plt.scatter(t2s, np.real(Cs), color="blue")
    plt.xlabel(r"$t_2$", fontsize=20)
    plt.ylabel(r"$C_s$", fontsize=20)
    plt.xticks(fontsize=20)
    plt.yticks(fontsize=20)
    plt.title(r"$\textrm{Honeycomb tight-binding}$", fontsize=20)
    plt.savefig(
        "spin_chern_number_tight_binding.pdf", format="pdf", bbox_inches="tight"
    )
