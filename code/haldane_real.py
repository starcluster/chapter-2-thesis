import numpy as np
import matplotlib.pyplot as plt
from pylatexenc.latex2text import LatexNodes2Text
import csv

import lattice_r as lr
import lattice_f as lf
import bott
import tex
import ipr

from sympy import pprint, Matrix

tex.useTex()


def distance(a,b):
    xa,ya = a
    xb,yb = b
    return np.sqrt((xa-xb)**2+(ya-yb)**2)

def pbc(site_a, site_b, a, lx, ly):
    dx = site_a[0]-site_b[0]
    dy = site_a[1]-site_b[1]
    gen = [(1,0),(1,1),(0,1),(-1,1),(-1,0),(-1,-1),(0,-1),(1,-1),(0,0)]
    list_quad = [(dx + c[0]*lx,dy+c[1]*ly) for c in gen]
    return list_quad
def are_nn(site_a, site_b, a):
    return distance(site_a,site_b) <= a+0.1

def are_nn_pbc(site_a, site_b, a, lx, ly):
    list_quad = pbc(site_a, site_b, a, lx, ly)
    list_are_nn = [np.sqrt(dx**2+dy**2) < a +0.1 for dx,dy in list_quad]
    return any(list_are_nn)

def pbc_border_transposed(site, a, lx, ly):
    gen = [(1,0),(1,1),(0,1),(-1,1),(-1,0),(-1,-1),(0,-1),(1,-1),(0,0)]
    list_quad = [(site[0] + c[0]*lx,site[1]+c[1]*ly) for c in gen]
    return list_quad

def are_nnn(site_a,site_b,a):
    dab = distance(site_a,site_b)
    return dab > a*np.sqrt(3)-0.01 and dab < a*np.sqrt(3)+0.01

def are_nnn_pbc_border_transposed(pt, potential_neighbour, a, lx, ly):
    list_quad = pbc_border_transposed(potential_neighbour, a, lx, ly)
    list_are_nn = [distance(pt,pn) > a*np.sqrt(3)-0.1 and distance(pt,pn) < a*np.sqrt(3)+0.1 for pn in list_quad]

    for k1,k2 in zip(list_quad, list_are_nn):
        if k2:
            return True, k1
    
    return False, None
def gen_vect_nnn(a):
    e0 = np.array([-3/2*a,-a*np.sqrt(3)/2])
    e1 = np.array([0,a*np.sqrt(3)])
    em1 = np.array([3/2*a,-a*np.sqrt(3)/2])
    return [e0,e1,em1,-e0,-e1,-em1]
    
def create_hamiltonian_haldane(lattice, a, t1, t2, M):
    N = lattice.shape[0]
    H = np.zeros((N,N),dtype=complex)
    for i in range(N):
        for j in range(N):
            if i==j:
                if i %2 ==0:
                    H[i,j] = -M
                else:
                    H[i,j] = M

            else:
                if are_nnn(lattice[i],lattice[j],a):
                    vect = -(lattice[i] - lattice[j])
                    gvnnn = gen_vect_nnn(a)
                    trigo = any(np.sum(np.abs(e - vect)) < 1e-9 for e in gvnnn[:3])                    
                    if i % 2 ==0:
                        if trigo:
                            H[i,j] = t2
                        else:
                            H[i,j] = np.conj(t2)
                    else:
                        if trigo:
                            H[i,j] = -t2
                        else:
                            H[i,j] = -np.conj(t2)
                            
                elif are_nn(lattice[i],lattice[j], a):
                    H[i,j] = t1
                    
    return H

def create_hamiltonian_haldane_pbc(lattice, a, t1, t2, M):
    N = lattice.shape[0]
    H = np.zeros((N,N),dtype=complex)
    x,y = lattice.T
    lx = np.max(x)-np.min(x)-a/2
    ly = np.max(y)-np.min(y)+a/2*np.sqrt(3)
    for i in range(N):
        for j in range(N):
            if i==j:
                if i%2==0:
                    H[i,j] = -M
                else:
                    H[i,j] = M

            else:
                an,pt = are_nnn_pbc_border_transposed(lattice[i],lattice[j], a, lx, ly)
                if an:
                    vect = -(lattice[i] - lattice[j])
                    gvnnn = gen_vect_nnn(a)
                    if distance(lattice[i],lattice[j]) >= 2:
                        vect = -(lattice[i] - pt)
                        
                    trigo = any(np.sum(np.abs(e - vect)) < 1e-9 for e in gvnnn[:3])                    
                    if i % 2 ==0:
                        if trigo:
                            H[i,j] = t2
                        else:
                            H[i,j] = np.conj(t2)
                    else:
                        if trigo:
                            H[i,j] = -t2
                        else:
                            H[i,j] = -np.conj(t2)

                elif are_nn_pbc(lattice[i],lattice[j], a, lx, ly):
                    H[i,j] = t1
                    
    return H


def sort_vectors(w,vl,vr):
    w = w.real
    idx = w.argsort()
    vl = vl[:,idx]
    vr = vr[:,idx]
    w = w[idx]
    return w,vl,vr

def compute_sum_bott(lattice, a, t1, t2, M, pbc_on):
    if pbc_on:
        H = create_hamiltonian_haldane_pbc(lattice, a, t1, t2, M)
    else:
        H = create_hamiltonian_haldane(lattice, a, t1, t2, M)
    print(H.shape)
    print(np.count_nonzero(H==t2)+ np.count_nonzero(H==np.conj(t2)) +np.count_nonzero(H==-t2)+ np.count_nonzero(H==-np.conj(t2)), 6*H.shape[0])
    print(np.count_nonzero(H==t1), 3*H.shape[0])
    print("sum",np.sum(np.abs(H-np.conj(H.T))))
    print("max",np.max(H-np.conj(H.T)))
    # pprint(Matrix(np.round(H,2)))
    # exit()
    

    eigenvalues, eigenvectors = np.linalg.eigh(H)
    els, evs, _ = sort_vectors(eigenvalues, eigenvectors, eigenvectors)
    print(np.max(H@evs[:,0] - els[0]*evs[:,0]))

    b = bott.all_bott(lattice, evs)
    plt.hist(els,bins=50,facecolor="none", edgecolor='black')
    plt.scatter(els, b, color = "red")
    plt.xlabel(r"$\textrm{Energy} $",fontsize=20)
    plt.ylabel(r"$\textrm{DOS} $",fontsize=20)
    if pbc_on:
        title = r"$\qquad\textrm{DOS and Bott index}$\newline" + f"$t_1 = {t1} - t_2 = {np.round(t2,2)} - M = {M}$"
    else:
        title = r"$\qquad\textrm{DOS and Bott index (NO PBC)}$\newline" + f"$t_1 = {t1} - t_2 = {np.round(t2,2)} - M = {M}$"
    plt.title(title,fontsize=20)
    plt.savefig(f"../figs/{LatexNodes2Text().latex_to_text(title)}.pdf",format="pdf",bbox_inches='tight')
    plt.show()
    # return b

    
def bott_evolution(a, t1, t2, N_cluster_side, pbc_on, M_min, M_max):
    lattice = lr.generate_hex_grid_stretch(N_cluster_side=N_cluster_side,a=a)
    Ms = np.linspace(M_min,M_max,20)
    bs = []
    for M in Ms:
        if pbc_on:
            H = create_hamiltonian_haldane_pbc(lattice, a, t1, t2, M)
        else:
            H = create_hamiltonian_haldane(lattice, a, t1, t2, M)
        eigenvalues, eigenvectors = np.linalg.eig(H)
        els, evs, _ = sort_vectors(eigenvalues, eigenvectors, eigenvectors)
        b = bott.bott(lattice=lattice, eigvec=evs, frequencies=els, omega=0, pol=False, dagger=False, verbose=False)
        bs.append(b)
        print(M,",",b)

    with open(f"../data/bott_evolv_{N_cluster_side}_pbc_{pbc_on}.csv", mode="w", newline="") as csvf:
        writer = csv.writer(csvf)
        for k,l in zip(Ms,bs):
            writer.writerow([k,l])




if __name__ == "__main__":
    N = 40
    a = 1
    t1 = -1
    t2 =  1j # 0.15*np.exp(-1j*np.pi/2)
    M = 0
    N_cluster_side = 10
    
    lattice = lr.generate_hex_grid_stretch(N_cluster_side=N_cluster_side,a=a)
    x,y = lattice.T
    lr.display_lattice(lattice,"black")
    
    plt.savefig("../figs/square_lattice_example.pdf",format="pdf",bbox_inches='tight')
    plt.show()
    exit()
    x_med = np.max(x)/2
    y_med = np.max(y)/2

    # R1 = 10
    # R2 = 15

    # lattice2 = []
    # for z in lattice:
    #     if distance((x_med,y_med),z) > R1 and distance((x_med,y_med),z) < R2:
    #         lattice2.append(z)
    # lattice2 = np.array(lattice2)

    # lr.display_lattice(lattice2)
    # plt.savefig("../figs/lattice_circle.pdf",format="pdf",bbox_inches='tight')
    # plt.show()
    # H = create_hamiltonian_haldane(lattice2, a, t1, t2, M)
    # # H = create_hamiltonian_haldane_pbc(lattice, a, t1, t2, M)
    # eigenvalues, eigenvectors = np.linalg.eig(H)
    # plt.hist(np.real(eigenvalues),50,histtype="step",color="black")
    # plt.savefig("../figs/dos_lattice_circle.pdf",format="pdf",bbox_inches='tight')
    # plt.show()
    # energy = np.real(eigenvalues)
    # idx = np.argsort(energy)
    # energy = energy[idx]
    # eigenvectors = eigenvectors[:,idx]
    # n = lattice2.shape[0]
    # for i in range(40):
    #     print(i)
    #     ipr.intensity_map(eigenvectors, lattice2, n//2+i, energy[n//2+i],True)

    # R1 = 10.5
    # R2 = 15

    # lattice2 = []
    # for z in lattice:
    #     xz = z[0]
    #     yz = z[1]
    #     if np.abs(xz-x_med) > R1 or np.abs(yz-y_med) > R1:
    #         lattice2.append(z)
    # lattice2 = np.array(lattice2)

    # lr.display_lattice(lattice2)
    # plt.savefig("../figs/lattice_hole.pdf",format="pdf",bbox_inches='tight')
    # plt.show()
    lattice2 = lattice
    H = create_hamiltonian_haldane(lattice2, a, t1, t2, M)
    # H = create_hamiltonian_haldane_pbc(lattice, a, t1, t2, M)
    eigenvalues, eigenvectors = np.linalg.eig(H)
    plt.hist(np.real(eigenvalues),50,histtype="step",color="black")
    plt.savefig("../figs/dos_lattice_hole.pdf",format="pdf",bbox_inches='tight')
    plt.show()
    energy = np.real(eigenvalues)
    idx = np.argsort(energy)
    energy = energy[idx]
    eigenvectors = eigenvectors[:,idx]
    n = lattice2.shape[0]
    coeff = np.zeros((n,1))
    no = 304
    coeff[no] = 1
    ipr.intensity_map(eigenvectors, lattice2, n, energy[304],True)
    exit()
    
    # n = lattice2.shape[0]
    # for i in range(40):
    #     print(i)
    #     ipr.intensity_map(eigenvectors, lattice2, n//2+i, energy[n//2+i],True)
    # exit()
    
    # N = lattice.shape[0]
    # lr.display_lattice(lattice[::2], "red")
    # lr.display_lattice(lattice[1::2], "blue")
    # plt.show()
    # exit()

    # plot_bott_evolution()
    # exit()

    # bott_evolution(a=a, t1=t1, t2=t2, N_cluster_side=6, pbc_on=True, M_min=2.6, M_max=6.1)
    # bott_evolution(a=a, t1=t1, t2=t2, N_cluster_side=6, pbc_on=False, M_min=2.4, M_max=5.9)
    # bott_evolution(a=a, t1=t1, t2=t2, N_cluster_side=10, pbc_on=False, M_min=2.3, M_max=5.8)
    # exit()
    
    # H = create_hamiltonian_haldane(lattice, a, t1, t2, M)
    # H = create_hamiltonian_haldane_pbc(lattice, a, t1, t2, M)

    # eigenvalues, eigenvectors = np.linalg.eig(H)
    # els, evs, _ = sort_vectors(eigenvalues, eigenvectors, eigenvectors)
    # print(bott.bott(lattice=lattice, eigvec=evs, frequencies=els, omega=0, pol=False, dagger=False, verbose=False))

    b = compute_sum_bott(lattice, a, t1, t2, M, False)
    # plt.hist(els,bins=40,facecolor="none", edgecolor='black')
    # plt.scatter(els, b, color = "red")
    # plt.show()

    
    # print(compute_sum_bott(lattice, a, t1, t2, M))




