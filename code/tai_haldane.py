import matplotlib.pyplot as plt
import numpy as np
import csv

import bott
import lattice_r as lr
import lattice_f as lf
import haldane_real as hr
import tex
from utils import cmap_colorblind
from haldane import Haldane
import chern
import tai_kanemele as tkm 

tex.useTex()

if __name__ == "__main__":
    N_cluster_side = 8
    a = 1
    t1 = 1
    t2 = np.exp(1j*2*np.pi/3)
    M = 4.5
    step = 0.1
    
    lattice = lr.generate_hex_grid_stretch(N_cluster_side=N_cluster_side,a=a)

    ws = np.linspace(0,1,20)

    # H0 = hr.create_hamiltonian_haldane_pbc(lattice, a, t1, t2, M)
    H0 = hr.create_hamiltonian_haldane(lattice, a, t1, t2*1j, M)
    print(f"{M=},{t2=}")

    I = np.eye(6)
    I1 = tkm.add_anderson_disorder(I, 0.5)

    # print(I1[::2])
    # np.fill_diagonal(I1[0], 0)
    # print(f"{np.diag(np.diagonal(I1))}")
    # exit()

    for w in ws:
        H = tkm.add_anderson_disorder(H0,w,True)

        # print(H[0:6,0:6])

        eigenvalues, eigenvectors = np.linalg.eig(H)
        f = np.real(eigenvalues)

        els, evs, _ = hr.sort_vectors(eigenvalues, eigenvectors, eigenvectors)
        b = bott.bott(lattice=lattice, eigvec=evs, frequencies=els, omega=-0.1, pol=False, dagger=False, verbose=False)
        bs = bott.all_bott(lattice=lattice, eigvec=evs, frequencies=els, pol=False, dagger=False, verbose=False)


        # print(len(f))
        # print(len(bs))

        plt.hist(f, bins=80, histtype="step",color="blue",label="")
        plt.scatter(bs.keys(), bs.values(), color="red")
        plt.xlabel(r"$\omega$",fontsize=20)
        plt.ylabel(r"$\textrm{DOS}$",fontsize=20)
        plt.title(f"$W={np.round(w,3)}$",fontsize=20)
        plt.xticks(fontsize=20)
        plt.yticks(fontsize=20)
        plt.savefig(f"w={np.round(w,3)}.png",format="png",bbox_inches='tight')
        plt.clf()
        plt.cla()
        # plt.show()
    
        print(f"{w=},{b=}")
