import matplotlib.pyplot as plt
import numpy as np
import csv

import bott
import lattice_r as lr
import lattice_f as lf
import haldane_real as hr
import tex
from utils import cmap_colorblind
from haldane import Haldane
import chern
import tai_kanemele as tkm

tex.useTex()

def lattice_18():
    lattice = lr.generate_hex_grid_stretch(N_cluster_side=2,a=a)
    lattice = lattice.tolist()
    lattice = lattice[:6] + lattice[15:16] + lattice[6:9] + lattice[10:12] + lattice[18:] 
    return  np.array(lattice)

def lattice_17():
    lattice = lr.generate_hex_grid_stretch(N_cluster_side=2,a=a)
    lattice = lattice.tolist()
    lattice = lattice[:6] + lattice[15:16] + lattice[6:9] + lattice[10:12] + lattice[19:] 
    return  np.array(lattice)

def lattice_15():
    lattice = lr.generate_hex_grid_stretch(N_cluster_side=2,a=a)
    lattice = lattice.tolist()
    lattice = lattice[:4] + lattice[15:16] + lattice[6:9] + lattice[10:12] + lattice[19:] 
    return  np.array(lattice)

def lattice_13():
    lattice = lr.generate_hex_grid_stretch(N_cluster_side=2,a=a)
    lattice = lattice.tolist()
    lattice = lattice[:4] + lattice[15:16] + lattice[6:8] + lattice[11:12] + lattice[19:] 
    return  np.array(lattice)
    
def phase_diagram_bott(N_cluster_side, a, t1, r=0):
    lattice = lr.generate_hex_grid_stretch(N_cluster_side=N_cluster_side,a=a)
    # lattice = lattice_13()
    # lr.display_lattice(lattice[::2],color="blue")
    # lr.display_lattice(lattice[1::2],color="red")
    # plt.xlabel("",fontsize=20)
    # plt.ylabel("",fontsize=20)
    # plt.title("",fontsize=20)
    # plt.xticks(fontsize=20)
    # plt.yticks(fontsize=20)
    # plt.savefig("../figs/lattice_exp.pdf",format="pdf",bbox_inches='tight')
    # plt.show()
    Ms = np.linspace(-10,10,60)
    # Ms = np.linspace(0,10,60)
    # Ms = np.linspace(0,1,2)
    t2s = np.linspace(-2,2,60)
    # t2s = np.linspace(-200,200,60)
    x = []
    y = []
    bs = []
    for M in Ms:
        for t2 in t2s:
            H = hr.create_hamiltonian_haldane_pbc(lattice, a, t1, t2*1j, M)
            H = tkm.add_anderson_disorder(H,r,False)
            # H = hr.create_hamiltonian_haldane(lattice, a, t1, t2*1j, M)
            eigenvalues, eigenvectors = np.linalg.eig(H)
            els, evs, _ = hr.sort_vectors(eigenvalues, eigenvectors, eigenvectors)
            b = bott.bott(lattice=lattice, eigvec=evs, frequencies=els, omega=0, pol=False, dagger=True, verbose=False)
            x.append(M)
            y.append(t2)
            bs.append(b)
            print(M,t2,b)

    with open(f"../data/phase_diagram_haldane_bott_{N_cluster_side}_{r}.csv", mode="w", newline="") as csvf:
        writer = csv.writer(csvf)

        for k,l,m in zip(x,y,bs):
            writer.writerow([k,l,m])

def plot_phase_diagram_bott(N_cluster_side, dark=False, r=0.):
    N_sites = N_cluster_side**2*6
    if dark:
        plt.style.use('dark_background')
    try:
        data = np.genfromtxt(f"../data/phase_diagram_haldane_bott_{N_cluster_side}_{r}.csv", delimiter=',', dtype=float, encoding=None)
    except:
        data = np.genfromtxt(f"../data/phase_diagram_haldane_bott_{N_cluster_side}.csv", delimiter=',', dtype=float, encoding=None)

    M = data[:, 0]
    t2 = data[:, 1]
    values = data[:, 2]

    x_unique = np.sort(np.unique(M))
    y_unique = np.sort(np.unique(t2))
    X, Y = np.meshgrid(x_unique, y_unique)

    # Créer une grille pour les valeurs z
    Z = np.zeros_like(X)
    for i in range(len(M)):
        row, col = np.where((X == M[i]) & (Y == t2[i]))
        Z[row, col] = values[i]

    # Créer la heatmap

    if dark:
        plt.pcolormesh(X, Y, Z, shading='auto', cmap="Greys")
    else:
        plt.pcolormesh(X, Y, Z, shading='auto', cmap=cmap_colorblind)
    # plt.colorbar(label='Valeurs de z')
    plt.axis((-9, 9, -1.75, 1.75))
    plt.plot([-10,10],[-10/3/np.sqrt(3),10/3/np.sqrt(3)],color="black",lw=3,ls="--")
    plt.plot([-10,10],[10/3/np.sqrt(3),-10/3/np.sqrt(3)],color="black",lw=3,ls="--")
    plt.xlabel(r"$M$",fontsize=20)
    plt.ylabel(r"$t_2$",fontsize=20)
    plt.title(r"$\textrm{Bott index for Haldane's model } N = " + f"{N_sites} $",fontsize=20)
    # plt.title(f"$N={N_sites}\quad W={r}$",fontsize=20)
    plt.xticks(fontsize=20)
    plt.yticks(fontsize=20)
    if dark:
        plt.savefig(f"../figs/phase_diagram_bott_{N_cluster_side}_{r}_dark.png",format="png",bbox_inches='tight',dpi=300)
    else:
        plt.savefig(f"../figs/phase_diagram_bott_{N_cluster_side}_{r}.pdf",format="pdf",bbox_inches='tight')
    plt.show()
            
def phase_diagram_chern(step, a, t1):
    BZ, border, Dk = lf.generate_bz(a, step)
    dim = 2
    no_bands = [1]
    Ms = np.linspace(-10,10,60)
    t2s = np.linspace(-2,2,60)
    x = []
    y = []
    Cs = []
    for M in Ms:
        for t2 in t2s:
            haldane = Haldane(a=a, t1=t1, t2=t2, M=M)
            C = chern.chern(haldane.compute_ham, BZ, Dk, dim, no_bands)
            x.append(M)
            y.append(t2)
            Cs.append(C)
            print(M,",",t2,",",C)
            

    with open("../data/phase_diagram_haldane_chern.csv", mode="w", newline="") as csvf:
        writer = csv.writer(csvf)

        for k,l,m in zip(x,y,Cs):
            writer.writerow([k,l,m])

def plot_phase_diagram_chern():

    data = np.genfromtxt("../data/phase_diagram_haldane_chern.csv", delimiter=',', dtype=complex, encoding=None)

    M = np.real(data[:, 0])
    t2 = np.real(data[:, 1])
    values = -np.real(data[:, 2])

    x_unique = np.sort(np.unique(M))
    y_unique = np.sort(np.unique(t2))
    X, Y = np.meshgrid(x_unique, y_unique)

    # Créer une grille pour les valeurs z
    Z = np.zeros_like(X)
    for i in range(len(M)):
        row, col = np.where((X == M[i]) & (Y == t2[i]))
        Z[row, col] = values[i]

    # Créer la heatmap

    plt.pcolormesh(X, Y, Z, shading='auto', cmap=cmap_colorblind)
    # plt.colorbar(label='Valeurs de z')
    plt.axis((-9, 9, -1.75, 1.75))
    plt.plot([-10,10],[-10/3/np.sqrt(3),10/3/np.sqrt(3)],color="black",lw=3,ls="--")
    plt.plot([-10,10],[10/3/np.sqrt(3),-10/3/np.sqrt(3)],color="black",lw=3,ls="--")
    plt.xlabel(r"$M$",fontsize=20)
    plt.ylabel(r"$t_2$",fontsize=20)
    plt.title(r"$\textrm{Chern number for Haldane's model}$",fontsize=20)
    plt.xticks(fontsize=20)
    plt.yticks(fontsize=20)
    plt.savefig("../figs/phase_diagram_chern.pdf",format="pdf",bbox_inches='tight')
    plt.show()
    
if __name__ == "__main__":
    N_cluster_side = 4
    a = 1
    t1 = 1
    step = 0.1
    r = 0.
    # phase_diagram_bott(N_cluster_side=N_cluster_side, a=a, t1=t1, r=r)
    plot_phase_diagram_bott(N_cluster_side, False, r=r)
    # phase_diagram_chern(step, a, t1)
    plot_phase_diagram_chern()
