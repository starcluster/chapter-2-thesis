import time
import sys

import numpy as np
import matplotlib.pyplot as plt


sys.path.append('../../chapter-2-thesis/code')
from utils import Color,timer,cmap_colorblind
import haldane_real as hr
import lattice_r as lr
import tex

tex.useTex()

def ipr_state(v):
    return np.sum(np.abs(v)**4)/np.sum(np.abs(v)**2)**2
    
def intensity_map(v, grid, m, omega, dark=False):
    if dark:
        plt.style.use('dark_background')
    N = v.shape[0]
    intensities = []
    intensities_for_ipr = []
    x, y = [], []
    for i in range(0, N):
        intensities.append(np.abs(v[i][m]) ** 2)
        x.append(grid[int(i)][0])
        y.append(grid[int(i)][1])

    sizes = [x * 10000 for x in intensities]

    iprm = np.round(ipr_state(v[:,m]),3)

    fig, ax = plt.subplots()
    if dark:
        im = ax.scatter(x, y, c=intensities, s=sizes, cmap="Greys_r")
    else:
        im = ax.scatter(x, y, c=intensities, s=sizes, cmap="inferno",label=r"$\textrm{IPR}="+f"{iprm}$")

    ax.set_xlabel(r"$x$",fontsize=20)
    ax.set_ylabel(r"$y$",fontsize=20)
    ax.legend(fontsize=20,loc='upper right')
    plt.xticks(fontsize=20)
    plt.yticks(fontsize=20)
    ax.set_title(r"$\textrm{Eigenstate associated to } E \approx" + f"{np.round(omega,3)}$",fontsize=20)
    cbar_ax = fig.add_axes([0.93, 0.11, 0.05, 0.71])
    clb = plt.colorbar(im, cax=cbar_ax)
    clb.ax.tick_params(labelsize=20)
    clb.ax.set_title(r"$|\psi_m^i|^2 $",fontsize=20)
    if dark:
        # plt.savefig(f"../figs/imap_{np.round(omega,1)}_dark.png",format="png",bbox_inches='tight',dpi=300)
        plt.savefig(f"../figs/imaps/imap_{np.round(omega,3)}_{m}_dark.pdf",format="pdf",bbox_inches='tight')
        plt.savefig(f"../figs/imaps/imap_{np.round(omega,3)}_{m}_dark.png",format="png",bbox_inches='tight',dpi=300)
    else:
        plt.savefig(f"../figs/imap_{np.round(omega,3)}_{m}.pdf",format="pdf",bbox_inches='tight')

    plt.clf()
    plt.cla()
    # plt.show()


if __name__ == "__main__":
    N_cluster_side = 10
    a = 1
    t1 = 1
    t2 = 0.2
    
    lattice = lr.generate_hex_grid_stretch(N_cluster_side=N_cluster_side,a=a)
    N_sites = lattice.shape[0]
    H_topo = hr.create_hamiltonian_haldane(lattice, a, t1, t2*1j, 0)
    w, v = np.linalg.eig(H_topo)

    f = np.real(w)
    idx = f.argsort()
    f = f[idx]
    v = v[:,idx]

    # intensity_map(v, lattice, N_sites//2-20, f[N_sites//2-20],True)
    # intensity_map(v, lattice, N_sites//2, f[N_sites//2],True)

    # for i in range(20):
    #     intensity_map(v, lattice, N_sites//2-20-i, f[N_sites//2-20-i])

    intensity_map(v, lattice, N_sites//2, f[N_sites//2])
    intensity_map(v, lattice, N_sites//2+5, f[N_sites//2+5])
    
        
        
