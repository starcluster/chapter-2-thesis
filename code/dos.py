import time
import sys

import numpy as np
import matplotlib.pyplot as plt

from utils import Color,timer,colors_colorblind
import lattice_r as lr
import haldane_real as hr
import tex
import ipr

tex.useTex()

def gen_hist(H):
    w, v = np.linalg.eig(H)
    e = np.real(w)
    n = H.shape[0]
    iprs = []
    for i in range(n):
        iprs.append(ipr.ipr_state(v[:,i]))
    return e,iprs

def plot_dos(N_cluster_side=6, a=1, topological=False, dark=False):
    if dark:
        plt.style.use('dark_background')
        
    lattice = lr.generate_hex_grid_stretch(N_cluster_side=N_cluster_side,a=a)
    N_sites = lattice.shape[0]

    t1 = 1
    if topological:
        M = 0
        t2 = 0.2 # because 0.2*3*sqrt(3) ~ 1 the width of the gap is the same
    else:
        t2 = 0
        M = 1

    if topological:
        H = hr.create_hamiltonian_haldane(lattice, a, t1, t2*1j, 0)
        H_pbc = hr.create_hamiltonian_haldane_pbc(lattice, a, t1, t2*1j, 0)
    else:
        H = hr.create_hamiltonian_haldane(lattice, a, t1, 0., 1)
        H_pbc = hr.create_hamiltonian_haldane_pbc(lattice, a, t1, 0., 1)

    if dark:
        gen_hist(H, "white", label=r"$\textrm{OBC}$")
        gen_hist(H_pbc, "yellow", label=r"$\textrm{PBC}$")
    else:
        e_obc, ipr_obc = gen_hist(H)
        e_pbc, ipr_pbc = gen_hist(H_pbc)

    fig, ax1 = plt.subplots()
    ax1.hist(e_obc, bins=80, histtype="step",color=colors_colorblind[0],label=r"$\textrm{OBC}$")
    ax1.hist(e_pbc, bins=80, histtype="step",color=colors_colorblind[1],label=r"$\textrm{PBC}$")
    ax1.set_ylabel(r"$\textrm{DOS}(E)$",fontsize=20)
    ax1.tick_params(axis='both', which='major', labelsize=20)
    ax1.set_xlabel(r"$\textrm{Energy } E$",fontsize=20)
    ax2 = ax1.twinx()
    ax2.scatter(e_obc,ipr_obc,color=colors_colorblind[0],s=1)
    ax2.scatter(e_pbc,ipr_pbc,color=colors_colorblind[1],s=1)
    ax2.set_ylabel(r"$\textrm{IPR}(\psi_m)$",fontsize=20)
    
        
    legend = ax1.legend(fontsize=20,loc='upper right')
    plt.axvline(x=t2*np.sqrt(3)*3,ls="--",color="black")
    plt.axvline(x=-t2*np.sqrt(3)*3,ls="--",color="black")
    plt.xlabel(r"$\textrm{Energy } E$",fontsize=20)

    if dark:
        plt.title(f"$N={N_sites}$" + r"$\textrm{ with }$"+ f"$t_2={t2}$" +r"$\textrm{ and }$" + f"${M=}$",fontsize=20)
    else:
        plt.title(r"$\textrm{Haldane's model on }$" + f"${N_sites}$" + r"$\textrm{ sites with }$"+ f"$t_2={t2}$",fontsize=20)
    plt.xticks(fontsize=20)
    plt.yticks(fontsize=20)


    if dark:
        plt.savefig(f"../figs/dos_N={N_sites}_{topological=}_{dark=}.png",format="png",bbox_inches='tight',dpi=300)
    else:
        plt.savefig(f"../figs/dos_N={N_sites}_{topological=}_{dark=}.pdf",format="pdf",bbox_inches='tight')
    plt.clf()
    # plt.cla()
    # legend.remove()
    # plt.axis((-3.1,3.1,0,17))
    # plt.savefig(f"../figs/dos_zoom.pdf",format="pdf",bbox_inches='tight')
    # plt.show()

if __name__ == "__main__":
    N_cluster_side = 10
    a = 1
    # plot_dos(N_cluster_side = N_cluster_side,a = a,topological=False,dark=True)
    # plot_dos(N_cluster_side = N_cluster_side,a = a,topological=True,dark=True)
    # plot_dos(N_cluster_side = N_cluster_side,a = a,topological=False)
    plot_dos(N_cluster_side = N_cluster_side,a = a,topological=True)

        
        
