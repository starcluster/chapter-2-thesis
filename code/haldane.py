import numpy as np
import matplotlib.pyplot as plt
import csv

from graphene import Graphene, basis_honeycomb_lattice
import plot_3d as p3d
import bz
import graphix
from functools import partial
import chern
import lattice_f as lf
from utils import colors_colorblind


class Haldane(Graphene):
    def __init__(self, a, t1, t2, M):
        super().__init__(a, t1)
        self.t2 = t2
        self.M = M

    def compute_ham(self, k):
        a1, a2, a3 = basis_honeycomb_lattice(self.a)
        b1 = -a3 + a2
        b2 = -a1 + a3
        b3 = -a2 + a1
        self.ham = super().compute_ham(k) + np.array([[1, 0], [0, -1]]) * (
            self.M + 2 * self.t2 * sum([np.sin(k @ bi) for bi in [b1, b2, b3]])
        )
        return self.ham


class HaldaneNanoRibbon(Graphene):
    def __init__(self, a, t1, t2, M, nb_rows):
        super().__init__(a, t1)
        self.a = a
        self.t1 = t1
        self.t2 = t2
        self.M = M
        self.nb_rows = nb_rows

    def compute_ham(self, k):
        b = self.a * np.sqrt(3)
        δ = 2 * np.cos(k * b / 2)
        γ = 2 * self.t2 * np.sin(k * b / 2)
        on_diag = np.array([[-δ * γ / 2 - self.M / 2, δ], [0, δ * γ / 2 + self.M / 2]])
        off_diag = np.array([[γ, 1], [0, -γ]])
        mat = np.kron(np.eye(self.nb_rows), on_diag) + np.kron(
            np.eye(self.nb_rows, k=1), off_diag
        )
        self.ham = mat + mat.T
        return self.ham


def gen_band_structure_3d(a, t1, t2, M,n_dots):
    grid = bz.square_bz(np.pi, n_dots)
    haldane = Haldane(a=a, t1=t1, t2=t2, M=M)
    haldane.compute_bands(grid)
    p3d.plot_band_structure_3d(haldane.bands, grid, "Haldane")

def gen_band_structure_3d_2(a, t1, t2, M,n_dots):
    grid = bz.square_bz(np.pi, n_dots)
    haldane = Haldane(a=a, t1=t1, t2=t2, M=M)
    haldane.compute_bands(grid)
    X,Y = grid.T
    fig = plt.figure(figsize=(16, 12))
    ax = fig.add_subplot(111, projection="3d")
    Z = np.array([haldane.bands[i][0] for i in range(len(haldane.bands))]).reshape(n_dots,n_dots)
    W = np.array([haldane.bands[i][1] for i in range(len(haldane.bands))]).reshape(n_dots,n_dots)
    X = X.reshape(n_dots,n_dots)
    Y = Y.reshape(n_dots,n_dots)
    print(X.shape)
    print(Z.shape)

    surf1 = ax.plot_surface(
        X, Y, Z, cmap="Blues"
    )

    surf2 = ax.plot_surface(
        X,
        Y,
        W,
        # rstride=1,
        # cstride=1,
        cmap="Reds",
        # edgecolor="none",
        # alpha=0.7,
    )

    plt.show()
def gen_band_structure(a, t1, t2s, Ms,n_dots, title):
    path = lf.generate_path_bz_2(a, ["K","Γ","M","K"], n_dots)
    path = lf.generate_path_bz_2(a, ["Γ","K","Γ"], n_dots)
    x_band = [i for i in range(path.shape[0])]
    colors = colors_colorblind

    legends = [r"$t_1=1,M=t_2=0$",r"$t_1=1,M=0.2,t_2=0$",r"$t_1=1,M=0,t_2=0.15$"]
    for t2,M,color,legend in zip(t2s,Ms,colors,legends):
        hf = Haldane(a=a, t1=t1, t2=t2, M=M)
        hf.compute_bands(path)
        graphix.plot_bands(
            x_band,
            hf.bands,
            title=title,
            pos_xticks=[0,n_dots//3, 2*n_dots//3-5],
            name_xticks=[r"$\Gamma$",r"$K$",r"$\Gamma$"],
            colors=color,
            dim_fig=0,
            size=3,
            legend=legend
        )
    plt.text(10,-2.6,"$E_-$",fontsize=20)
    plt.text(10,2.4,"$E_+$",fontsize=20)
    M = Ms[1]
    plt.hlines([-M, M], xmin=0, xmax=n_dots, color='gray', linestyle='--')
    M = Ms[-1]
    gap2 = np.sqrt((M-3*np.sqrt(3)*t2s[-1])**2)
    plt.hlines([-gap2, gap2], xmin=0, xmax=n_dots, color='gray', linestyle='dotted')
    plt.legend(loc="upper center",fontsize = 14)
    plt.savefig(
        f"../figs/Graphene.pdf",
        format="pdf",
        bbox_inches="tight",
    )
    plt.show()    

    
def gen_nanoribbon(a, t1, t2, M,n_dots,nb_rows):
    hnb = HaldaneNanoRibbon(a=a, t1=t1, t2=t2, M=M, nb_rows=nb_rows)
    path_kx = np.linspace(0, 2 * np.pi / np.sqrt(3), n_dots)
    hnb.compute_bands(path_kx)
    print(hnb.bands.shape)
    graphix.plot_bands(
        path_kx,
        hnb.bands,
        title=r"$\textrm{Nanoribbon }$"+f"$t_2={t2}$ $M={M}$ $N_r={nb_rows}$" ,
        pos_xticks=[0,np.pi/np.sqrt(3),2*np.pi/np.sqrt(3)],
        name_xticks=["$0$", "$\pi /\sqrt{3} a$", "$2\pi /\sqrt{3} a$"],
        colors="black",
        dim_fig=0,
        size=1
    )
    
def gen_localization(a, t1, t2, M, n_dots):
    grid = bz.square_bz(2*np.pi, n_dots)
    haldane = Haldane(a=a, t1=t1, t2=t2, M=M)
    haldane.compute_bands(grid)
    graphix.plot_weight(grid, haldane.weights, [0],f"$t_2={t2}$ ${M=}$",r"$k_x a$",r"$k_y a$")

def gen_berry_curvature(a, t1, t2, M, step, n_dots,dim):
    BZ, border, Dk = lf.generate_bz(a, step)
    grid_berry_curvature = bz.square_bz(2*np.pi,n_dots)
    haldane = Haldane(a=a, t1=t1, t2=t2, M=M)
    graphix.plot_lattice_field(haldane.compute_ham, grid_berry_curvature, Dk, dim,f"$t_2={t2}$ ${M=}$",r"$k_x a$",r"$k_y a$")
    
def gen_chern_number(a, t1, t2, step, n_dots,dim,  no_bands):
    BZ, border, Dk = lf.generate_bz(a, step)
    Ms = np.linspace(2.5, 6, 20)
    Cs = []
    for M in  Ms:
        haldane = Haldane(a=a, t1=t1, t2=t2, M=M)
        C = chern.chern(haldane.compute_ham, BZ, Dk, dim, no_bands)
        Cs.append(C)
        print(M,",",C)

    with open("../data/chern_evolv.csv", mode="w", newline="") as csvf:
        writer = csv.writer(csvf)
        for k,l in zip(Ms,Cs):
            writer.writerow([k,l])
    
if __name__ == "__main__":
    # BAND STRUCTURE
    # gen_band_structure_3d_2(a=1, t1=1, t2=0., M=0.,n_dots=100)
    gen_band_structure(a=1, t1=1, t2s=[0,0,0.15], Ms=[0,0.2,0], n_dots=1000, title=r"$\textrm{Haldane's model on Dirac cone}$")
    exit()


    # NANORIBBON
    n_dots=1000
    nb_rows = 10
    # gen_nanoribbon(a=1, t1=1, t2=0, M=0,n_dots=n_dots,nb_rows=nb_rows)
    gen_nanoribbon(a=1, t1=1, t2=0, M=0.2,n_dots=n_dots,nb_rows=nb_rows)
    plt.ylabel(r"$E(k_x)$")
    plt.xlabel(r"$k_x$")
    plt.savefig("../figs/nr_t20_M0.2.pdf",format="pdf",bbox_inches='tight')
    plt.clf()
    plt.cla()
    gen_nanoribbon(a=1, t1=1, t2=0.1, M=0,n_dots=n_dots,nb_rows=nb_rows)
    plt.ylabel(r"$E(k_x)$")
    plt.xlabel(r"$k_x$")
    plt.savefig("../figs/nr_t20.1_M0.pdf",format="pdf",bbox_inches='tight')
    exit()

    # LOCALIZATION
    # n_dots = 200
    # step = 0.01
    # gen_localization(a=1, t1=1, t2=0, M=0.2,n_dots=n_dots)
    # gen_localization(a=1, t1=1, t2=0.05, M=0.2,n_dots=n_dots)
    # exit()
    
    # BERRY CURVATURE
    n_dots = 200
    step = 0.01
    dim = 2
    gen_berry_curvature(a=1, t1=1, t2=0, M=0.3,step = step,n_dots=n_dots,dim=dim)
    gen_berry_curvature(a=1, t1=1, t2=0.2, M=1,step=step,n_dots=n_dots,dim=dim)
    exit()


    # CHERN NUMBER
    n_dots = 200
    step = 0.1
    dim = 2
    no_bands = [1]
    # gen_chern_number(a=1, t1=1, t2=0, M=0.2,step = step,n_dots=n_dots,dim=dim,no_bands=no_bands)
    # gen_chern_number(a=1, t1=1, t2=0.15, M=0.2,step=step,n_dots=n_dots,dim=dim,no_bands=no_bands)
   
    gen_chern_number(a=1, t1=1, t2=1,step=step,n_dots=n_dots,dim=dim,no_bands=no_bands)

    


