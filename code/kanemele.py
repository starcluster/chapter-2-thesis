import numpy as np
import matplotlib.pyplot as plt
from numpy import cos, sin, exp, sqrt
from functools import partial

import graphix
from modele import Modele
import lattice_f as lf
import lattice_r as lr
import chern
import bz
import plot_3d as p3d
from utils import colors_colorblind
import spin_chern as sc

class KaneMele(Modele):
    def __init__(self, t, λ_SO, λ_R, λ_v):
        super().__init__()
        self.t = t
        self.λ_SO = λ_SO
        self.λ_R = λ_R
        self.λ_v = λ_v

    def compute_ham(self, k):
        a = 1

        k_x, k_y = k[0], k[1]
        Γ_k = 2 * cos(a*k_x / 2) * exp(-1j * a*k_y / 2 / sqrt(3)) + exp(1j * a*k_y / sqrt(3))
        M_k = 2 * sin(a*k_x) - 4 * sin(a*k_x / 2) * cos(a*k_y * sqrt(3) / 2)
        N_k = -2 * sqrt(3) * cos(a*k_x / 2) * sin(sqrt(3) * a*k_y / 2) - 1j * 2 * (
            sin(a*k_x / 2) * cos(sqrt(3) * a*k_y / 2) + sin(a*k_x)
        )
        ham_up = np.zeros((4, 4), dtype="complex")
        ham_diag = np.diag(
            [
                self.λ_SO * M_k - self.λ_v,
                -self.λ_SO * M_k + self.λ_v,
                -self.λ_SO * M_k - self.λ_v,
                 self.λ_SO * M_k + self.λ_v ,
            ]
        )
        ham_up[0, 1] = -self.λ_R * N_k
        ham_up[0, 2] = -self.t * Γ_k
        ham_up[1, 3] = -self.t * Γ_k
        ham_up[2, 3] = self.λ_R * N_k
        self.ham = ham_diag + ham_up + np.conj(ham_up.T)
        return self.ham


    def compute_gap(self, path):
        B2 = [b[1] for b in self.bands]
        B3 = [b[2] for b in self.bands]
        return min(B3) - max(B2)

a = 1
e_vect = [(-a*np.sqrt(3)/2,-3/2*a),(a*np.sqrt(3),0),(-a*np.sqrt(3)/2,3/2*a)]
a_vect = [(0,a),(-np.sqrt(3)/2*a,-a/2),(np.sqrt(3)/2*a,-a/2)]

def H(k, t, λ_SO):
    M = np.zeros((2,2),dtype=complex)                       
    for γ in range(3):
        ke = np.dot(k,e_vect[γ])
        ka = np.dot(k,a_vect[γ])
        M += np.array([[2*np.real(λ_SO)*np.cos(ke)-2*np.imag(λ_SO)*np.sin(ke),np.exp(1j*ka)],
                       [np.exp(-1j*ka),-2*np.real(λ_SO)*np.cos(ke)+2*np.imag(λ_SO)*np.sin(ke)]])
    return M
 

def H_prime(k, t, λ_SO, λ_R, λ_v):
    Hk = H(k, t, λ_SO)
    Hk2 = H(k, t, np.conj(λ_SO))
    Mk = np.zeros((2,2),dtype=complex)

    for γ in range(3):
        ke = np.dot(k,e_vect[γ])
        ka = np.dot(k,a_vect[γ])
        c = γ 
        if γ == 2:
            c = -1
        Mk += np.array([[0,np.exp(1j*2*np.pi*c/3+1j*ka)],
                       [-np.exp(1j*2*np.pi*c/3-1j*ka),0]])
    # S = np.exp(1j*2*a*k[1]) + np.exp(1j*a*k[1])*np.cos(np.sqrt(2)*a)*2
    # Mk = np.array([[0,S],
    #                [-np.conj(S),0]])
    Mk = Mk*λ_R/2

    I_v = np.diag([-λ_v, λ_v, -λ_v, λ_v])

    return np.block([[Hk,Mk],
                     [np.conj(Mk.T),Hk2]]) + I_v

def get_ham_m(k, t, λ_SO, λ_R, λ_v):
    Hp = H_prime(k, t, λ_SO, λ_R, λ_v)
    psp_m, _ = sc.psp_from_H(Hp)
    return psp_m


def get_ham_p(k, t, λ_SO, λ_R, λ_v):
    Hp = H_prime(k, t, λ_SO, λ_R, λ_v)
    _, psp_p = sc.psp_from_H(Hp)
    return psp_p

def spin_chern_km(t, λ_SO,λ_R, λ_v, BZ , Dk, dim, no_bands):
    Cm = chern.chern(partial(get_ham_m, t=t, λ_SO=λ_SO, λ_R=λ_R, λ_v=λ_v), BZ , Dk, dim, [0])
    Cp = chern.chern(partial(get_ham_p, t=t, λ_SO=λ_SO, λ_R=λ_R, λ_v=λ_v), BZ , Dk, dim, [0])
    return (Cp-Cm)/2

def gen_band_structure(t, λ_SO, λ_R, λ_v ,n_dots, title, nfile):
    path = lf.generate_path_bz_2(a, ["Γ","K","Γ"], n_dots)
    kmf = KaneMele(t, λ_SO, λ_R, λ_v) 
    kmf.compute_bands(path)
    bands = []
    for k in path:
        w,v = np.linalg.eig(H_prime(k, t, λ_SO, λ_R, λ_v))
        w.sort()
        band = [b for b in w]

        bands.append(band)
    x_band = [i for i in range(path.shape[0])]    
    graphix.plot_bands(
        x_band,
        bands,
        title=title,
        pos_xticks=[0,n_dots//3,2*n_dots//3-2 ],
        name_xticks=[r"$\Gamma$",r"$K$",r"$\Gamma$"],
        colors=colors_colorblind[1],
        size=5
    )

    plt.savefig("../figs/" + nfile + "_bs.pdf",format="pdf",bbox_inches='tight')
    plt.show()


def gen_band_structure_3d(t, λ_SO, λ_R, λ_v ,n_dots):
    grid = bz.square_bz(np.pi, n_dots)
    kmf = KaneMele(t, λ_SO, λ_R, λ_v) 
    kmf.compute_bands(grid)

    p3d.plot_band_structure_3d(kmf.bands, grid, "Haldane")


if __name__ == "__main__":
    a = 1
    n_dots = 200
    t = 1
    λ_SO = 0.
    λ_R = 0.
    λ_v = 1

    # t0 = -1
    # t1 = -0.3
    # M = 0.3
    # φ = 3

    # gen_band_structure_3d(t, λ_SO, λ_R, λ_v ,n_dots)
    # exit()
    
    # gen_band_structure(t, λ_SO*1j, λ_R, λ_v ,n_dots, f"$t={t}\quad" + r"\lambda_{\textrm{SO}}=" + f"{λ_SO}\quad \lambda_v = {λ_v}$","Kane-Mele-1")
    # λ_SO = 0.2
    # gen_band_structure(t, λ_SO*1j, λ_R, λ_v ,n_dots, f"$t={t}\quad" + r"\lambda_{\textrm{SO}}=" + f"{λ_SO}\quad \lambda_v = {λ_v}$","Kane-Mele-2")
    # exit()
    # gen_band_structure_2(t0, t1, M, φ, n_dots, "KaneMele2")
    # exit()

    step = 0.1
    dim = 4
    no_bands = [0,1]
    title =  f"$t = {t} - \lambda_S = {λ_SO} - \lambda_R = {λ_R} - \lambda_v = {λ_v}$"

    BZ, _, Dk = lf.generate_bz(a, step)
    BZ = BZ + (1e-4, 1e-4)
    dim = 4
    λ_SO = 0.3
    λ_R = 0.
    λ_v = 1
    C = spin_chern_km(t, λ_SO*1j,λ_R, λ_v, BZ , Dk, dim, [0])
    print(λ_SO,",", λ_v,",", C)    
    
    # gen_band_structure(t, λ_SO, λ_R, λ_v ,n_dots, f"\n" + title)
    # gen_band_structure(t, λ_SO, λ_R, λ_v ,n_dots, f"$truc$")
    # gen_chern_number(t, λ_SO, λ_R, λ_v, step, n_dots, dim,  no_bands)
    # gen_berry_curvature(t, λ_SO, λ_R, λ_v, step, n_dots,dim,  no_bands,r"$\textrm{Berry curvature}$\newline"+title)
    # lr.display_lattice(path,color="red")
    # plt.show()
                                     
