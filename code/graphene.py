import numpy as np
import matplotlib.pyplot as plt

import lattice_f as lf
import tex
from modele import Modele
import plot_3d as p3d
import bz
import graphix

tex.useTex()


def basis_honeycomb_lattice(a):
    a1 = np.array([a, 0])
    a2 = np.array([-a / 2, a * np.sqrt(3) / 2])
    a3 = np.array([-a / 2, -a * np.sqrt(3) / 2])

    a1 = np.array([0, a])
    a2 = np.array([a * np.sqrt(3) / 2, -a / 2])
    a3 = np.array([-a * np.sqrt(3) / 2,-a / 2])
    return a1, a2, a3


class Graphene(Modele):
    def __init__(self, a, t1):
        super().__init__()
        self.a = a
        self.t1 = t1

    def small_h(self, k):
        a1, a2, a3 = basis_honeycomb_lattice(self.a)
        return self.t1 * sum([np.exp(1j * k @ ai) for ai in [a1, a2, a3]])

    def compute_ham(self, k):
        self.ham = np.block([[0, self.small_h(k)], [np.conj(self.small_h(k)).T, 0]])
        return self.ham

    def compute_bands(self, path):
        super().compute_bands(path)


class GrapheneNanoRibbon(Modele):
    def __init__(self, a, t1, nb_rows):
        super().__init__()
        self.a = a
        self.t1 = t1
        self.nb_rows = nb_rows

    def compute_ham(self, k):
        b = a*np.sqrt(3)
        δ = 2*np.cos(k*b/2)
        on_diag = np.array([[0,δ],
                            [0,0]])
        off_diag = np.array([[0,1],
                             [0,0]])
        mat = np.kron(np.eye(self.nb_rows),on_diag) + np.kron(np.eye(self.nb_rows,k=1),off_diag)
        self.ham = mat + mat.T
        return self.ham
        

def energy_graphene(k):
    kx, ky = k
    e2 = (
        3
        + 2 * np.cos(np.sqrt(3) * kx * a)
        + 4 * np.cos(3 / 2 * ky * a) * np.cos(np.sqrt(3) / 2 * kx * a)
    )
    return [-np.sqrt(e2), np.sqrt(e2)]

def e1(k):
    return energy_graphene(k)[0]

def e2(k):
    return energy_graphene(k)[1]


def compute_band_diagram(path, energy):
    energies = []
    for k in path:
        energies.append(energy(k))
    return np.array(energies)


def plot_band_diagram(path, energy):
    energies = compute_band_diagram(path, energy)
    band_1 = energies[:, 0]
    band_2 = energies[:, 1]
    x_band = [i for i in range(band_1.shape[0])]
    plt.scatter(x_band, band_1)
    plt.scatter(x_band, band_2)
    plt.show()


def gen_band_structure_3d_2(a, t1, t2, M,n_dots):
    grid = bz.square_bz(np.pi, n_dots)
    Z = np.array([energy_graphene(k)[0] for k in grid])
    W = np.array([energy_graphene(k)[1] for k in grid])
    X,Y = grid.T
    X = X.reshape(n_dots,n_dots)
    Y = Y.reshape(n_dots,n_dots)
    Z = Z.reshape(n_dots,n_dots)
    W = W.reshape(n_dots,n_dots)

    fig = plt.figure(figsize=(10, 10))
    ax = fig.add_subplot(111, projection="3d")


    print(X.shape)
    print(Z.shape)

    surf1 = ax.plot_surface(
        X, Y, Z, cmap="Blues",rcount=100
    )

    surf2 = ax.plot_surface(
        X,
        Y,
        W,
        cmap="Reds_r",rcount=100
    )

    ax.set_xlabel(r"$k_x$",fontsize=20)
    ax.set_ylabel(r"$k_y$",fontsize=20)
    ax.set_zlabel(r"$E(\mathbf{k})$",fontsize=20)

    ax.tick_params(axis="x", labelsize=20, pad=5)
    ax.tick_params(axis="y", labelsize=20, pad=5)
    ax.tick_params(axis="z", labelsize=20, pad=5)

    ax.view_init(azim=40, elev=10)

    plt.savefig("../figs/graphene_bs.pdf",format="pdf")
    plt.show()

if __name__ == "__main__":
    a = 1
    step = 0.01
    t1 = 1
    nb_rows = 10

    n_dots = 1000
    path_graphene = lf.generate_path_bz(a, n_dots)
    # plot_band_diagram(path_graphene, energy_graphene)
    gen_band_structure_3d_2(a=a, t1=t1, t2=0, M=0,n_dots=300)
    exit()
    # plot_band_structure_3d(energy_graphene)

    # grid = bz.square_bz(np.pi,n_dots)    
    # graphene = Graphene(a,t1)
    # graphene.compute_bands(grid) 
    # p3d.plot_band_structure_3d(graphene.bands, grid, "Graphene")

    gnb = GrapheneNanoRibbon(a = a, t1=t1, nb_rows=nb_rows)
    path_kx = np.linspace(0,3.65,n_dots)
    gnb.compute_bands(path_kx)
    print(gnb.bands.shape)
    graphix.plot_bands(path_kx,gnb.bands,title="Graphene Nanoribbon",pos_xticks=[],name_xticks=[],colors="black")
    plt.show()
    
    
    
    
