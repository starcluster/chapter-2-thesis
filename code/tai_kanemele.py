import numpy as np
import matplotlib.pyplot as plt
from scipy.linalg import eig,eigh
import os
import sys

from utils import colors_colorblind
import bott
import haldane_real as hr
import lattice_r as lr
import phase_diagram_kanemele as pdk

import spin_bott as sb


plt.rc("text.latex", preamble=r"\usepackage{amsmath}")

plt.rcParams.update(
    {"text.usetex": True, "font.family": "sans-serif", "font.sans-serif": ["Helvetica"]}
)

def add_anderson_disorder(hamiltonian,w_disorder=1,half=False):
    w_disorder /= 2
    n_hamiltonian = hamiltonian.shape[0]
    diag = np.diagonal((np.random.random((n_hamiltonian,n_hamiltonian))*2 - 1)*w_disorder)
    if half:
        diag = np.array([x if i%2==0 else 0 for i,x in enumerate(diag)])
    return hamiltonian + np.diag(diag)
    

if __name__ == "__main__":
    a = 1
    N_cluster_side = 10 # 16
    N_cluster = N_cluster_side**2
    N_sites = 6*N_cluster
    lattice = lr.generate_hex_grid_stretch(N_cluster_side=N_cluster_side,a=a)
    lattice_x2 = np.concatenate((lattice, lattice))
    
    t1 = 1
    t2 = 0.3
    M = 1.65
    M = 1.4

    n_ws = 10
    ws = np.linspace(0,2,n_ws)

    H0 = pdk.minimal_H_km(lattice, a, t1, t2*1j, M)

    print(f"{M=},{t2=}")
    n_batches = 10
    batches = np.arange(0,n_batches)
    r_batches = [0]*n_ws

    for batch in batches:
        for k,w in enumerate(ws):
            H = add_anderson_disorder(H0,w)

            eigenvalues, eigenvectors_r = np.linalg.eigh(H)
            
            els, evl , evr = sb.sort_vectors_and_listify(eigenvalues, eigenvectors_r, eigenvectors_r)
            threshold_psp = -0.1
            threshold_energy = -0.1

            sigma = sb.get_sigma_bott(N_sites)
    

            c_sb = sb.spin_bott(lattice_x2, evl, evr, els, sigma,  threshold_psp, threshold_energy, H.shape[0], False, "spectrum_psp")
    
            print(f"{w=},{c_sb=}")
            r_batches[k] += c_sb
            
    r_batches_normed = np.array(r_batches)/n_batches
    print(r_batches_normed)

    plt.scatter(ws, r_batches_normed,color="purple")
    plt.xlabel(r"$W$",fontsize=20)
    plt.ylabel(r"$C_{\textrm{B}}$",fontsize=20)
    plt.title(f"$N={N_sites}$",fontsize=20)
    plt.xticks(fontsize=20)
    plt.yticks(fontsize=20)
    plt.savefig(f"../figs/tai_kanemele_{t2=}_{M=}.pdf",format="pdf",bbox_inches='tight')
            


