"""This module provides a timer decorator
and a function for enabling or disabling the timer globally.

`timer()` is used to time the execution of a function.
It prints the execution time, function name, arguments
and keyword arguments to the console.

`enable_timer()`  is used to enable or disable the timer globally. 

`truncate_args()`  is used to truncate long arguments
 of the function for pretty printing.

Color is a dictionary of ANSI escape sequences for text formatting in the console.
"""


import timeit

from matplotlib.colors import LinearSegmentedColormap, ListedColormap

TIMER_ENABLED = True

Color = {
    "PURPLE": "\033[95m",
    "CYAN": "\033[96m",
    "DARKCYAN": "\033[36m",
    "BLUE": "\033[94m",
    "GREEN": "\033[92m",
    "YELLOW": "\033[93m",
    "RED": "\033[91m",
    "BOLD": "\033[1m",
    "UNDERLINE": "\033[4m",
    "END": "\033[0m",
}

colors_colorblind  = ["#c1272d", "#0000a7",  "#008176", "#b3b3b3", "#eecc16"] # Dark Red, Indigo,  Teal, Light Gray, Yellow
cmap_colorblind = ListedColormap(colors_colorblind, name="my_cmap")


def truncate_args(args, max_len=50):
    """Truncate arguments of the function for pretty printing."""
    arg_strs = []

    if isinstance(args, tuple):
        args = {index + 1: value for index, value in enumerate(args)}

    for argk, argv in zip(args.keys(), args.values()):
        argk_str = repr(argk)
        argv_str = repr(argv)
        if len(argv_str) > max_len:
            argv_str = argv_str[: max_len - 3] + "..."
        arg_strs.append(f"{argk_str}:{argv_str}")
    return ", ".join(arg_strs)


def timer(func):
    """Decorator for timing functions"""

    def wrapper(*args, **kwargs):
        start_time = timeit.default_timer()
        result = func(*args, **kwargs)
        end_time = timeit.default_timer()
        if TIMER_ENABLED:
            print(f"Execution time: {Color['BOLD']}{func.__name__}{Color['END']}")
            print(f"with args={truncate_args(args)}, kwargs={truncate_args(kwargs)}")
            print(
                f"took {Color['BOLD']}{end_time - start_time:.6f}{Color['END']} seconds"
            )

        return result

    return wrapper


def enable_timer(flag):
    """Gives the possibility to enable or disable timer in all the code"""
    global TIMER_ENABLED
    TIMER_ENABLED = flag


if __name__ == "__main__":
    pass
