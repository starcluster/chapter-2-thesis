import numpy as np
import plotly.graph_objects as go
from plotly.offline import plot

def plot_band_structure_3d(energy, path, name):
    nb_bands = len(energy[0])
    
    fig = go.Figure()
    scatter3ds = []
    colorscales = ["blues", "reds", "greens", "purples"]
    colors = ["blue","red"]
    for n in range(nb_bands):
        band = [e[n] for e in energy]
        scatter3d = go.Scatter3d(
            x=path[:,0],
            y=path[:,1],
            z=band,
            mode='markers',
            marker=dict(
                size=4,
                color=band,
                colorscale=colorscales[n],
                opacity=0.8
            )
        )

        scatter3ds.append(scatter3d)

    fig = go.Figure(data=scatter3ds)

    fig.update_layout(
        title=f"{name} Band Structure",
        scene=dict(
            xaxis=dict(title=r"x"),
            yaxis=dict(title=r"y"),
            zaxis=dict(title=r"z"),
            camera=dict(eye=dict(x=1.75-0.4, y=1.75-0.4, z=1-0.4)),
        ),
    )
    plot_div = plot(fig, output_type="div")
    fig.write_image("../figs/" + name + ".pdf", scale=3)
    fig.write_html("../figs/" + name + ".html")
    fig.show()
