import matplotlib.pyplot as plt
import numpy as np
import csv

import bott
import lattice_r as lr
import lattice_f as lf
import haldane_real as hr
import tex
from utils import cmap_colorblind
from haldane import Haldane
import chern
import tai_kanemele as tkm 

tex.useTex()


def wannier(v, lattice, D):
    x,y = lattice.T
    X = np.diag(x)
    Y = np.diag(y)
    n = v.shape[0]
    xv = np.dot(v,X@v)
    yv = np.dot(v,Y@v)
    s = 0
    for i in range(n):
        if hr.distance(lattice[i],(xv,yv)) > D:
            s += np.abs(v[i])
    return s
            

if __name__ == "__main__":
    N_cluster_side = 4
    a = 1
    t1 = 1
    t2 = 0j
    M = 1
    step = 0.1
    D = 5
    
    lattice = lr.generate_hex_grid_stretch(N_cluster_side=N_cluster_side,a=a)
    
    H_trivial = hr.create_hamiltonian_haldane_pbc(lattice, a, t1, 0, M)
    eigenvalues, eigenvectors = np.linalg.eig(H_trivial)
    els, evs_trivial, _ = hr.sort_vectors(eigenvalues, eigenvectors, eigenvectors)
    f = np.real(els)
    norm_evs_trivial = [wannier(vect, lattice, D) for vect in evs_trivial]
    plt.scatter(f, norm_evs_trivial, color="black", label=r"$\textrm{Trivial}$")

    H_topo = hr.create_hamiltonian_haldane_pbc(lattice, a, t1, 1j, M)
    eigenvalues, eigenvectors = np.linalg.eig(H_topo)
    els, evs_topo, _ = hr.sort_vectors(eigenvalues, eigenvectors, eigenvectors)
    f = np.real(els)
    norm_evs_topo = [wannier(vect, lattice, D) for vect in evs_topo]
    plt.scatter(f, norm_evs_topo, color="red",label=r"$\textrm{Topo}$")

    plt.legend(fontsize=20)
    plt.xlabel(r"$E$",fontsize=20)
    plt.ylabel(r"$\delta$",fontsize=20)
    plt.title(r"$\textrm{Localization of Wannier function for }" + f"{D=}$",fontsize=20)
    plt.xticks(fontsize=20)
    plt.yticks(fontsize=20)
    plt.axis((-7,0,0,8))
    plt.savefig(f"../figs/wannier_{D}.pdf",format="pdf",bbox_inches='tight')
    plt.show()

    Ds = np.linspace(0.1,15,100)

    x,y = lattice.T
    lx = np.max(x) - np.min(x)
    ly = np.max(y) - np.min(y)

    print(lx,ly)

    for i in range(H_trivial.shape[0]//2):
        loc_topo = []
        loc_trivial = []
        for D in Ds:
            loc_topo.append(wannier(evs_topo[i], lattice, D))
            loc_trivial.append(wannier(evs_trivial[i], lattice, D))
        plt.plot(Ds, loc_trivial, color="black")
        plt.plot(Ds, loc_topo, color="red")

    plt.xlabel(r"$D$",fontsize=20)
    plt.ylabel(r"$\delta $",fontsize=20)
    plt.title(r"$\textrm{Typical size of the system } L \sim 10$",fontsize=20)
    plt.xticks(fontsize=20)
    plt.yticks(fontsize=20)
    plt.savefig("../figs/wannier_evolution.pdf",format="pdf",bbox_inches='tight')
    plt.show()
