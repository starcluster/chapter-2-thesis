import numpy as np
import matplotlib.pyplot as plt
import chern
import pandas as pd
from pylatexenc.latex2text import LatexNodes2Text

import tex

tex.useTex()

def gen_ticks(a,b,c,d):
    step = (d-c)/(b-a)
    lab = ["$"+str(i)+"$" for i in range(int(a),int(b)+1)]
    return [int(i*step) for i in range(len(lab))],lab

def plot_weight(grid, weights, n_weight,name, x_title="", y_title=""):
    weight_map = []
    for w in weights:
        wm = 0
        for n in n_weight:
            wm += w[n,0]**2
        weight_map.append(wm)

    x,y = grid[:,0],grid[:,1]
    df = pd.DataFrame.from_dict(np.array([x,y,weight_map]).T)
    df.columns = ['X_value','Y_value','Z_value']
    df['Z_value'] = pd.to_numeric(df['Z_value'])
    pivotted= df.pivot('Y_value','X_value','Z_value')
    fig, ax = plt.subplots()
    im = plt.imshow(pivotted,cmap='inferno')
    posx,labx = gen_ticks(np.min(x),np.max(x),0,int(np.sqrt(len(x))))
    posy,laby = gen_ticks(np.min(y),np.max(y),0,int(np.sqrt(len(y))))
    plt.xticks(posx,labx,fontsize=14)
    plt.yticks(posy,laby[::-1],fontsize=14)
    plt.title(name,fontsize=20)
    plt.xlabel(x_title,fontsize=20)
    plt.ylabel(y_title,fontsize=20)
    cbar_ax = fig.add_axes([0.83, 0.11, 0.05, 0.71])
    clb = plt.colorbar(im, cax=cbar_ax)
    clb.ax.set_title(r"$W_1^A$",fontsize=20)
    clb.ax.tick_params(labelsize=14)
    plt.savefig(f"../figs/{LatexNodes2Text().latex_to_text(name)}.pdf", format="pdf",bbox_inches="tight")
    plt.show()
    
    
def plot_lattice_field(H, BZ, Dk, dim, name,x_title="",y_title=""):
    X, Y = [k[0] for k in BZ], [k[1] for k in BZ]
    print("ok")
    Z = np.vectorize(chern.lattice_field_k)(X, Y, H, Dk[0], Dk[1], dim, 0)
    Z += np.vectorize(chern.lattice_field_k)(X, Y, H, Dk[0], Dk[1], dim, 1)



    df = pd.DataFrame.from_dict(np.array([X,Y,Z]).T)
    df.columns = ['X_value','Y_value','Z_value']
    df['Z_value'] = pd.to_numeric(df['Z_value'])
    pivotted= df.pivot(index='Y_value',columns='X_value',values='Z_value')
    fig, ax = plt.subplots()
    im = plt.imshow(pivotted,cmap='viridis',vmin=-0.01,vmax=0.01,aspect='equal')
    print(im.get_size())
    print(np.min(X),np.max(X))
    print(np.min(Y),np.max(Y))
    print(len(X))
    posx,labx = gen_ticks(np.min(X),np.max(X),0,im.get_size()[1]+2)
    plt.xticks(posx,labx,fontsize=14)
    plt.xticks([])
    posy,laby = gen_ticks(np.min(Y),np.max(Y),0,im.get_size()[0]+2)
    plt.yticks([])

    # posx,labx = gen_ticks(np.min(X),np.max(X),0,np.sqrt(len(X))*0.9)
    # plt.xticks(posx,labx,fontsize=14)
    # posy,laby = gen_ticks(np.min(Y),np.max(Y),0,np.sqrt(len(Y))*0.9)
    # plt.yticks(posy[::-1],laby[::-1],fontsize=14)
    # ax.set_xticks(np.arange(len(X)))  # Réglage des ticks pour l'axe X
    # ax.set_xticklabels(X)  # Étiquettes pour les ticks sur l'axe X

    # ax.set_yticks(np.arange(len(Y)))  # Réglage des ticks pour l'axe Y
    # ax.set_yticklabels(Y)  # Étiquettes pour les ticks sur l'axe Y
    plt.title(name,fontsize=20)
    plt.xlabel(x_title,fontsize=20)
    plt.ylabel(y_title,fontsize=20)
    cbar_ax = fig.add_axes([0.95, 0.21, 0.05, 0.51])
    clb = plt.colorbar(im, cax=cbar_ax)
    clb.ax.set_title(r"$\bf{\Omega}(\bf{k})$",fontsize=20)
    clb.ax.tick_params(labelsize=14)

    plt.savefig(f"../figs/{LatexNodes2Text().latex_to_text(name)}.pdf", format="pdf",bbox_inches="tight")
    plt.show()


def plot_bands(
        path,
        bands,
        title="",
        pos_xticks=[],
        name_xticks=[],
        colors="blue",
        dim_fig=0,
        size=0.1,
        legend=""
):
    """Simple band diagram."""
    
    res_bands = len(path)
    nb_of_bands = len(bands[0])
    e_max = np.max(bands)
    e_min = np.min(bands)

    # quasimomentum = np.linspace(0, res_bands, res_bands)
    quasimomentum = path

    if not (isinstance(colors, str)):
        colors, color_min, color_max = colors



    for i in range(nb_of_bands):
        if isinstance(colors, str):
            if i==0:
                plt.plot(quasimomentum, [b[i] for b in bands], color=colors,label=legend, lw=size)
            else:
                plt.plot(quasimomentum, [b[i] for b in bands], color=colors, lw=size)
            # plt.scatter(quasimomentum, [b[i] for b in bands], color=colors, s=size)
        else:
            plt.scatter(
                quasimomentum,
                [b[i] for b in bands],
                c=[c[i] for c in colors],
                cmap="viridis",
                norm=matplotlib.colors.Normalize(vmin=color_min, vmax=color_max),
                s=size,
                label=legend
            )

    if not (isinstance(colors, str)):
        cbar = plt.colorbar()
        cbar.ax.tick_params(labelsize=20)

    plt.xticks(pos_xticks, name_xticks, fontsize=20)
    plt.xticks(fontsize=20)
    plt.yticks(fontsize=20)
    plt.ylabel(r"$E[\bf{k}]$", fontsize=20)
    plt.xlabel(r"$\textrm{Quasimomentum}\;[\bf{k}]$", fontsize=20)
    plt.title(r"$\textrm{" + title + "}$", fontsize=20)
    if dim_fig == 0:
        plt.axis((0, np.max(quasimomentum), e_min * 1.1, e_max * 1.1))
    else:
        plt.axis(dim_fig)
    if legend != "":
        plt.legend(fontsize=14)




if __name__ == "__main__":
    pass
