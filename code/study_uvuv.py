import matplotlib.pyplot as plt
import numpy as np
import csv
from scipy.spatial.distance import cdist

import bott
import lattice_r as lr
import lattice_f as lf
import haldane_real as hr
import tex
from haldane import Haldane
import chern

tex.useTex()

def find_nearest_point(point, points):
    distances = cdist([point], points)
    nearest_index = np.argmin(distances)
    return nearest_index

def update_positions(current_points, new_points):
    for i in range(len(current_points)):
        nearest_index = find_nearest_point(current_points[i], new_points)
        current_points[i] = new_points[nearest_index]
    
def plot_eigenvalues(N_cluster_side=6,t1=1,M=1):
    lattice = lr.generate_hex_grid_stretch(N_cluster_side=N_cluster_side,a=a)
    fig, axs = plt.subplots(2, 2)

    for ax,t2 in zip(axs.flatten(),[0,0.1,0.2,0.3]):
        H = hr.create_hamiltonian_haldane_pbc(lattice, a, t1, t2*1j, M)
        # H = hr.create_hamiltonian_haldane(lattice, a, t1, t2*1j, M)
        eigenvalues, eigenvectors = np.linalg.eig(H)
        els, evs, _ = hr.sort_vectors(eigenvalues, eigenvectors, eigenvectors)
        k = np.searchsorted(els, 0)
        u,v = bott.compute_vxvy_no_pol(lattice, evs)
        u,v = u[:k,:k], v[:k, :k]
        uvuv = u@v@np.linalg.inv(u)@np.linalg.inv(v)
        uvuv = u@v@np.conj(u.T)@np.conj(v.T)
        λ_uvuv, ψ_uvuv = np.linalg.eig(uvuv)
        b = np.round(np.sum(np.log(λ_uvuv))/2/np.pi,3)

        b_positive = np.sum(np.imag(λ_uvuv) < 0)
        b_negative = np.sum(np.imag(λ_uvuv) > 0)
        print(b_positive)
        print(b_negative)

        
        ax.axhline(y=0,color="red")
        ax.axvline(x=0,color="red")
        ax.scatter(np.real(λ_uvuv), np.imag(λ_uvuv),color="black")
        ax.set_title(f"$t_2={t2}\quad B={b}$")
        ax.axis((-0.1,2.5,-0.3,0.3))


    plt.savefig("../figs/uvuv_eigenvalues.pdf",format="pdf",bbox_inches='tight')
    plt.tight_layout()
    plt.show()

def anim_eigenvalues(N_cluster_side=6,M=1,t1=1):
    lattice = lr.generate_hex_grid_stretch(N_cluster_side=N_cluster_side,a=a)
    t2s = np.linspace(0,0.3,100)

    for i,t2 in enumerate(t2s):
        H = hr.create_hamiltonian_haldane_pbc(lattice, a, t1, t2*1j, M)
        # H = hr.create_hamiltonian_haldane(lattice, a, t1, t2*1j, M)
        eigenvalues, eigenvectors = np.linalg.eig(H)
        els, evs, _ = hr.sort_vectors(eigenvalues, eigenvectors, eigenvectors)
        k = np.searchsorted(els, 0)
        u,v = bott.compute_vxvy_no_pol(lattice, evs)
        u,v = u[:k,:k], v[:k, :k]
        uvuv = u@v@np.linalg.inv(u)@np.linalg.inv(v)
        # uvuv = u@v@np.conj(u.T)@np.conj(v.T)
        λ_uvuv, ψ_uvuv = np.linalg.eig(uvuv)
        b = np.round(np.sum(np.log(λ_uvuv))/2/np.pi,3)

        b_positive = np.sum(np.imag(λ_uvuv) > 0)
        b_negative = np.sum(np.imag(λ_uvuv) < 0)

        plt.text(x=1.85,y=0.2,s=f"$N_+ = {b_positive}$",fontsize=20)
        plt.text(x=1.85,y=-0.2,s=f"$N_- = {b_negative}$",fontsize=20)
        
        plt.axhline(y=0,color="red")
        plt.axvline(x=0,color="red")
        plt.scatter(np.real(λ_uvuv), np.imag(λ_uvuv),color="black")
        plt.title(f"$t_2={np.round(t2,2)}\quad B={b}$",fontsize=20)
        plt.axis((-0.1,2.5,-0.3,0.3))
        plt.xlabel(r"$\textrm{Re}(\lambda)$",fontsize=20)
        plt.ylabel(r"$\textrm{Im}(\lambda)$",fontsize=20)
        plt.xticks(fontsize=20)
        plt.yticks(fontsize=20)
        plt.tight_layout()
        plt.savefig(f"../anim/eigenvalues_uvuv_{i}.png",format="png",bbox_inches='tight',dpi=300)
        plt.cla()
        plt.clf()

def anim_eigenvalues_t(N_cluster_side=4,M=1,t1=1):
    plt.style.use('dark_background')
    lattice = lr.generate_hex_grid_stretch(N_cluster_side=N_cluster_side,a=a)
    ts = np.linspace(0.2,0.8,700)
    t2 = 0.3

    H = hr.create_hamiltonian_haldane_pbc(lattice, a, t1, t2*1j, M)
    # H = hr.create_hamiltonian_haldane(lattice, a, t1, t2*1j, M)
    eigenvalues, eigenvectors = np.linalg.eig(H)
    els, evs, _ = hr.sort_vectors(eigenvalues, eigenvectors, eigenvectors)
    k = np.searchsorted(els, 0)
    u,v = bott.compute_vxvy_no_pol(lattice, evs)
    u,v = u[:k,:k], v[:k, :k]

    ut = np.eye(k)
    vt = np.eye(k)
    uvuv = ut@vt@np.linalg.inv(ut)@np.linalg.inv(vt)
    λ_uvuv, ψ_uvuv = np.linalg.eig(uvuv)

    current_points = np.array(list(zip(np.real(λ_uvuv),np.imag(λ_uvuv))))

    plt.axhline(y=0,color="white",zorder=0,lw=0.3)
    plt.axvline(x=0,color="white",zorder=0,lw=0.3)


    for i,t in enumerate(ts):
        ut = np.eye(k)*(1-t) + u*t
        vt = np.eye(k)*(1-t) + v*t
        uvuv = ut@vt@np.linalg.inv(ut)@np.linalg.inv(vt)
        # uvuv = u@v@np.conj(u.T)@np.conj(v.T)
        λ_uvuv, _ = np.linalg.eig(uvuv)
        b = np.abs(np.imag(np.round(np.sum(np.log(λ_uvuv))/2/np.pi,3)))
        new_points = np.array(list(zip(np.real(λ_uvuv),np.imag(λ_uvuv))))
        line1s = []
        for j in range(len(current_points)):
            nearest_index = find_nearest_point(current_points[j], new_points)
            line1 = plt.plot([current_points[j, 0], new_points[nearest_index, 0]],
                     [current_points[j, 1], new_points[nearest_index, 1]],
                             color='white',zorder=2)
            line1s.append(line1)

            line2 = plt.plot([current_points[j, 0], new_points[nearest_index, 0]],
                     [current_points[j, 1], new_points[nearest_index, 1]],
                     color='gray',zorder=1)
        current_points = new_points

        # b_positive = np.sum(np.imag(λ_uvuv) > 0)
        # b_negative = np.sum(np.imag(λ_uvuv) < 0)

        # k_positive = np.sum(np.real(λ_uvuv) > 0)
        # k_negative = np.sum(np.real(λ_uvuv) < 0)

        # plt.text(x=0,y=0.75,s=f"$N_+ = {b_positive}$",fontsize=20)
        # plt.text(x=0,y=-0.75,s=f"$N_- = {b_negative}$",fontsize=20)

        # plt.text(x=1.5,y=0,s=f"$K_+ = {k_positive}$",fontsize=20)
        # plt.text(x=-2.5,y=-0,s=f"$K_- = {k_negative}$",fontsize=20)

        plt.text(x=-2.4,y=4.2,s=f"$t_2 = {t2}$",fontsize=20)
        plt.text(x=-2.4,y=3.3,s=f"$M = {M}$",fontsize=20)

        text_b = plt.text(x=1.5,y=4.2,s=f"$B={b}$",fontsize=20)
        text_s = plt.text(x=1.5,y=3.3,s=f"$s={np.round(t,2)}$",fontsize=20)
        
        

        sca = plt.scatter(np.real(λ_uvuv), np.imag(λ_uvuv),color="white",s=2,zorder=3)

        plt.axis((-2.5,2.5,-5,5))
        plt.xlabel(r"$\textrm{Re}(\lambda)$",fontsize=20)
        plt.ylabel(r"$\textrm{Im}(\lambda)$",fontsize=20)
        plt.xticks(fontsize=20)
        plt.yticks(fontsize=20)
        plt.tight_layout()
        plt.savefig(f"../anim/eigenvalues_uvuv_{i}.png",format="png",bbox_inches='tight',dpi=300)
        sca.remove()
        for line1 in line1s:
            line1[0].remove()
        text_b.remove()
        text_s.remove()
        # plt.cla()
        # plt.clf()

def anim_eigenvalues_invertible_matrices_t(u,v):
    ts = np.linspace(0,1,100)
    k = u.shape[0]
    ut = np.eye(k)
    vt = np.eye(k)
    uvuv = ut@vt@np.linalg.inv(ut)@np.linalg.inv(vt)
    λ_uvuv, ψ_uvuv = np.linalg.eig(uvuv)

    current_points = np.array(list(zip(np.real(λ_uvuv),np.imag(λ_uvuv))))

    for i,t in enumerate(ts):
        ut = np.eye(k)*(1-t) + u*t
        vt = np.eye(k)*(1-t) + v*t
        uvuv = ut@vt@np.linalg.inv(ut)@np.linalg.inv(vt)
        # uvuv = u@v@np.conj(u.T)@np.conj(v.T)
        λ_uvuv, _ = np.linalg.eig(uvuv)
        b = np.abs(np.imag(np.round(np.sum(np.log(λ_uvuv))/2/np.pi,3)))
        new_points = np.array(list(zip(np.real(λ_uvuv),np.imag(λ_uvuv))))
        line1s = []
        for j in range(len(current_points)):
            nearest_index = find_nearest_point(current_points[j], new_points)
            line1 = plt.plot([current_points[j, 0], new_points[nearest_index, 0]],
                     [current_points[j, 1], new_points[nearest_index, 1]],
                             color='deepskyblue',zorder=2)
            line1s.append(line1)

            line2 = plt.plot([current_points[j, 0], new_points[nearest_index, 0]],
                     [current_points[j, 1], new_points[nearest_index, 1]],
                     color='lightblue',zorder=1)
        current_points = new_points

        # b_positive = np.sum(np.imag(λ_uvuv) > 0)
        # b_negative = np.sum(np.imag(λ_uvuv) < 0)

        # k_positive = np.sum(np.real(λ_uvuv) > 0)
        # k_negative = np.sum(np.real(λ_uvuv) < 0)

        # plt.text(x=0,y=0.75,s=f"$N_+ = {b_positive}$",fontsize=20)
        # plt.text(x=0,y=-0.75,s=f"$N_- = {b_negative}$",fontsize=20)

        # plt.text(x=1.5,y=0,s=f"$K_+ = {k_positive}$",fontsize=20)
        # plt.text(x=-2.5,y=-0,s=f"$K_- = {k_negative}$",fontsize=20)
        
        plt.axhline(y=0,color="black",zorder=0,lw=0.3)
        plt.axvline(x=0,color="black",zorder=0,lw=0.3)
        sca = plt.scatter(np.real(λ_uvuv), np.imag(λ_uvuv),color="steelblue",s=1,zorder=3)
        plt.title(f"$B={b}\quad \\theta={np.round(t,2)} $",fontsize=20)
        plt.axis((-2.5,2.5,-5,5))
        plt.xlabel(r"$\textrm{Re}(\lambda)$",fontsize=20)
        plt.ylabel(r"$\textrm{Im}(\lambda)$",fontsize=20)
        plt.xticks(fontsize=20)
        plt.yticks(fontsize=20)
        plt.tight_layout()
        plt.savefig(f"../anim/eigenvalues_uvuv_{i}.png",format="png",bbox_inches='tight',dpi=300)
        sca.remove()
        for line1 in line1s:
            line1[0].remove()

    

if __name__ == "__main__":
    N_cluster_side = 6
    a = 1
    t1 = 1
    t2 = 1
    M = 1
    step = 0.1

    # anim_eigenvalues_t(N_cluster_side=4,M=1,t1=1)

    anim_eigenvalues_t()
    exit()

    N = 10
    for i in range(1000):
        print(i)
        u = (np.random.rand(N,N)*2-1) + 1j*(np.random.rand(N,N)*2-1) 
        v = (np.random.rand(N,N)*2-1) + 1j*(np.random.rand(N,N)*2-1) 
        u = np.round(u,1)
        v = np.round(v,1)
        # u = np.array([[1,1,1],[0,1,1],[0,0,1]])
        # v = np.array([[1,0,1],[0,1,0],[1,0,2]])
        uvuv = u@v@np.linalg.inv(u)@np.linalg.inv(v)
        comm_uv = u@v-v@u
        λ,_ = np.linalg.eig(uvuv)
        b = np.imag(np.sum(np.log(λ))/2/np.pi)
        if b > 1.9:
            print(b)
            print(u)
            print(v)
            print(np.linalg.norm(comm_uv))
            anim_eigenvalues_invertible_matrices_t(u,v)
            exit()
            
    exit()
    anim_eigenvalues_t()
    
