import numpy as np
import matplotlib.pyplot as plt
from scipy.linalg import eig,eigh
import os
import sys

from utils import colors_colorblind
import bott
import haldane_real as hr
import lattice_r as lr
import phase_diagram_kanemele as pdk


plt.rc("text.latex", preamble=r"\usepackage{amsmath}")

plt.rcParams.update(
    {"text.usetex": True, "font.family": "sans-serif", "font.sans-serif": ["Helvetica"]}
)


def get_sigma_bott(N):
    """N is the number of cluster"""
    return np.kron(np.array([[1,0],[0,-1]]), np.eye(N))


def make_projector(vl, vr):
    """Create a projector from left and right eigenvectors"""
    n = vl[0].shape[0]
    P = np.zeros((n, n), dtype="complex128")
    for i in range(len(vl)):
        vi = vl[i]
        vit = vr[i]
        P += np.outer(vi, vit.conj())/np.dot(vit.conj(), vi)

    return P


def get_p_sigma_p_bott(w, vl, vr, sigma, omega):
    i = np.searchsorted(w, omega)
    P = make_projector(vl[0:i], vr[0:i])
    return P @ sigma @ P


def sort_vectors(w, vl, vr):
    idx = w.argsort()
    vl = vl[:, idx]
    vr = vr[:, idx]
    w = w[idx]
    return w, vl, vr

def sort_vectors_and_listify(w, vl, vr):
    idx = w.argsort()
    vl = vl[:, idx]
    vr = vr[:, idx]
    w = w[idx]
    vl = [vl[:,i] for i in range(vl.shape[0])]
    vr = [vr[:,i] for i in range(vr.shape[0])]
    return w, vl, vr

def spin_bott(grid, vl, vr, w, sigma, threshold_psp, threshold_energy, N_cluster, plot_psp=False, name_psp="spectrum_psp"):
    # Σ = get_sigma_bott(N_cluster)
    psp = get_p_sigma_p_bott(w, vl, vr, sigma, threshold_psp)
    w, vl, vr = eig(psp, left=True, right=True)
    w_psp, vl_psp, vr_psp = sort_vectors(*eig(psp, left=True, right=True))

    if plot_psp:
        xp = np.arange(0,w_psp.shape[0])
        plt.axhline(y=-0.1,color=colors_colorblind[0])
        plt.axhline(y=0.1,color=colors_colorblind[1])
        plt.scatter(xp,np.real(w_psp),color="black")
        plt.xlabel(r"$\textrm{Index of eigenvalues}$",fontsize=20)
        plt.ylabel(r"$\textrm{DOS}$",fontsize=20)
        plt.xticks(fontsize=20)
        plt.yticks(fontsize=20)
        plt.title(r"$\textrm{Spectrum of } P\sigma P\quad$",fontsize=20)
        plt.savefig(f"../figs/{name_psp}.pdf",format="pdf",bbox_inches='tight')
        plt.show()
    
    idx = w_psp.argsort()
    bm = bott.bott(
        grid, vr_psp, w_psp, threshold_energy, pol=False, dagger=False, verbose=False
    )
    idx = (-w_psp).argsort()
    bp = bott.bott(
        grid,
        vr_psp[:,idx],
        -w_psp[idx],
        threshold_energy,
        pol=False,
        dagger=False,
        verbose=False,
    )
    return (bp - bm) / 2

def all_spin_bott(grid, vl, vr, w, sigma, threshold_psp, N_cluster, plot_psp=False, name_psp="spectrum_psp"):
    # Σ = get_sigma_bott(N_cluster)
    psp = get_p_sigma_p_bott(w, vl, vr, sigma, threshold_psp)
    w, vl, vr = eig(psp, left=True, right=True)
    w_psp, vl_psp, vr_psp = sort_vectors(*eig(psp, left=True, right=True))

    if plot_psp:
        xp = np.arange(0,w_psp.shape[0])
        plt.axhline(y=-0.1,color=colors_colorblind[0])
        plt.axhline(y=0.1,color=colors_colorblind[1])
        plt.scatter(xp,np.real(w_psp),color="black")
        plt.xlabel(r"$\textrm{Index of eigenvalues}$",fontsize=20)
        plt.ylabel(r"$\textrm{DOS}$",fontsize=20)
        plt.xticks(fontsize=20)
        plt.yticks(fontsize=20)
        plt.title(r"$\textrm{Spectrum of } P\sigma P\quad$",fontsize=20)
        plt.savefig(f"../figs/{name_psp}.pdf",format="pdf",bbox_inches='tight')
        plt.show()
    
    idx = w_psp.argsort()
    all_bm = bott.all_bott(
        grid, vr_psp, w_psp, pol=False, dagger=False, verbose=False
    )

    idx = (-w_psp).argsort()
    all_bp = bott.all_bott(
        grid,
        vr_psp[:,idx],
        -w_psp[idx],
        pol=False,
        dagger=False,
        verbose=False,
    )

    all_sb = {}

    
    # print(all_bp.values())
    # print(all_bm)
    # exit()

    # print(all_bp[-w_psp[0]])
    # exit()

    for f in w_psp:
        sb = (all_bp[-f]-all_bm[f])/2
        all_sb[f] = sb

    return all_sb

def compute_psp(grid, vl, vr, w, sigma, threshold_psp, threshold_energy, N_cluster):
    # Σ = get_sigma_bott(N_cluster)
    psp = get_p_sigma_p_bott(w,vl,vr,sigma,threshold_psp)
    w_psp, vl_psp, vr_psp = sort_vectors(*eig(psp, left=True, right=True))
    return w_psp
    

if __name__ == "__main__":
    a = 1
    N_cluster_side = 6
    N_cluster = N_cluster_side**2
    N_sites = 6*N_cluster
    lattice = lr.generate_hex_grid_stretch(N_cluster_side=N_cluster_side,a=a)
    lattice_x2 = np.concatenate((lattice, lattice))
    
    t1 = 1
    t2 = 1
    M = 5.5

    H = pdk.minimal_H_km(lattice, a, t1, t2*1j, M)

    eigenvalues, eigenvectors_r = np.linalg.eigh(H)

    
    # plt.axvline(x=0,color=colors_colorblind[0])
    # plt.hist(np.real(eigenvalues), bins=80, histtype="step",color=colors_colorblind[2],label=f"$N={N_sites}$")
    # plt.xlabel(r"$\textrm{Energy } \omega$",fontsize=20)
    # plt.ylabel(r"$\textrm{DOS}$",fontsize=20)
    # plt.title(r"$\textrm{Kane-Mele's model with }$" + f"$t_2={t2}" +r"\textrm{ and }" +f"M={M}$" ,fontsize=20)
    # plt.legend(fontsize=20,loc="upper right")
    # plt.xticks(fontsize=20)
    # plt.yticks(fontsize=20)
    # plt.savefig(f"../figs/dos_kanemele.pdf",format="pdf",bbox_inches='tight')
    # plt.show()
    
    els, evl , evr = sort_vectors_and_listify(eigenvalues, eigenvectors_r, eigenvectors_r)
    threshold_psp = -0.1
    threshold_energy = -0.1

    sigma = get_sigma_bott(N_sites)
    

    c_sb = spin_bott(lattice_x2, evl, evr, els, sigma,  threshold_psp, threshold_energy, H.shape[0], False, "spectrum_psp")
    
    print(f"{M=},{t2=},{c_sb=}")


