<TeXmacs|2.1>

<style|<tuple|generic|british>>

<\body>
  <section*|R�sum� en Fran�ais>

  Le but de ce chapitre est d'introduire l'ensemble des outils que l'on
  utilisera ensuite. Pour cela, on �tudie des mod�les simples et d�j� connus
  comme le mod�le d'Haldane.\ 

  Pour s'�chauffer, on commence par une �tude succinte du graph�ne: des
  atomes de carbone plac�s dans un r�seau en \Pnid d'abeille\Q. Chaque atome
  est trait� comme un syst�me � deux niveaux poss�dant une fr�quence de
  r�sonnance <math|\<omega\><rsub|0>>, on parle de r�sonateur ponctuel.
  L'ensemble peut �tre vu comme la superposition de deux sous-r�seaux
  triangulaires <math|A> et <math|B>, see <reference|fig_graphene_bs>.
  L'interaction pour un atome se fait uniquement avec ses trois plus proches
  voisins par un terme de couplage <math|t<rsub|1>> qui en pratique vaudra
  l'unit� dans tout ce chapitre. Ce mod�le nous permet d'introduire le
  concept de zone de Brillouin et permet au lecteur de se familiariser avec
  l'�tude d'un syst�me cristallin dans l'espace de Fourier: on s'int�resse
  aux bandes d'�nergies autoris�es pour les �lectrons dans ce r�seau. Puis on
  complexifie ce syst�me en brisant la symm�trie par inversion spaciale: les
  atomes du sous-r�seau <math|A> ont d�sormais une fr�quence de r�sonance
  diff�rente des atomes du sous r�seau <math|B> et on notera <math|M> cette
  diff�rence. Cet ajout permet au syst�me d'exhiber un gap entre la bande de
  valence et la bande de conduction. Notre syst�me n'est donc plus conducteur
  mais semi-conducteur. Un mat�riau qui peut �tre repr�sent� par ce mod�le
  est le nitrure de bore.\ 

  On aimerait aussi savoir quels chemins empruntent les �lectrons lorsqu'ils
  traversent le mat�riau, et plus particuli�rement si ceux-ci circulent sur
  le bord en �vitant soigneusement l'int�rieur (le \Pbulk\Q). Un tel
  comportement traduit l'existence d'une phase topologique, la recherche de
  ces phases est l'objectif principal de cette th�se. Ceci �tant dit, une
  �tude dans l'espace r�ciproque ne permet pas d'observer d'�tats de bords,
  on d�cide donc de supposer l'�chantillon infini dans une direction
  seulement et dans l'autre direction on consid�re un nombre fini de couches.
  On parlera de \Prubban\Q ou de \Pnanorubban\Q pour d�signer ce type de
  mod�le. Dans le cas du graph�ne avec <math|M\<neq\>0>, on ne d�tecte pas
  d'�tats de bords. Pour la suite on aimerait avoir un syst�me o� des �tats
  sont pr�sents dans le gap et localis�s sur les bords de l'�chantillon.

  Cela nous m�ne � �tudier le mod�le d'Haldane, on ajoute aux atomes du
  graph�ne la possibilit� de se coupler � leurs second plus proche voisins
  par l'interm�diaire d'un couplage purement complexe <math|i*t<rsub|2>>. Ce
  terme permet d'ouvrir un gap avec des �tats de bord. On se pose la question
  de savoir si ce gap bn�ficie d'une protection topologique (on parlera dans
  la suite de \Pgap topologique\Q). Cette propri�t� assure que les �tats de
  bords pr�sents dans le gap existeront tant que le gap n'aura pas �t� ferm�.
  Pour d�terminer si c'est le cas, on peut calculer un invariant topologique,
  c'est � dire un nombre qui trahit l'existence d'une phase topologique en
  �tant non nul au sein d'une telle phase. Un premier exemple d'invariant
  topologique est le nombre de Chern qui se calcule tr�s bien dans l'espace
  de Fourier. Dans l'espace r�el en revanche on utilisera un autre invariant:
  l'indice de Bott. On verifie que ces deux invariants co�ncident
  compl�tement en tra�ant le diagramme de phase en fonction des deux
  param�tres <math|M> et <math|t<rsub|2>>. On remarquera �galement que
  l'indice de Bott contrairement � d'autres invariants est toujours un nombre
  entier et cela m�me pour un syst�me avec relativement peu d\Qatomes. Le
  temps de calcul pour l'indice de Bott est directement reli� � la taille du
  syst�me et est donc sup�rieur � celui n�cessaire pour calculer le nombre de
  Chern. Cependant, avec un indice dans l'espace r�el, nous pouvons pr�dire
  le caract�re topologique de syst�me d�sordonn�s ou ne pr�sentant pas
  d'invariance par translation. C'est ce dernier point quui nous int�resse le
  plus.

  On s'interesse finalement au mod�le de Kane-Mele, il s'agit d'un mod�le
  d'Haldane auquel on ajoute pour chaque atome l'existence d'un spin. On
  n�gligera tout terme de m�lange entre spin \Pup\Q et spin \Pdown\Q. Le
  calcul d'un invariant topologique dans ces conditions requiert de s�parer
  les fonctions propres associ�es aux spin \Pdown\Q de celles associ�es aux
  spin \Pup\Q. On aura recourt pour cela au nombre Chern de spin et �
  l'indice de Bott de spin. L� encore on trace le diagramme de phase afin de
  v�rifier la correspondance entre ces deux indices.

  \ <section|Graphene>

  Graphene, a single layer of carbon atoms arranged in a two-dimensional
  honeycomb lattice, has garnered significant scientific interest since its
  theoretical prediction by P. R. Wallace in 1947. However, it wasn't until
  2004 that graphene was experimentally realized by Andre Geim and Konstantin
  Novoselov, leading them to the Nobel Prize.\ 

  <subsection|Analysing Graphene's Brillouin zone>

  By its construction process, graphene can be viewed as the superposition of
  two triangular lattices, referred to as sublattices <math|A> and <math|B>,
  despite their complete identity. We introduce this notation distinction
  solely for calculation purposes in this section. However, we will later
  explore the possibility of treating these two sublattices differently.
  \ Electron hopping occurs exclusively between nearest neighbors; in this
  perspective, atoms in the sublattice <math|A> interact with atoms in the
  <math|B> sublattice, and vice versa, with an interaction amplitude equal to
  unity, see <reference|fig_graphene_bs>. The distance between nearest
  neighbors remains constant and will be denoted as <math|a> throughout this
  manuscript. Additionally, we define the following vectors:

  <\equation>
    <math-bf|a><rsub|1>=<matrix|<tformat|<table|<row|<cell|0>>|<row|<cell|a>>>>>,<math-bf|a><rsub|2>=<matrix|<tformat|<table|<row|<cell|-a<sqrt|3>/2>>|<row|<cell|-a/2>>>>>,<math-bf|a><rsub|3>=<matrix|<tformat|<table|<row|<cell|a<sqrt|3>/2>>|<row|<cell|-a/2>>>>>
  </equation>

  <\big-figure|<image|figs/graphene_schema.pdf|0.4001par|||><image|figs/BZ.pdf|0.51par|||><label|fig_graphene_bs>>
    (Left) Graphene can be seen as a superposition of two triangular
    sublattices <math|A> (blue disks) and <math|B> (red disks).
    <math|<math-bf|a><rsub|1>,<math-bf|a><rsub|2>> and
    <math|<math-bf|a><rsub|3>> are basis vector of the lattice. <math|a> is
    the unique parameter of this lattice. (Right) represents the first
    Brillouin zone with different paths: the blue one followed to construct
    the band diagram shown in <reference|haldane_bs>.
  </big-figure>

  The Hamiltonian of Graphene is given by

  <\equation>
    H<rsub|graphene>=<big|sum><rsub|<around*|\<langle\>|j
    n|\<rangle\>>>c<rsub|j><rsup|\<dagger\>>c<rsub|n><label|h-graphene>
  </equation>

  where <math|c<rsub|><rsup|\<dagger\>>> and <math|c> are operators of
  construction and annihilation. The notation <math|<around*|\<langle\>|j
  n|\<rangle\>>> means that the sum runs over all nearest neighbors. The
  Schr�dinger equation can be written for a specific eigenfunction
  <math|\<psi\>> as

  <\equation>
    H<rsub|graphene>\<psi\>=E\<psi\><label|schrodinger>
  </equation>

  This gives

  <\equation>
    <big|sum><rsub|<text|n>.<text|n>.n>\<psi\><rsub|n>=E\<psi\><rsub|j><label|nnschro>
  </equation>

  where the sum runs over the nearest neighbor of <math|j>. If we place
  ourselves in the bulk, far from the edges, we can consider the lattice to
  be infinite and periodic, we are in the conditions for applying Bloch's
  theorem:

  <\theorem>
    The solutions of the Schr�dinger equation for a periodic potential must
    be of a special form:

    <\equation*>
      <wide|\<psi\>|^><rsub|j><around*|(|<math-bf|k>|)>=u<around*|(|<math-bf|k>|)>\<mathe\><rsup|i*<math-bf|k>\<cdot\><math-bf|r><rsub|j>>
    </equation*>

    where <math|u<around*|(|<math-bf|k>|)>> has the period of the crystal
    lattice and <math|<math-bf|r><rsub|j>> the position of one cell of the
    lattice.
  </theorem>

  [kittel]

  In the following we will consider a Hamiltonian
  <math|<wide|H|^><rsub|graphene>> and its associated eigenvectors
  <math|<wide|\<psi\>|^>> that belongs to the reciprocal space. In this space
  the primitive cell is called the Brillouin Zone and is also an hexagon as
  shown in <reference|fig_graphene_bs>. In Fourier space the Hamiltonian
  <reference|h-graphene> becomes:

  <\equation>
    <wide|H|^><rsub|graphene><around*|(|<math-bf|k>|)>=<matrix|<tformat|<table|<row|<cell|0>|<cell|h<rsub|0><around*|(|<math-bf|k>|)>>>|<row|<cell|h<rsub|0><around*|(|<math-bf|k>|)><rsup|\<ast\>>>|<cell|0>>>>>
  </equation>

  where:

  <\equation>
    h<rsub|0><around*|(|<math-bf|k>|)>=t<rsub|1><big|sum><rsub|n=1><rsup|3>exp<around*|(|i*<math-bf|k>\<cdot\><math-bf|a><rsub|n>|)>
  </equation>

  And <math|t<rsub|1>> is equals to unity here. We can rewrite the wave
  function as follows, for <math|A> or <math|B> sublattice:

  <\equation>
    <wide|\<psi\>|^><rsub|j>=<around*|{|<tabular*|<tformat|<table|<row|<cell|A<around*|(|<math-bf|k>|)>\<mathe\>xp<around*|(|i*<math-bf|k>\<cdot\><math-bf|r><rsub|j>|)>>>|<row|<cell|B<around*|(|<math-bf|k>|)>\<mathe\>xp<around*|(|i*<math-bf|k>\<cdot\><math-bf|r><rsub|j>|)>>>>>>|\<nobracket\>>
  </equation>

  \;

  The coefficients <math|A<around*|(|<math-bf|k>|)>> stands for the <math|A>
  sublattice and <math|B<around*|(|<math-bf|k>|)>> stands for the <math|B>
  sublattice. Using this ansatz in <reference|nnschro> we can write

  <\eqnarray*>
    <tformat|<table|<row|<cell|B<around*|(|<math-bf|k>|)><big|sum><rsub|n=1><rsup|3>exp<around*|(|i*<math-bf|k>\<cdot\><around*|(|<math-bf|r><rsub|j>+<math-bf|a><rsub|n>|)>|)>>|<cell|=>|<cell|E*A<around*|(|<math-bf|k>|)>\<mathe\>xp<around*|(|i*<math-bf|k>\<cdot\><math-bf|r><rsub|j>|)><eq-number><label|eqA>>>|<row|<cell|A<around*|(|<math-bf|k>|)><big|sum><rsub|n=1><rsup|3>exp<around*|(|i*<math-bf|k>\<cdot\><around*|(|<math-bf|r><rsub|j>-<math-bf|a><rsub|n>|)>|)>>|<cell|=>|<cell|E*B<around*|(|<math-bf|k>|)>\<mathe\>xp<around*|(|i*<math-bf|k>\<cdot\><math-bf|r><rsub|j>|)><eq-number><label|eqB>>>>>
  </eqnarray*>

  Where <reference|eqA> is written for an arbitrary site of the sublattice
  <math|A> and <reference|eqB> \ for an arbitrary site of the sublattice
  <math|B>. We multiply both equation and simplify by
  <math|A<around*|(|<math-bf|k>|)>B<around*|(|<math-bf|k>|)>>, this lets us
  with:

  <\equation>
    E<rsup|2>=<around*|\||<big|sum><rsub|k=1><rsup|3>exp<around*|(|i*<math-bf|k>\<cdot\><math-bf|a><rsub|k>|)>|\|><rsup|2>
  </equation>

  After simplification, we obtain formula <reference|eqE2> for
  <math|E<rsup|2>>, resulting in two roots, one positive and one negative. By
  calculating these <math|E> values in the Brillouin zone, we can infer the
  structure of the energy bands. It becomes apparent that these two surfaces
  touch at points known as Dirac cones. Around the Dirac points, the
  dispersion relation is linear in <math|<math-bf|k>>.

  <\equation>
    E<rsup|2>=3+2cos<around*|(|<sqrt|3>k<rsub|x>a|)>+4cos<around*|(|<frac|3|2>k<rsub|y>a|)>cos<around*|(|<frac|<sqrt|3>|2>k<rsub|x>a|)><label|eqE2>
  </equation>

  Note that we could have obtained this result using the Pauli matrices to
  rewrite the hamiltonian:

  <\equation>
    \<sigma\><rsub|x>=<matrix|<tformat|<table|<row|<cell|0>|<cell|1>>|<row|<cell|1>|<cell|0>>>>>,\<sigma\><rsub|x>=<matrix|<tformat|<table|<row|<cell|0>|<cell|-i>>|<row|<cell|i>|<cell|0>>>>>,\<sigma\><rsub|x>=<matrix|<tformat|<table|<row|<cell|1>|<cell|0>>|<row|<cell|0>|<cell|-1>>>>>
  </equation>

  Will give:

  <\equation>
    <wide|H|^><rsub|graphene><around*|(|<math-bf|k>|)>=t<rsub|1><big|sum><rsub|n=1><rsup|3>\<sigma\><rsub|x>cos<around*|(|<math-bf|k>\<cdot\><math-bf|a><rsub|n>|)>-\<sigma\><rsub|y>sin<around*|(|<math-bf|k>\<cdot\><math-bf|a><rsub|n>|)>=<math-bf|\<Omega\>><around*|(|<math-bf|k>|)>\<cdot\><math-bf|\<sigma\>>
  </equation>

  where <math|<math-bf|\<sigma\>>=<around*|(|\<sigma\><rsub|x>,\<sigma\><rsub|y>,\<sigma\><rsub|z>|)>>
  is the Pauli vector and\ 

  <\equation>
    <math-bf|\<Omega\>><around*|(|<math-bf|k>|)>=<matrix|<tformat|<table|<row|<cell|t<rsub|1><big|sum><rsub|n=1><rsup|3>cos<around*|(|<math-bf|k>\<cdot\><math-bf|a><rsub|n>|)>>>|<row|<cell|-t<rsub|1><big|sum><rsub|n=1><rsup|3><rsub|>sin<around*|(|<math-bf|k>\<cdot\><math-bf|a><rsub|n>|)>>>|<row|<cell|0>>>>>
  </equation>

  From properties of the Pauli vector<\footnote>
    It is easy to check that for an arbitrary vector
    <math|<math-bf|v>\<in\>\<bbb-R\><rsup|3>> we have
    <math|det<around*|(|<math-bf|v>\<cdot\><math-bf|\<sigma\>>|)>=-<around*|\<\|\|\>|<math-bf|v>|\<\|\|\>><rsup|2>>
    and <math|tr><math|<around*|(|<math-bf|v>\<cdot\><math-bf|\<sigma\>>|)>=0>.
  </footnote>, the eigenvalues of <math|<wide|H|^><rsub|graphene><around*|(|<math-bf|k>|)>>
  are

  <\equation>
    E=\<pm\><sqrt|<around*|(|t<rsub|1><big|sum><rsub|n=1><rsup|3>cos<around*|(|<math-bf|k>\<cdot\><math-bf|a><rsub|n>|)>|)><rsup|2>+<around*|(|-t<rsub|1><big|sum><rsub|n=1><rsup|3>sin<around*|(|<math-bf|k>\<cdot\><math-bf|a><rsub|n>|)>|)><rsup|2>>
  </equation>

  \;

  This last result allows us to visualize the band structure, either by
  following a path in the Brillouin zone that will cross points of interest,
  or by plotting the entire bands in 3D as displayed in
  <reference|graphene_bs>. This last solution, although it gives a better
  idea of what is going on, can also be very ressource consuming as we don't
  always have access to analytical formula for
  <math|E<around*|(|<math-bf|k>|)>>. In such cases, we resort to another
  method: numerically diagonalizing the matrix for each <math|k> and then we
  will settle for a simple path in the Brillouin zone.

  <\big-figure|<image|figs/graphene_bs.pdf|0.51par|||>>
    \ Band structure of graphene for a bit more than the Brillouin zone,
    notice the so-called six \PDirac points\Q where the conduction band and
    the valence band are touching each other.<label|graphene_bs>
  </big-figure>

  A simple way to open a gap here would be to introduce an opposite onsite
  energy <math|M> on <math|A> and <math|B> sites respectively, the
  Hamiltonian in Fourier space becomes then:

  <\equation>
    <wide|H|^><rsub|onsite><around*|(|<math-bf|k>|)>=<wide|H|^><rsub|graphene><around*|(|<math-bf|k>|)>+M\<sigma\><rsub|z><label|nitrure-bore>
  </equation>

  Where <math|\<sigma\><rsub|z>> denotes the third Pauli matrice. This model
  could be used to describe the gapped semi-conductor Boron nitride. Although
  we have now opened a gap in the Fourier space, we don't know if this gap
  would be present in real space. Indeed in real space, because of the
  presence of the edges, modes might appear within the region of the gap. I
  recall here that edge modes are a matter of interest because they are the
  signature for a topological phase.

  <subsection|Graphene nanoribbon><label|graphene-nanoribbon>

  <\big-figure|<image|figs/graphene_nano_ribbon_schema.pdf|0.901par|||>>
    <label|nanoribbon_schema>
  </big-figure>

  Since it is not possible to observe edge states in Fourier space because
  the system is considered infinite. To be able to observe them without
  making the calculation too cumbersome, we consider an infinite system in
  the <math|x> direction but finite in the <math|y> direction:
  <math|N<rsub|r>> rows are repeating. Such a system is usually called
  nanoribbon or just ribbon. The path traced will now be of the form
  <math|<math-bf|k>=<around*|(|k<rsub|x>,0|)>>. We follow the same procedure
  as before but assuming that the translational invariance is now only
  present along <math|x> axis:

  <\equation>
    \<psi\><rsub|j>=<around*|{|<tabular*|<tformat|<table|<row|<cell|A<rsub|j
    > \<mathe\>xp<around*|(|i*k<rsub|x>x<rsub|j>|)>>>|<row|<cell|B<rsub|j>
    \<mathe\>xp<around*|(|i*k<rsub|x>x<rsub|j>|)>>>>>>|\<nobracket\>>
  </equation>

  where <math|j=1\<ldots\>.N<rsub|r>>. And then from the Schr�dinger equation
  <reference|schrodinger>:

  <\eqnarray*>
    <tformat|<table|<row|<cell|E*A<rsub|j>
    \<mathe\>xp<around*|(|i*k<rsub|x>x<rsub|j>|)>>|<cell|=>|<cell|B<rsub|j+1>
    \<mathe\>xp<around*|(|i*k<rsub|x>x<rsub|j>|)>>>|<row|<cell|>|<cell|+>|<cell|B<rsub|j>
    \<mathe\>xp<around*|(|i*k<rsub|x><around*|(|x<rsub|j>-b/2|)>|)>>>|<row|<cell|>|<cell|+>|<cell|B<rsub|j>
    \<mathe\>xp<around*|(|i*k<rsub|x><around*|(|x<rsub|j>+b/2|)>|)><eq-number>>>>>
  </eqnarray*>

  <\eqnarray*>
    <tformat|<table|<row|<cell|E*B<rsub|j>
    \<mathe\>xp<around*|(|i*k<rsub|x>x<rsub|j>|)>>|<cell|=>|<cell|A<rsub|j+1>
    \<mathe\>xp<around*|(|i*k<rsub|x>x<rsub|j>|)>>>|<row|<cell|>|<cell|+>|<cell|A<rsub|j>
    \<mathe\>xp<around*|(|i*k<rsub|x><around*|(|x<rsub|j>-b/2|)>|)>>>|<row|<cell|>|<cell|+>|<cell|A<rsub|j>
    \<mathe\>xp<around*|(|i*k<rsub|x><around*|(|x<rsub|j>+b/2|)>|)><eq-number>>>>>
  </eqnarray*>

  where <math|b=a<sqrt|3>> is the distance between nearest-neighbor sites of
  the same sublattice as shown in <reference|nanoribbon_schema>. We can
  divide both sides by <math|exp<around*|(|i*k<rsub|x>x<rsub|j>|)>> and
  introduce the variable <math|\<delta\>=2cos<around*|(|k<rsub|x>b/2|)>>:

  <\eqnarray*>
    <tformat|<table|<row|<cell|E*A<rsub|j>>|<cell|=>|<cell|B<rsub|j+1>+B<rsub|j>
    \<delta\>>>|<row|<cell|E*B<rsub|j>>|<cell|=>|<cell|A<rsub|j-1>+*A<rsub|j>
    \<delta\><eq-number>>>>>
  </eqnarray*>

  This leaves us with a system of <math|2*N<rsub|r>> equations that can be
  written in the following compact form:

  <\equation>
    <wide|H|^><rsub|graphene,nr><math-bf|\<Psi\>>=E<math-bf|\<Psi\>>
  </equation>

  where <math|<math-bf|\<Psi\>>=<around*|(|A<rsub|1>,B<rsub|1>,\<ldots\>.A<rsub|N<rsub|r>>,B<rsub|N<rsub|r>>|)>>
  and:

  <\equation>
    <wide|H|^><rsub|graphene,nr><around*|(|k<rsub|x>|)>=<matrix|<tformat|<cwith|1|2|1|2|color|#c1272d>|<cwith|3|4|3|4|color|#c1272d>|<cwith|6|7|6|7|color|#c1272d>|<cwith|8|9|8|9|color|#c1272d>|<cwith|1|2|3|4|color|#0000a7>|<cwith|3|4|1|2|color|#0000a7>|<cwith|6|7|8|9|color|#0000a7>|<cwith|8|9|6|7|color|#0000a7>|<table|<row|<cell|0>|<cell|\<delta\>>|<cell|0>|<cell|1>|<cell|\<ldots\>>|<cell|0>|<cell|0>|<cell|0>|<cell|0>>|<row|<cell|\<delta\>>|<cell|0>|<cell|0>|<cell|0>|<cell|\<ldots\>>|<cell|0>|<cell|0>|<cell|0>|<cell|0>>|<row|<cell|0>|<cell|0>|<cell|0>|<cell|\<delta\>>|<cell|\<ldots\>>|<cell|0>|<cell|0>|<cell|0>|<cell|0>>|<row|<cell|1>|<cell|0>|<cell|\<delta\>>|<cell|0>|<cell|\<ldots\>>|<cell|0>|<cell|0>|<cell|0>|<cell|0>>|<row|<cell|\<vdots\>>|<cell|\<vdots\>>|<cell|\<vdots\>>|<cell|\<vdots\>>|<cell|\<ddots\>>|<cell|\<vdots\>>|<cell|\<vdots\>>|<cell|\<vdots\>>|<cell|\<vdots\>>>|<row|<cell|0>|<cell|0>|<cell|0>|<cell|0>|<cell|\<ldots\>>|<cell|0>|<cell|\<delta\>>|<cell|0>|<cell|1>>|<row|<cell|0>|<cell|0>|<cell|0>|<cell|0>|<cell|\<ldots\>>|<cell|\<delta\>>|<cell|0>|<cell|0>|<cell|0>>|<row|<cell|0>|<cell|0>|<cell|0>|<cell|0>|<cell|\<ldots\>>|<cell|0>|<cell|0>|<cell|0>|<cell|\<delta\>>>|<row|<cell|0>|<cell|0>|<cell|0>|<cell|0>|<cell|\<ldots\>>|<cell|1>|<cell|0>|<cell|\<delta\>>|<cell|0>>>>>
  </equation>

  Colors are there to highlight blocks of self-interacting sites (red) and
  interacting sites (blue). We then follow a straight line going from
  <math|<around*|(|0,0|)>> to <math|<around*|(|2\<pi\>/<sqrt|3>,0|)>> and we
  plot the figure shown in <reference|nanoribbon_simu>. <math|>

  For the gapped system previously introduces in <reference|nitrure-bore> the
  equations become:

  <\eqnarray*>
    <tformat|<table|<row|<cell|E*A<rsub|j>>|<cell|=>|<cell|-M*A<rsub|j>+B<rsub|j+1>+2*B<rsub|j>
    cos<around*|(|k<rsub|x>b/2|)>>>|<row|<cell|E*B<rsub|j>>|<cell|=>|<cell|M*B<rsub|j>+A<rsub|j-1>+2*A<rsub|j>
    cos<around*|(|k<rsub|x>b/2|)><eq-number>>>>>
  </eqnarray*>

  turning the Hamiltonian into:

  \;

  <\equation>
    <wide|H|^><rsub|onsite,nr><rsub|><around*|(|k<rsub|x>|)>=<matrix|<tformat|<cwith|1|2|1|2|color|#bf4040>|<cwith|3|4|3|4|color|#bf4040>|<cwith|6|7|6|7|color|#bf4040>|<cwith|8|9|8|9|color|#bf4040>|<cwith|1|2|3|4|color|#0000a7>|<cwith|3|4|1|2|color|#0000a7>|<cwith|6|7|8|9|color|#0000a7>|<cwith|8|9|6|7|color|#0000a7>|<table|<row|<cell|-M>|<cell|\<delta\>>|<cell|0>|<cell|1>|<cell|\<ldots\>>|<cell|0>|<cell|0>|<cell|0>|<cell|0>>|<row|<cell|\<delta\>>|<cell|M>|<cell|0>|<cell|0>|<cell|\<ldots\>>|<cell|0>|<cell|0>|<cell|0>|<cell|0>>|<row|<cell|0>|<cell|0>|<cell|-M>|<cell|\<delta\>>|<cell|\<ldots\>>|<cell|0>|<cell|0>|<cell|0>|<cell|0>>|<row|<cell|1>|<cell|0>|<cell|\<delta\>>|<cell|M>|<cell|\<ldots\>>|<cell|0>|<cell|0>|<cell|0>|<cell|0>>|<row|<cell|\<vdots\>>|<cell|\<vdots\>>|<cell|\<vdots\>>|<cell|\<vdots\>>|<cell|\<ddots\>>|<cell|\<vdots\>>|<cell|\<vdots\>>|<cell|\<vdots\>>|<cell|\<vdots\>>>|<row|<cell|0>|<cell|0>|<cell|0>|<cell|0>|<cell|\<ldots\>>|<cell|-M>|<cell|\<delta\>>|<cell|0>|<cell|1>>|<row|<cell|0>|<cell|0>|<cell|0>|<cell|0>|<cell|\<ldots\>>|<cell|\<delta\>>|<cell|M>|<cell|0>|<cell|0>>|<row|<cell|0>|<cell|0>|<cell|0>|<cell|0>|<cell|\<ldots\>>|<cell|0>|<cell|0>|<cell|-M>|<cell|\<delta\>>>|<row|<cell|0>|<cell|0>|<cell|0>|<cell|0>|<cell|\<ldots\>>|<cell|1>|<cell|0>|<cell|\<delta\>>|<cell|M>>>>>
  </equation>

  and the associated band diagram is shown in <reference|nanoribbon_simu>.
  This band diagram shows a gap of width <math|2M>; unfortunately, this gap
  is not topological as no edge states are present. This shows that breaking
  parity symmetry in this model is not enough to triggers topology because we
  don't observe edge states. An alternative would be break time reversal
  symmetry. We will adress this point in the next section.

  <section|Haldane's model>

  In this section we will study a model introduced by the physicist Duncan
  Haldane in 1988 to demonstrate the existence of quantum Hall effect even
  without strong magnetic field [Haldane]. This model exhibits topological
  states so it will be perfect to simply test tools that we will use later
  when looking for topology. We will also use it to show the equivalence
  between two topological invariants: the Chern number and the Bott index,
  one operating in the Fourier space and the other in real space.\ 

  <subsection|Brillouin zone>

  <\big-figure>
    <image|figs/haldane_schema.pdf|0.521par|||>

    \;
  <|big-figure>
    <label|haldane_nanoribbon>Haldane's model rules for next nearest neghour
    coupling. If we are on sites <math|A> and the vector between two
    interacting sites is in <math|<around*|{|e<rsub|0>,e<rsub|1>,e<rsub|-1>|}>>
    (respectively <math|<around*|{|-e<rsub|0>,-e<rsub|1>,-e<rsub|-1>|}>>)
    then the strength of the interaction will be <math|t<rsub|2>>
    (respectively <math|<wide|t<rsub|2>|\<bar\>>>). On sites <math|B> the
    rule is the same but both <math|t2> and <math|<wide|t<rsub|2>|\<bar\>>>
    are multiplied by <math|-1>.
  </big-figure>

  The Haldane's model can be seen as an extension of the model describing the
  graphene that now takes into account an imaginary coupling between
  next-nearest neighbour. This coupling takes place between sites of the same
  sublattice as shown by <reference|haldane_nanoribbon>. Physically, this
  imaginary coupling represents a phase acquired by the electrons while
  hoping. Such a phase can be acquire when electrons move in the presence of
  a weak magnetic field per example. The presence of this phase breaks time
  reversal symmetry in the system but we insist that no magnetic flux is
  going through the system. The Hamiltonian becomes in real space:

  <\equation>
    H<rsub|Haldane>=t<rsub|1><big|sum><rsub|<around*|\<langle\>|j
    k|\<rangle\>>>c<rsub|j><rsup|\<dagger\>>c<rsub|k>+i*t<rsub|2><big|sum><rsub|<around*|\<langle\>|<around*|\<langle\>|j
    k|\<rangle\>>|\<rangle\>>>c<rsub|j><rsup|\<dagger\>>c<rsub|k>+M<big|sum><rsub|j>\<varepsilon\><rsub|j>c<rsub|j><rsup|\<dagger\>>c<rsub|j>
  </equation>

  where <math|i*t<rsub|2>> denotes the interaction between next-nearest
  neighbour and <math|M> the onsite energy which sign alternates between
  sites <math|A> and <math|B>. To keep it simple we will only consider the
  case where <math|t<rsub|2>> is a real number and thus the bond
  <math|i*t<rsub|2>> is purely imaginary. This hamiltonian can be rewritten
  in Fourier space following the same method exposed in the previous section:

  <\equation>
    <wide|H|^><rsub|Haldane><around*|(|<math-bf|k>|)>=<wide|H|^><rsub|graphene><around*|(|<math-bf|k>|)>+M\<sigma\><rsub|z>+2t<rsub|2><big|sum><rsub|n=1><rsup|3>\<sigma\><rsub|z>sin<around*|(|<math-bf|k>\<cdot\><math-bf|b><rsub|n>|)>=<math-bf|\<Xi\>><around*|(|<math-bf|k>|)>\<cdot\><math-bf|\<sigma\>>
  </equation>

  Where <math|<math-bf|\<sigma\>>=<around*|(|\<sigma\><rsub|x>,\<sigma\><rsub|y>,\<sigma\><rsub|z>|)>>
  and

  <\equation>
    <math-bf|\<Xi\>><around*|(|<math-bf|k>|)>=<matrix|<tformat|<table|<row|<cell|t<rsub|1><big|sum><rsub|n=1><rsup|3>cos<around*|(|<math-bf|k>\<cdot\><math-bf|a><rsub|n>|)>>>|<row|<cell|-t<rsub|1><big|sum><rsub|n=1><rsup|3><rsub|>sin<around*|(|<math-bf|k>\<cdot\><math-bf|a><rsub|n>|)>>>|<row|<cell|M+2t<rsub|2><big|sum><rsub|n=1><rsup|3>sin<around*|(|<math-bf|k>\<cdot\><math-bf|b><rsub|n>|)>>>>>>
  </equation>

  From which we deduce the eigenenergies

  <\equation>
    E<rsub|\<pm\>><around*|(|<math-bf|k>|)>=\<pm\><sqrt|<around*|(|t<rsub|1><big|sum><rsub|n=1><rsup|3>cos<around*|(|<math-bf|k>\<cdot\><math-bf|a><rsub|n>|)>|)><rsup|2>+<around*|(|-t<rsub|1><big|sum><rsub|n=1><rsup|3>sin<around*|(|<math-bf|k>\<cdot\><math-bf|a><rsub|n>|)>|)><rsup|2>+<around*|(|M+2t<rsub|2><big|sum><rsup|3><rsub|n=1>sin<around*|(|<math-bf|k>\<cdot\><math-bf|b><rsub|n>|)>|)><rsup|2>>
  </equation>

  The width of the gap is obtained by taking the difference between the upper
  band and the lower band:

  <\equation>
    W<rsub|gap>=E<rsub|+><around*|(|K|)>-E<rsub|-><around*|(|K|)>=2<around*|\||M-3<sqrt|3>t<rsub|2>|\|>
  </equation>

  <\big-figure|<image|figs/Graphene.pdf|0.6par|||>>
    Zoom on a dirac point following a path in the Brillouin Zone, the
    dispersion relation is clearly linear (red line) for the graphene model.
    Blue and teal lines represents Haldane's model for
    <math|<around*|(|t=1,M=0.2|)>> and <math|<around*|(|t=1,t<rsub|2>=0.15|)>>,
    width of the gap is given by <math|2<around*|\||M-3<sqrt|3|>t<rsub|2>|\|>>.<label|haldane_bs>
  </big-figure>

  This last formula shows that both <math|M> and <math|t<rsub|2>> can open a
  gap but also that the combination of both can close it. However, one
  question remains: are these gaps topologically identical ? A first hint of
  topology can be demonstrated by plotting the relative weight of quasimodes
  on sites of sublattices <math|A> and <math|B> <with|color|red|ref?>. We can
  define for the band <math|\<alpha\>>:

  <\equation>
    W<rsub|\<alpha\>><rsup|A>=<around*|\||\<psi\><rsub|\<alpha\>><rsup|A>|\|><rsup|2>
    <infix-and>W<rsub|\<alpha\>><rsup|B>=<around*|\||\<psi\><rsub|\<alpha\>><rsup|B>|\|><rsup|2>
  </equation>

  and we have:

  <\equation>
    W<rsub|\<alpha\>><rsup|A>+W<rsub|\<alpha\>><rsup|B>=1
  </equation>

  The result for the first band (<math|\<alpha\>=1>) is depicted in
  <reference|weight_a> and clearly shows that <math|K> and <math|K<rprime|'>>
  points are not equivalent anymore in the case of time-reversal symmetry
  breaking. These types of modifications provide strong indications of
  topology, although they do not constitute irrefutable evidence. []

  <\big-figure|<image|figs/t_2=0 M=0.2.pdf|0.49par|||><image|figs/t_2=0.05
  M=0.2.pdf|0.49par|||>>
    First band of the Hamiltonian for the Haldane's model with a trivial gap
    on left and with time-reversal symmetry broken on right. The band is
    color-coded as a function of weight of the quasimodes on the atomic
    sublattice <math|A>: <math|W<rsup|A><rsub|1>>.<label|weight_a>
  </big-figure>

  Another hint we can have a look at is the edge states, they are of course
  inexistant in this borderless system but we can make them appear by
  studying our model on a nanoribbon.

  <subsection|Nanoribbon>

  Like in the previous section <reference|graphene-nanoribbon>, we consider
  again the nanoribbon presented in <reference|nanoribbon_schema>, we
  consider the <math|y> direction to be finite and the <math|x> direction
  infinite, then we use Schr�dinger equation <reference|schrodinger>:

  <\eqnarray*>
    <tformat|<table|<row|<cell|E*A<rsub|j>
    \<mathe\>xp<around*|(|i*k<rsub|x>x<rsub|j>|)>>|<cell|=>|<cell|B<rsub|j+1>
    \<mathe\>xp<around*|(|i*k<rsub|x>x<rsub|j>|)>>>|<row|<cell|>|<cell|+>|<cell|B<rsub|j>
    \<mathe\>xp<around*|(|i*k<rsub|x><around*|(|x<rsub|j>-b/2|)>|)>>>|<row|<cell|>|<cell|+>|<cell|B<rsub|j>
    \<mathe\>xp<around*|(|i*k<rsub|x><around*|(|x<rsub|j>+b/2|)>|)><eq-number><label|ea-nano>>>|<row|<cell|>|<cell|+>|<cell|i*t<rsub|2><around*|{|A<rsub|j>
    \<mathe\>xp<around*|(|i*k<rsub|x><around*|(|x<rsub|j>+b|)>|)>|\<nobracket\>>>>|<row|<cell|>|<cell|>|<cell|-A<rsub|j+1>
    \<mathe\>xp<around*|(|i*k<rsub|x><around*|(|x<rsub|j>+b/2|)>|)>>>|<row|<cell|>|<cell|>|<cell|+A<rsub|j+1>
    \<mathe\>xp<around*|(|i*k<rsub|x><around*|(|x<rsub|j>-b/2|)>|)>>>|<row|<cell|>|<cell|>|<cell|-A<rsub|j>
    \<mathe\>xp<around*|(|i*k<rsub|x><around*|(|x<rsub|j>-b|)>|)>>>|<row|<cell|>|<cell|>|<cell|+A<rsub|j-1>
    \<mathe\>xp<around*|(|i*k<rsub|x><around*|(|x<rsub|j>-b/2|)>|)>>>|<row|<cell|>|<cell|>|<cell|-A<rsub|j-1>
    \<mathe\>xp<around*|(|i*k<rsub|x><around*|(|x<rsub|j>+b/2|)>|)>>>>>
  </eqnarray*>

  <\eqnarray*>
    <tformat|<table|<row|<cell|E*B<rsub|j>
    \<mathe\>xp<around*|(|i*k<rsub|x>x<rsub|j>|)>>|<cell|=>|<cell|A<rsub|j+1>
    \<mathe\>xp<around*|(|i*k<rsub|x>x<rsub|j>|)>>>|<row|<cell|>|<cell|+>|<cell|A<rsub|j>
    \<mathe\>xp<around*|(|i*k<rsub|x><around*|(|x<rsub|j>-b/2|)>|)>>>|<row|<cell|>|<cell|+>|<cell|A<rsub|j>
    \<mathe\>xp<around*|(|i*k<rsub|x><around*|(|x<rsub|j>+b/2|)>|)><eq-number><label|eb-nano>>>|<row|<cell|>|<cell|+>|<cell|i*t<rsub|2><around*|{|-B<rsub|j>
    \<mathe\>xp<around*|(|i*k<rsub|x><around*|(|x<rsub|j>+b|)>|)>|\<nobracket\>>>>|<row|<cell|>|<cell|>|<cell|+B<rsub|j+1>
    \<mathe\>xp<around*|(|i*k<rsub|x><around*|(|x<rsub|j>+b/2|)>|)>>>|<row|<cell|>|<cell|>|<cell|-B<rsub|j+1>
    \<mathe\>xp<around*|(|i*k<rsub|x><around*|(|x<rsub|j>-b/2|)>|)>>>|<row|<cell|>|<cell|>|<cell|+B<rsub|j>
    \<mathe\>xp<around*|(|i*k<rsub|x><around*|(|x<rsub|j>-b|)>|)>>>|<row|<cell|>|<cell|>|<cell|-B<rsub|j-1>
    \<mathe\>xp<around*|(|i*k<rsub|x><around*|(|x<rsub|j>-b/2|)>|)>>>|<row|<cell|>|<cell|>|<cell|+B<rsub|j-1>
    \<mathe\>xp<around*|(|i*k<rsub|x><around*|(|x<rsub|j>+b/2|)>|)>>>>>
  </eqnarray*>

  After simplification and introduction of
  <math|\<gamma\>=2t<rsub|2>sin<around*|(|k<rsub|x>b/2|)>> equations
  <reference|ea-nano> and <reference|eb-nano> become more compact:

  <\eqnarray*>
    <tformat|<table|<row|<cell|E*A<rsub|j>>|<cell|=>|<cell|\<gamma\>A<rsub|j-1>-\<delta\>\<gamma\>A<rsub|j>+\<delta\>B<rsub|j>>>|<row|<cell|>|<cell|+>|<cell|\<gamma\>A<rsub|j+1>+B<rsub|j+1><eq-number>>>>>
  </eqnarray*>

  <\eqnarray*>
    <tformat|<table|<row|<cell|E*B<rsub|j>>|<cell|=>|<cell|A<rsub|j-1>-\<gamma\>B<rsub|j-1>+\<delta\>A<rsub|j>>>|<row|<cell|>|<cell|+>|<cell|\<gamma\>\<delta\>B<rsub|j>-\<gamma\>B<rsub|j+1><eq-number>>>>>
  </eqnarray*>

  <with|color||And for the Hamiltonian:>

  <\equation*>
    <wide|H|^><rsub|Haldane,nr><rsub|><around*|(|k<rsub|x>|)>=<matrix|<tformat|<cwith|1|2|1|2|color|#bf4040>|<cwith|3|4|3|4|color|#bf4040>|<cwith|6|7|6|7|color|#bf4040>|<cwith|8|9|8|9|color|#bf4040>|<cwith|1|2|3|4|color|#0000a7>|<cwith|3|4|1|2|color|#0000a7>|<cwith|6|7|8|9|color|#0000a7>|<cwith|8|9|6|7|color|#0000a7>|<table|<row|<cell|-\<delta\>\<gamma\>>|<cell|\<delta\>>|<cell|\<gamma\>>|<cell|1>|<cell|\<ldots\>>|<cell|0>|<cell|0>|<cell|0>|<cell|0>>|<row|<cell|\<delta\>>|<cell|\<delta\>\<gamma\>>|<cell|0>|<cell|-\<gamma\>>|<cell|\<ldots\>>|<cell|0>|<cell|0>|<cell|0>|<cell|0>>|<row|<cell|\<gamma\>>|<cell|0>|<cell|-\<delta\>\<gamma\>>|<cell|\<delta\>>|<cell|\<ldots\>>|<cell|0>|<cell|0>|<cell|0>|<cell|0>>|<row|<cell|1>|<cell|-\<gamma\>>|<cell|\<delta\>>|<cell|\<delta\>\<gamma\>>|<cell|\<ldots\>>|<cell|0>|<cell|0>|<cell|0>|<cell|0>>|<row|<cell|\<vdots\>>|<cell|\<vdots\>>|<cell|\<vdots\>>|<cell|\<vdots\>>|<cell|\<ddots\>>|<cell|\<vdots\>>|<cell|\<vdots\>>|<cell|\<vdots\>>|<cell|\<vdots\>>>|<row|<cell|0>|<cell|0>|<cell|0>|<cell|0>|<cell|\<ldots\>>|<cell|-\<delta\>\<gamma\>>|<cell|\<delta\>>|<cell|\<gamma\>>|<cell|1>>|<row|<cell|0>|<cell|0>|<cell|0>|<cell|0>|<cell|\<ldots\>>|<cell|\<delta\>>|<cell|\<delta\>\<gamma\>>|<cell|0>|<cell|-\<gamma\>>>|<row|<cell|0>|<cell|0>|<cell|0>|<cell|0>|<cell|\<ldots\>>|<cell|\<gamma\>>|<cell|0>|<cell|-\<delta\>\<gamma\>>|<cell|\<delta\>>>|<row|<cell|0>|<cell|0>|<cell|0>|<cell|0>|<cell|\<ldots\>>|<cell|1>|<cell|-\<gamma\>>|<cell|\<delta\>>|<cell|\<delta\>\<gamma\>>>>>>
  </equation*>

  <\big-figure|<image|figs/Nanoribbont_2=0 M=0.2
  N_r=10.pdf|0.5par|||><image|figs/Nanoribbont_2=0.1 M=0
  N_r=10.pdf|0.5par|||>>
    <with|color|red|<label|nanoribbon_simu>>Band structure of a Nanoribbon of
    graphene folowing a path defined by <math|<around*|{|<around*|(|k<rsub|x>,0|)>,k<rsub|x>\<in\><around*|[|0;2\<pi\>/<sqrt|3>|]>|}>>.
    On the left we added the parameter <math|M> that represents the onsite
    energy and on the right <math|t<rsub|2>> is a pure imaginary coefficient
    that make coupling between next nearest neighbours. Using a nanoribbon
    allows one to see the edge states that are present in the case
    <math|3<sqrt|2>t<rsub|2>\<gtr\>M>.
  </big-figure>

  <subsection|Chern number>

  The Chern number is a topological invariant useful to demonstrate the
  topological properties of a band gap. It is given by integration of the
  Berry curvature over the entire Brillouin Zone. Firstly we can define the
  Berry connection of the band <math|m>:

  <\equation>
    <math-bf|A><rsub|m><around*|(|<math-bf|k>|)>=i<around*|\<langle\>|\<psi\><rsub|m><around*|(|<math-bf|k>|)><around*|\||\<nabla\>|\<nobracket\>>\<psi\><rsub|m><around*|(|<math-bf|k>|)>|\<rangle\>>\<in\>\<cal-M\><rsub|2\<times\>1><around*|(|\<bbb-C\>|)>
  </equation>

  where <math|\<cal-M\><rsub|2\<times\>1><around*|(|\<bbb-C\>|)>> is the set
  of complex <math|2\<times\>1> vectors. The definition of the Berry
  curvature for a band <math|m> follows:

  <\equation>
    \<Omega\><rsub|m><around*|(|<math-bf|k>|)>=\<partial\><rsub|k<rsub|x>>A<rsub|m,x><around*|(|<math-bf|k>|)>-\<partial\><rsub|k<rsub|y>>A<rsub|m,y><around*|(|<math-bf|k>|)>\<in\>\<bbb-C\><label|berry_curvature_eq>
  </equation>

  This can be rewritten as:

  <\equation>
    \<Omega\><rsub|m><around*|(|<math-bf|k>|)>=<around*|\<nobracket\>|<around*|\<langle\>|<frac|\<partial\>
    \<psi\><rsub|m><around*|(|<math-bf|k>|)>|\<partial\>k<rsub|x>>|\|><frac|\<partial\>
    \<psi\><rsub|m><around*|(|<math-bf|k>|)>|\<partial\>k<rsub|y>>|\<rangle\>>-<around*|\<nobracket\>|<around*|\<langle\>|<frac|\<partial\>
    \<psi\><rsub|m><around*|(|<math-bf|k>|)>|\<partial\>k<rsub|y>>|\|><frac|\<partial\>
    \<psi\><rsub|m><around*|(|<math-bf|k>|)>|\<partial\>k<rsub|x>>|\<rangle\>>
  </equation>

  Finally by integrating we get the Chern number:

  <\equation>
    C<rsub|m>=<frac|1|2\<pi\>i><big|int><rsub|BZ>\<Omega\><rsub|m><around*|(|<math-bf|k>|)>\<mathd\><rsup|2><math-bf|k>\<in\>\<bbb-Z\>
  </equation>

  I won't prove here why the Chern number is an integer, but I'll offer an
  intuitive idea: According to Stokes' theorem, integrating the Berry
  curvature over the Brillouin zone is equivalent to integrating the Berry
  connection along its boundary. If the latter doesn't possess any
  singularities, the integration over the boundary will be zero. However, if
  there is a singularity, we can trace a contour that excludes this
  singularity. The infinitesimal circle will contribute <math|\<pm\>2\<pi\>>
  to the integral because the connection exhibits a ``whirlpool'' around the
  singularity, and the sign depends on the direction of this whirlpool.
  [Dalibard]

  We can define the Chern number of the gap as the sum of the Chern numbers
  of the bands that are below the gap:

  <\equation>
    C=<big|sum><rsub|m\<less\>m<rsub|gap>>C<rsub|m>
  </equation>

  This definition can be used for computing the Chern number when analytical
  expressions of the eigenvectors are known. However it is better to avoid
  using it when we have to find the eigenvectors numerically for each
  discrete point <math|<math-bf|k>> of the Brillouin zone. Indeed, algorithms
  use to numerically find the eigenvectors are gauge-dependant and thus, as
  the Berry connection is also gauge-dependant, it would require an
  infinetely discretized Brillouin zone to have a correct result. To
  circumvent this problem, we can use a method due to [Fukui] that avoid the
  use of any gauge-dependant quantities.

  We consider a discretization of the Brillouin zone into
  <math|N<rsub|x>N<rsub|y>> points, note that the Brillouin zone we consider
  excludes certain points on the border as they are already counted by
  symmetry on other sides of the hexagon. Such a discretization is
  illustrated in <reference|discretized_bz>.

  For a point <math|<math-bf|k><rsub|i>> laying in the discretized Brillouin
  zone, one can introduce the following quantity:

  <\equation>
    U<rsub|\<mu\>><rsup|m><around*|(|<math-bf|k>|)>=<frac|<around*|\<langle\>|\<psi\><rsub|m><around*|(|<math-bf|k><rsub|i>|)><around*|\|||\<nobracket\>>\<psi\><rsub|m><around*|(|<math-bf|k><rsub|i>+<math-bf|u><rsub|\<mu\>>|)>|\<rangle\>>|<around*|\||<around*|\<langle\>|\<psi\><rsub|m><around*|(|<math-bf|k><rsub|i>|)><around*|\|||\<nobracket\>>\<psi\><rsub|m><around*|(|<math-bf|k><rsub|i>+<math-bf|u><rsub|\<mu\>>|)>|\<rangle\>>|\|>>,\<space\>\<mu\>=x,y
  </equation>

  where <math|<math-bf|u><rsub|\<mu\>>> is the unit vector connecting two
  plaquettes. Note that for this Brillouin zone
  <math|u<rsub|y>/u<rsub|x>=<sqrt|3>>, this is done to ensure that the points
  <math|<math-bf|k><rsub|i>> on the boundary are included in the calculation.
  Indeed, it is these regions that concentrate the lattice field strength the
  most. The lattice field strength is a discretized equivalent of the Berry
  curvature and is defined by:

  <\equation>
    F<rsup|m><around*|(|<math-bf|k><rsub|i>|)>=log<around*|(|U<rsub|x><rsup|m><around*|(|<math-bf|k><rsub|i>|)>U<rsub|y><rsup|m><around*|(|<math-bf|k><rsub|i>|)>U<rsub|x><rsup|m><around*|(|<math-bf|k><rsub|i>+<math-bf|u><rsub|y>|)><rsup|-1>U<rsub|y><rsup|m><around*|(|<math-bf|k><rsub|i>+<math-bf|u><rsub|x>|)><rsup|-1>|)>
  </equation>

  This quantity is plotted in <reference|berry_curvature> and is sufficient
  to demonstrate the topological behaviour of our system, however to have a
  global quantity that is a number instead of a color map, we define the
  Chern number of the band <math|m> by:

  <\equation>
    C<rsub|m>=<frac|1|2\<pi\>i><big|sum><rsub|i>F<rsup|m><around*|(|<math-bf|k><rsub|i>|)>
  </equation>

  It can be demonstrated that the result obtains here is equivalent to the
  Chern number computed by direct integration, however here the quantity
  <math|F<rsup|m><around*|(|<math-bf|k><rsub|i>|)>> is gauge-invariant and so
  is the Chern number. This allows us to numerically compute the Chern number
  without worrying about the gauge choosed by the diagonalization algorithm.

  <\big-figure|<image|figs/discretized_bz.pdf|0.501par|||>>
    <label|discretized_bz>Discretized Brillouin zone used to compute Berry
    curvature and thus Chern number using the method recommanded by [Fukui et
    al]. The red points are not considered in the sum giving the chern number
    as they are repetition of other points in the Brillouin zone. Vectors
    <math|<math-bf|u><rsub|x>> and <math|<math-bf|u><rsub|y>> are defined to
    ensure \ <math|<around*|\<\|\|\>|<math-bf|u><rsub|x>|\<\|\|\>>/><math|<around*|\<\|\|\>|<math-bf|u><rsub|y>|\<\|\|\>>=1/<sqrt|3>>
    in the goal of having discretized point to fall exactly on the border of
    the Brillouin zone.
  </big-figure>

  <\big-figure|<image|figs/t_2=0 M=0.3.pdf|0.5par|||> <image|figs/t_2=0.2
  M=1.pdf|0.5par|||>>
    Berry curvature in the trivial (left) and topological case (right). In
    the trivial case Berry cuvature compensates itself at points <math|K> and
    <math|K<rprime|'>>, however in the topologcial case it becomes zero for
    <math|K<rprime|'>> points thus making the Chern number non
    zero.<label|berry_curvature>
  </big-figure>

  It appears from the definition of Berry Curvature given in
  <reference|berry_curvature_eq> that when time reversal symmetry is broken
  we do not have anymore that <math|\<Omega\><around*|(|-<math-bf|k>|)>=-\<Omega\><around*|(|<math-bf|k>|)>>
  because we loose the property <math|\<psi\><around*|(|-<math-bf|k>|)>=-\<psi\><around*|(|<math-bf|k>|)>>
  and thus, the Chern number might be non zero. This is clearly visible in
  <reference|berry_curvature>. It is, however, very important to highlight
  than breaking time reversal symmetry is not always enough to make the
  system topological and that the Haldane model is a toy model where it
  happens to work, but as we shall see later, there are more complex system
  where it does not work.

  <subsection|Real space>

  After studying Haldane's model in Brillouin space and on a nanoribbon, we
  present here the density of states of this model in real space, it is
  derived from the eigen equation

  <\equation*>
    H<rsub|Haldane>\<psi\>=\<Lambda\>\<psi\>
  </equation*>

  by taking the real part of <math|\<Lambda\>>. We obtain the density of
  states that we can compare to the band diagram, this is shown in
  <reference|dos_ipr>. One option that has not been discussed yet is to
  consider a sample with open or periodic bounadry conditions (we will use
  the two acronyms \POBC\Q and \PPBC\Q later in this text). By plotting the
  two density of states we notice that the case of OBC still has states
  inside the gap. To demonstrate that these states localize on the border we
  introduce a new quantity: the inverse participation ratio (IPR), for a
  given mode <math|m>:

  <\equation*>
    IPR<around*|(|m|)>=<frac|<big|sum><rsub|i=1><rsup|N><around*|\||\<psi\><rsub|m><rsup|i>|\|><rsup|4>|<around*|(|<big|sum><rsub|i=1><rsup|N><around*|\||\<psi\><rsub|m><rsup|i>|\|><rsup|2>|)><rsup|2>>
  </equation*>

  where <math|<around*|\||\<psi\><rsub|m><rsup|i>|\|><rsup|2>> is the squared
  norm of the <math|i>-th component of the mode <math|\<psi\><rsub|m>>. This
  quantity can also be mapped onto the lattice as each component of the
  vector <math|\<psi\><rsub|m>> represents the participation of the site
  <math|i.> Then we can obtain a map showing where the state is localized,
  this is done in <reference|dos_ipr> for a state that exhibits localization
  on the edges of the sample. One can notice that the IPR will take values in
  the range of <math|1/N> (meaning that the state is completely delocalized)
  to <math|1> (the states is localized on one unique site). The intuition
  behind the IPR is that it is strong when the probability for the wave to
  come back is high.\ 

  <\big-figure|<image|figs/graphene_type_border.pdf|0.501par|||>>
    This figure highlights the distinct edge structures found in a honeycomb
    lattice. These edge configurations play a crucial role in the appearance
    of edges states.
  </big-figure>

  <\big-figure|<image|figs/dos.pdf|0.45par|||>
  <image|figs/imap_0.0.pdf|0.55par|||>>
    Density of states of the Haldane's model with <math|t<rsub|2>=0.2> for
    two systems with open (OBC) and periodic (PBC) boundary conditions. Note
    that in the OBC case, states are present within the gap. These states
    localize on the edges of the sample as shown by the IPR map on the
    right.<label|dos_ipr>
  </big-figure>

  \;

  <subsection|Bott index><label|bott_index>

  The Bott index can be seen as a topological invariant for a system in real
  space. It has been shown to be equivalent to Chern number in the
  thermodynamical limit [Toniolo]. We first examine some results regarding
  Bott index of almost commuting matrices that can help understand how to
  efficiently compute such index. The reader only interested in the
  definition of Bott index for physics can skip the following and directly go
  to <reference|bott_physicist>

  <subsubsection|Bott index of two unitary matrices><label|bott_unitary>

  Independantly of physics, Bott index is mathematically defined for two
  unitary matrices of size <math|n>: <math|U\<in\>\<cal-M\><rsub|n><around*|(|\<bbb-C\>|)>>
  and <math|V\<in\>\<cal-M\><rsub|n><around*|(|\<bbb-C\>|)>> as

  <\equation>
    Bott<around*|(|U,V|)>=<frac|1|2\<pi\>i>Tr
    log<around*|(|U*V*U<rsup|-1>V*<rsup|-1>|)>
  </equation>

  One will notice that to ensure the log to be well defined, an assumption
  needs to be made on the spectrum:

  <\equation>
    <around*|{|-1|}>\<notin\>\<sigma\><around*|(|U*V*U<rsup|-1>V*<rsup|-1>|)>\<Leftrightarrow\><around*|\<\|\|\>|<around*|[|U,V|]>|\<\|\|\>>\<less\>2
  </equation>

  where <math|<around*|\<\|\|\>|\<cdummy\>|\<\|\|\>>> is a norm for a matrix
  in <math|\<cal-M\><rsub|n><around*|(|\<bbb-C\>|)>>. By this inequality we
  state that \ <math|U*V*U<rsup|-1>V*<rsup|-1>> has no eigenvalues equal to
  <math|-1> if, and only if, the norm of the commutator of <math|U> and
  <math|V> is strictly inferior to <math|2>. This equivalence is simply
  coming for the equality:

  <\equation>
    <around*|\<\|\|\>|U*V*U<rsup|-1>V*<rsup|-1>-<with|font|Bbb|1><rsub|n>|\<\|\|\>>=<around*|\<\|\|\>|U*V*U<rsup|-1>V*<rsup|-1>-V*U*U<rsup|-1>*V<rsup|-1>|\<\|\|\>>=<around*|\<\|\|\>|<around*|[|U,V|]>|\<\|\|\>>
  </equation>

  The first statement we can make out of this definition is to notice that
  the Bott index of two unitary matrices is integer. Indeed eigenvalues of
  unitary matrices have modulus 1 and <math|-1> can't be an eigenvalues so we
  can write:

  <\equation>
    \<sigma\><around*|(|U*V*U<rsup|-1>V*<rsup|-1>|)>=<around*|{|exp<around*|(|i\<theta\><rsub|j>|)>,1\<leqslant\>j\<leqslant\>n,-\<pi\>\<less\>\<theta\><rsub|j>\<less\>\<pi\>|}>
  </equation>

  And because <math|det<around*|(|U*V*U<rsup|-1>V*<rsup|-1>|)>=det<around*|(|U|)>det<around*|(|V|)>det<around*|(|U<rsup|-1>|)>det<around*|(|V<rsup|-1>|)>=1>
  we have:

  <\equation>
    1=<big|prod><rsub|j=1><rsup|n>exp<around*|(|i\<theta\><rsub|j>|)>=exp<around*|(|i<big|sum><rsub|j=1><rsup|n>\<theta\><rsub|j>|)>\<Rightarrow\><frac|1|2\<pi\>><big|sum><rsub|j=1><rsup|n>\<theta\><rsub|j>\<in\>\<bbb-Z\>
  </equation>

  <subsubsection|Bott index of two invertible matrix><label|bott_invertible>

  Bott index can more generally be defined for two invertible matrices of
  size <math|n> <math|><math|S\<in\>GL<rsub|n><around*|(|\<bbb-C\>|)>> and
  <math|T\<in\>GL<rsub|n><around*|(|\<bbb-C\>|)>>, but then we loose the
  easily checkable condition <math|<around*|\<\|\|\>|<around*|[|S,T|]>|\<\|\|\>>\<less\>2>
  and we instead have to check that

  <\equation>
    \<sigma\><around*|(|S*T*S<rsup|-1>T*<rsup|-1>|)>\<cap\>\<bbb-R\><rsup|->=\<emptyset\>
  </equation>

  which is not as convenient. For two invertible matrices, the Bott index is
  also an integer. The proof for this fact is analogous to the one provided
  in the preceding section. Let <math|\<sigma\><around*|(|S*T*S<rsup|-1>T*<rsup|-1>|)>=<around*|{|\<lambda\><rsub|j>,1\<leqslant\>j\<leqslant\>n|}>=<around*|{|<around*|\||\<lambda\><rsub|j>|\|>\<mathe\>xp<around*|(|i\<theta\><rsub|j>|)>,1\<leqslant\>j\<leqslant\>n|}>>
  and then we can write:

  <\equation>
    1=<big|prod><rsub|j=1><rsup|n>\<lambda\><rsub|j>=<around*|\||<big|prod><rsub|j=1><rsup|n>\<lambda\><rsub|j>|\|>=<big|prod><rsub|j=1><rsup|n><around*|\||\<lambda\><rsub|j>|\|>
  </equation>

  Giving:

  <\equation>
    1=<big|prod><rsub|j=1><rsup|n><around*|\||\<lambda\><rsub|j>|\|>\<mathe\>xp<around*|(|i\<theta\><rsub|j>|)>=<big|prod><rsub|j=1><rsup|n>\<mathe\>xp<around*|(|i\<theta\><rsub|j>|)>
  </equation>

  And we can conlude similarly as in <reference|bott_unitary>.

  <subsubsection|Vanishing of Bott index>

  It is clear that if <math|U> and <math|V> are commuting, then the Bott
  index will be zero. A less obvious result is given for an upper bound on
  the trace norm: if <math|<around*|\<\|\|\>|<around*|[|U,V|]>|\<\|\|\>>\<less\>2>
  and <math|><math|<around*|\<\|\|\>|<around*|[|U,V|]>|\<\|\|\>><rsub|1>\<less\>4>.
  We previously proved that the Bott index has to be an integer, thus if its
  modulus is smaller than one, it is exactly zero. So from the definition of
  the Bott index we can write:

  <\equation>
    <around*|\||Bott<around*|(|U,V|)>|\|>\<leqslant\><frac|1|2\<pi\>><around*|\<\|\|\>|log<around*|(|U*V*U<rsup|-1>V*<rsup|-1>|)>|\<\|\|\>><rsub|1>=<frac|1|2\<pi\>><big|sum><rsub|j=1><rsup|n><around*|\||\<theta\><rsub|j>|\|>
  </equation>

  Using the inequality <math|\<forall\>x\<in\><around*|[|-\<pi\>;\<pi\>|[>,<around*|\||x|\|>\<leqslant\><frac|\<pi\>|2><around*|\||\<mathe\><rsup|i*x>-1|\|>>
  we get:

  <\equation>
    <frac|1|2\<pi\>><big|sum><rsub|j=1><rsup|n><around*|\||\<theta\><rsub|j>|\|>\<leqslant\><frac|1|4><big|sum><rsub|j=1><rsup|n><around*|\||\<mathe\><rsup|i*\<theta\><rsub|j>>-1|\|>=<frac|1|4><around*|\<\|\|\>|U*V*U<rsup|-1>*V<rsup|-1>-<with|font|Bbb|1><rsub|n>|\<\|\|\>><rsub|1>\<leqslant\><frac|1|4><around*|\<\|\|\>|<around*|[|U,V|]>|\<\|\|\>><rsub|1>
  </equation>

  and thus it comes that <math|<around*|\||Bott<around*|(|U,V|)>|\|>\<less\>1>.

  <subsubsection|Bott index for the physicist><label|bott_physicist>

  Bott index for physicas has been introduces by Loring and Hasting and later
  improved [Loring 2010, Loring 2019, Toniolo], what we present here
  summarizes their work.\ 

  We consider a Hamiltonian <math|H\<in\>\<cal-M\><rsub|n><around*|(|\<bbb-C\>|)>>
  which models an insulator on a lattice wrapped around a torus. That lattice
  can be seen as a rectangle with periodic boundary conditions (PBC) of size
  <math|L<rsub|x>\<times\>L<rsub|y>> can be denoted as a set of points\ 

  <\equation>
    \<Lambda\>=<around*|{|<around*|(|x<rsub|i>,y<rsub|j>|)>,1\<leqslant\>i\<leqslant\>N<rsub|x>,1\<leqslant\>j\<leqslant\>N<rsub|y>|}>
  </equation>

  \ This Hamiltonian is required to fill the following conditions

  <\itemize>
    <item>short-ranged; meaning that interaction between sites <math|i> and
    <math|j> should not take place if the distance between these two sites is
    greater t<math|>han a value <math|R> such that <math|R\<ll\>L<rsub|x>>
    and <math|R\<ll\>L<rsub|y>> with <math|L<rsub|x>> and <math|L<rsub|y>>
    length of the sides of the rectangle containing the sample.

    <item>bounded; <math|<around*|\<\|\|\>|H|\<\|\|\>>> has to be bounded by
    a value independant from the system's size

    <item>gapped; there is an energy gap <math|\<Delta\>E> in the spectrum
    gap unaltered when increasing the size of the system. Note that this gap
    can be a mobility gap, i.e. a gap with states that are localized only on
    the edges of <math|\<Lambda\>>. We define the Fermi level
    <math|E<rsub|F>> as an energy within that gap.

    <item><math|L<rsub|x>>, <math|L<rsub|y>>, <math|R>,
    <math|<around*|\<\|\|\>|H|\<\|\|\>>>, and <math|\<Delta\>E> have to
    verify <math|<frac|R<around*|\<\|\|\>|H|\<\|\|\>>|L<rsub|x>\<Delta\>E>\<ll\>1>
    and <math|<frac|R<around*|\<\|\|\>|H|\<\|\|\>>|L<rsub|y>\<Delta\>E>\<ll\>1>
  </itemize>

  We will see in this work that some liberty can be taken with this quite
  restrictive definition. But we will for now assume that these hypotheses
  are true.\ 

  We now consider that we have obtain an eigenbasis of <math|H>:
  <math|<around*|(|<math-bf|q><rsub|1>,\<ldots\>,<math-bf|q><rsub|n>|)>> and
  we split this basis in two distinct basis, one
  <math|<around*|(|<math-bf|q><rsub|1>,\<ldots\>,<math-bf|q><rsub|m>|)>> that
  generates the subspace below the Fermi level <math|E<rsub|F>> and
  \ <math|<around*|(|<math-bf|q><rsub|m+1>,\<ldots\>,<math-bf|q><rsub|n>|)>>
  that generates the subspace above. We define the <math|m\<times\>n> matrix\ 

  <\equation>
    W=<around*|[|<math-bf|q><rsub|1>\<ldots\><math-bf|q><rsub|m>|]>
  </equation>

  Note that in the case of a disordered system, there might be no apparent
  gap, the value of <math|E<rsub|F>> used in this case should be the one used
  in the ordered case.\ 

  We can now introduce position operators on the torus:

  <\equation>
    X=diag<around*|(|x<rsub|1>,\<ldots\>,x<rsub|n>|)>
    <infix-and>Y=diag<around*|(|y<rsub|1>,\<ldots\>,y<rsub|n>|)>
  </equation>

  From wich we can introduce:

  <\equation>
    U=W<rsup|\<dagger\>>exp<around*|(|i<frac|2\<pi\>|L<rsub|x>>X|)>W
  </equation>

  <\equation>
    V=W<rsup|\<dagger\>>exp<around*|(|i<frac|2\<pi\>|L<rsub|y>>Y|)>W
  </equation>

  Note that the restriction that the lattice has to be projected on a torus
  to have a clear definition of what <math|L<rsub|x>> and <math|L<rsub|y>>
  can make other sample shapes fall outside this defini<math|>tion.

  Finally we can define the Bott index as:

  <\equation>
    C<rsub|B>=<frac|1|2\<pi\>i>Im<around*|(|tr<around*|(|log<around*|(|V*U*V<rsup|-1>U<rsup|-1>|)>|)>|)>
  </equation>

  Or alternatively, because \ <math|U> and <math|V> are almost unitary:
  <math|U*U<rsup|\<dagger\>>\<approx\>V*V<rsup|\<dagger\>>\<approx\>I>

  <\equation>
    C<rsub|B>=<frac|1|2\<pi\>i>Im<around*|(|tr<around*|(|log<around*|(|V*U*V<rsup|\<dagger\>>U<rsup|\<dagger\>>|)>|)>|)><label|bott-index-final>
  </equation>

  This means in a more formal langage that under a certain norm defined for
  matrix <math|<around*|\<\|\|\>|\<cdummy\>|\<\|\|\>>>,
  <math|<around*|\<\|\|\>|U*U<rsup|\<dagger\>>-I|\<\|\|\>>=<around*|\<\|\|\>|V*V<rsup|\<dagger\>>-I|\<\|\|\>>=\<cal-O\><around*|(|1/L<rsub|x>L<rsub|y>|)>>,
  thus by increasing the size of the sample, we make the two matrices more
  and more unitary. See [Toniolo] for proof of this last statement.

  To compute <reference|bott-index-final>, one should compute the eigenvalues
  of <math|V*U*V<rsup|\<dagger\>>U<rsup|\<dagger\>>> and sum their
  logarithms. The result will be an integer considering the result
  demonstrated in <reference|bott_invertible> and the fact that <math|U> and
  <math|V> are invertible by construction.

  We draw attention onto the fact that there exists another definition of the
  Bott index based on a \ the projector onto the eigenstates below the
  gap<math| >instead of <math|W>. Actually this is even the historical
  definition:

  <\equation>
    P=<big|sum><rsub|j\<leqslant\>m><around*|\||<math-bf|q><rsub|j>|\<rangle\>><around*|\<langle\>||\<nobracket\>><math-bf|q><rsub|j><around*|\|||\<nobracket\>>
    <infix-and> P<rsup|\<perp\>>=<big|sum><rsub|j\<gtr\>m><around*|\||<math-bf|q><rsub|j>|\<rangle\>><around*|\<langle\>||\<nobracket\>><math-bf|q><rsub|j><around*|\|||\<nobracket\>>
    </equation>

  and from wich we can define:

  <\eqnarray*>
    <tformat|<table|<row|<cell|U>|<cell|=>|<cell|P*\<mathe\><rsup|2i\<pi\>X/L<rsub|x>>P+P<rsup|\<perp\>><eq-number>>>|<row|<cell|V>|<cell|=>|<cell|P*\<mathe\><rsup|2i\<pi\>Y/L<rsub|y>>P+P<rsup|\<perp\>><eq-number>>>>>
  </eqnarray*>

  And then we continue like previously. One important difference lays in the
  size of the matrices <math|U> and <math|V> which are <math|m\<times\>m> in
  the first case and <math|n\<times\>n> in the second case. We highly
  recomend to use the first method, except if you are in an exotic situation
  where you only have access to the projector and not to the orthonormal
  basis of the space.

  <subsubsection|Bott Index on Haldane's model>

  <\big-figure|<image|figs/phase_diagram_chern.pdf|0.501par|||><image|figs/phase_diagram_bott.pdf|0.50par|||>>
    Chern number and Bott index computed for various values of
    <math|t<rsub|2>> and <math|M>, black line is
    <math|t<rsub|2>=M/<around*|(|3<sqrt|3>|)>> and shows the transition
    between the topological (<math|C=1> or <math|C<rsub|<text|B>>=1>) and
    trivial (<math|C=0> or <math|C<rsub|<text|B>>=0>) phase. These two
    figures demonstrate the equivalence between the two topological
    invariants.
  </big-figure>

  <section|Kane-Mele model>

  In this section we introduce the Kane-Mele's model which, in addition to
  the Haldane's model, takes into accound the spin of the sites. This model
  is used only to demonstrate the relevance of two new topological
  invariants: the spin Chern number and the spin Bott index.

  <subsection|Band structure>

  On a graphene lattice, the Hamiltonian of the Kane-Mele model reads as:

  <\equation>
    H<rsub|KM>=t<rsub|><big|sum><rsub|<around*|\<langle\>|j
    k|\<rangle\>>>c<rsub|j><rsup|\<dagger\>>c<rsub|k>+i\<lambda\><rsub|SO><big|sum><rsub|<around*|\<langle\>|<around*|\<langle\>|j
    k|\<rangle\>>|\<rangle\>>>\<nu\><rsub|j
    k>c<rsub|j><rsup|\<dagger\>>\<sigma\><rsub|z>c<rsub|k>+i\<lambda\><rsub|<with|font-family|rm|><text|R>><big|sum><rsub|<around*|\<langle\>|j
    k|\<rangle\>>><around*|(|<math-bf|s>\<times\><math-bf|d>|)><rsub|z>c<rsub|j><rsup|\<dagger\>>c<rsub|k>+\<lambda\><rsub|v><big|sum><rsub|j>\<varepsilon\><rsub|j>c<rsub|j><rsup|\<dagger\>>c<rsub|j>
  </equation>

  where the two first term were already present in the Haldane model as
  nearest neighbour and next nearest neighbour hoping terms excepts that now
  this last term takes into accound the spin of each sites. The third term is
  nearest neighbour Rashba coupling it will be consider null in our study as
  they are not relevant to triggers simple topological behaviour for this
  model. The fourth term is on-site energy, before we used the parameter
  <math|M> to write it.

  <\big-figure|<image|figs/Kane-Mele-1_bs.pdf|0.5par|||>
  <image|figs/Kane-Mele-2_bs.pdf|0.5par|||>>
    Band structure for the Kane-Mele model. Four bands are present as each
    sites <math|A> and <math|B> have to be considered either for spin up or
    for spin down.\ 
  </big-figure>

  <subsection|Spin Chern number><label|spin_chern_number>

  In this model, time reversal symmetry is preserved and thus, the Chern
  number will be zero. But as we have seen before, there are still
  topological effects that occur in this model. Another invariant that can we
  can use to spot topology in these situations is the spin Chern number. To
  compute the spin Chern number we build the projectors
  <math|<wide|P|^><rsub|\<pm\>><around*|(|<math-bf|k>|)>> for each
  <math|<wide|H|^><around*|(|<math-bf|k>|)>> of the Brillouin zone by summing
  all the states below or above the gap.

  <\equation*>
    <wide|P|^><rsub|+><around*|(|<math-bf|k>|)>=<big|sum><rsub|E<rsub|j>\<gtr\>gap><around*|\||\<psi\><rsub|j><around*|(|<math-bf|k>|)>|\<rangle\>><around*|\<langle\>||\<nobracket\>>\<psi\><rsub|j><around*|(|<math-bf|k>|)><around*|\|||\<nobracket\>>
  </equation*>

  \ From there we introduce

  <\equation>
    <wide|H|^><rsub|\<pm\>><around*|(|<math-bf|k>|)>=<wide|P|^><rsub|\<pm\>><around*|(|<math-bf|k>|)><wide|\<sigma\>|^><wide|P|^><rsub|\<pm\>><around*|(|<math-bf|k>|)>
  </equation>

  which will allow us to compute <math|C<rsub|\<pm\>>> by replacing
  <math|<wide|H|^>> in the original definition of the Chern number by
  <math|<wide|H|^><rsub|\<pm\>>> and <math|\<sigma\>> is the operator used to
  differentiate eigenstates associated to \Pspin-up\Q than those associated
  to \Pspin-down\Q:

  <\equation>
    \<sigma\>=<matrix|<tformat|<table|<row|<cell|<with|font|Bbb|1><rsub|2>>|<cell|0>>|<row|<cell|0>|<cell|-<with|font|Bbb|1><rsub|2>>>>>>
  </equation>

  where <math|<with|font|Bbb|1><rsub|2>> is the <math|2\<times\>2> identity
  matrix. The spin Chern number is defined by

  <\equation>
    C<rsub|<text|S>>=<frac|1|2><around*|(|C<rsub|+>-C<rsub|->|)>
  </equation>

  <subsection|Spin Bott index>

  [Huang,Liu, PRB 98]

  The same way we define the Bott index from the Chern number, we can define
  the spin Bott index (SBI) as the real space equivalent of the spin Chern
  number. We consider a Hamiltonian <math|H\<in\>\<cal-M\><rsub|2N><around*|(|\<bbb-C\>|)>>
  that describes a system of <math|N> sites with spin up and down. Once the
  eigenstates of <math|H> have been found, we build the projected spin
  operator

  <\equation>
    P<rsub|z>=P\<sigma\>P
  </equation>

  where <math|P> is the projector on the eigenstates below the gap and
  <math|\<sigma\>> is the version in real space of the operator defined in
  <reference|spin_chern_number>:

  <\equation>
    \<sigma\>=<matrix|<tformat|<table|<row|<cell|<with|font|Bbb|1><rsub|N>>|<cell|0>>|<row|<cell|0>|<cell|-<with|font|Bbb|1><rsub|N>>>>>>
  </equation>

  where <math|<with|font|Bbb|1><rsub|N>> is the <math|N\<times\>N> identity
  matrix. We then find the eigenstates of <math|P<rsub|z>> and differentiate
  them between those associated to the eigenvalue <math|-1> and <math|1>
  allowing us to compute respectively <math|C<rsub|<text|B>><rsup|->> and
  <math|C<rsub|<text|B>><rsup|+>> with the procedure described in
  <reference|bott_index>. Finally the SBI is defined by:

  <\equation>
    C<rsub|<text|SB>>=<frac|1|2><around*|(|C<rsub|<text|B>><rsup|+>-C<rsub|<text|B>><rsup|->|)>
  </equation>

  We compute the phase diagram in <reference|spin_index> to show the
  equivalence between the spin Chern number and the spin Bott index.

  <\big-figure|<image|figs/phase_diagram_spin_chern.pdf|0.5par|||>
  <image|figs/phase_diagram_spin_bott_6.pdf|0.5par|||>>
    <label|spin_index>Spin Chern number and Spin Bott index computed for
    Kane-Mele's Model. <math|N=216>
  </big-figure>

  <\equation*>
    \;
  </equation*>

  <subsection|Notation>

  In the following, Chern number will be denoted as <math|C> and spin Chern
  number as <math|C<rsub|<text|S>>>. To avoid confusion with the magnetic
  field <math|<math-bf|B>> or sites <math|B>, Bott index will be denoted as
  <math|C<rsub|<text|B>>> and spin Bott index as <math|C<rsub|<text|SB>>>.

  <\big-table|<tabular|<tformat|<cwith|1|-1|1|-1|cell-halign|c>|<cwith|1|-1|1|-1|cell-tborder|1ln>|<cwith|1|-1|1|-1|cell-bborder|1ln>|<cwith|1|-1|1|-1|cell-lborder|1ln>|<cwith|1|-1|1|-1|cell-rborder|1ln>|<table|<row|<cell|>|<cell|<math|C>>|<cell|<math|C<rsub|<text|B>>>>|<cell|<math|C<rsub|<text|S>>>>|<cell|<math|C<rsub|<text|SB>>>>>|<row|<cell|Graphene>|<cell|0>|<cell|0>|<cell|0>|<cell|0>>|<row|<cell|Haldane>|<cell|1>|<cell|1>|<cell|0>|<cell|0>>|<row|<cell|Kane-Mele>|<cell|0>|<cell|0>|<cell|1>|<cell|1>>>>>>
    \;
  </big-table>

  <section*|Bibliography>

  <with|color|red|[TO DO]>

  Jean Dalibard\ 

  Frederic Faure\ 

  Bart V T

  Sergey\ 

  Haldane

  Kane Mele

  Loring
</body>

<\initial>
  <\collection>
    <associate|page-medium|paper>
  </collection>
</initial>

<\references>
  <\collection>
    <associate|ab-nano|<tuple|33|?>>
    <associate|auto-1|<tuple|?|1>>
    <associate|auto-10|<tuple|4|6>>
    <associate|auto-11|<tuple|5|7>>
    <associate|auto-12|<tuple|6|8>>
    <associate|auto-13|<tuple|2.2|8>>
    <associate|auto-14|<tuple|7|9>>
    <associate|auto-15|<tuple|2.3|9>>
    <associate|auto-16|<tuple|8|11>>
    <associate|auto-17|<tuple|9|11>>
    <associate|auto-18|<tuple|2.4|11>>
    <associate|auto-19|<tuple|10|12>>
    <associate|auto-2|<tuple|1|1>>
    <associate|auto-20|<tuple|11|12>>
    <associate|auto-21|<tuple|2.5|13>>
    <associate|auto-22|<tuple|2.5.1|13>>
    <associate|auto-23|<tuple|2.5.2|13>>
    <associate|auto-24|<tuple|2.5.3|14>>
    <associate|auto-25|<tuple|2.5.4|14>>
    <associate|auto-26|<tuple|2.5.5|15>>
    <associate|auto-27|<tuple|12|15>>
    <associate|auto-28|<tuple|3|15>>
    <associate|auto-29|<tuple|3.1|16>>
    <associate|auto-3|<tuple|1.1|1>>
    <associate|auto-30|<tuple|13|16>>
    <associate|auto-31|<tuple|3.2|16>>
    <associate|auto-32|<tuple|3.3|16>>
    <associate|auto-33|<tuple|14|17>>
    <associate|auto-34|<tuple|3.4|17>>
    <associate|auto-35|<tuple|1|17>>
    <associate|auto-36|<tuple|1|17>>
    <associate|auto-4|<tuple|1|2>>
    <associate|auto-5|<tuple|2|4>>
    <associate|auto-6|<tuple|1.2|4>>
    <associate|auto-7|<tuple|3|4>>
    <associate|auto-8|<tuple|2|6>>
    <associate|auto-9|<tuple|2.1|6>>
    <associate|berry_curvature|<tuple|9|11>>
    <associate|berry_curvature_eq|<tuple|37|9>>
    <associate|bott-index-final|<tuple|60|?>>
    <associate|bott_index|<tuple|2.5|13>>
    <associate|bott_invertible|<tuple|2.5.2|13>>
    <associate|bott_physicist|<tuple|2.5.4|14>>
    <associate|bott_unitary|<tuple|2.5.1|13>>
    <associate|discretized_bz|<tuple|8|11>>
    <associate|dos_ipr|<tuple|11|12>>
    <associate|ea-nano|<tuple|32|?>>
    <associate|eb-nano|<tuple|33|?>>
    <associate|eqA|<tuple|8|3>>
    <associate|eqB|<tuple|9|3>>
    <associate|eqE2|<tuple|11|3>>
    <associate|fig_graphene_bs|<tuple|1|2>>
    <associate|footnote-1|<tuple|1|3>>
    <associate|footnr-1|<tuple|1|3>>
    <associate|graphene-nanoribbon|<tuple|1.2|?>>
    <associate|graphene_bs|<tuple|2|4>>
    <associate|h-graphene|<tuple|2|?>>
    <associate|haldane_bs|<tuple|5|7>>
    <associate|haldane_nanoribbon|<tuple|4|6>>
    <associate|nanoribbon_schema|<tuple|3|4>>
    <associate|nanoribbon_simu|<tuple|7|9>>
    <associate|nitrure-bore|<tuple|16|?>>
    <associate|nnschro|<tuple|4|2>>
    <associate|schrodinger|<tuple|3|?>>
    <associate|spin_chern_number|<tuple|3.2|16>>
    <associate|spin_index|<tuple|14|17>>
    <associate|weight_a|<tuple|6|8>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|figure>
      <tuple|normal|<\surround|<hidden-binding|<tuple>|1>|>
        (Left) Graphene can be seen as a superposition of two triangular
        sublattices <with|mode|<quote|math>|A> (blue disks) and
        <with|mode|<quote|math>|B> (red disks).
        <with|mode|<quote|math>|<rigid|<with|mode|<quote|text>|<with|font-family|<quote|rm>|font-series|<quote|bold>|font-shape|<quote|right>|a>>><rsub|1>,<rigid|<with|mode|<quote|text>|<with|font-family|<quote|rm>|font-series|<quote|bold>|font-shape|<quote|right>|a>>><rsub|2>>
        and <with|mode|<quote|math>|<rigid|<with|mode|<quote|text>|<with|font-family|<quote|rm>|font-series|<quote|bold>|font-shape|<quote|right>|a>>><rsub|3>>
        are basis vector of the lattice. <with|mode|<quote|math>|a> is the
        unique parameter of this lattice. (Right) represents the first
        Brillouin zone with different paths: the blue one followed to
        construct the band diagram shown in <reference|haldane_bs>.
      </surround>|<pageref|auto-4>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|2>|>
        \ Band structure of graphene for a bit more than the Brillouin zone,
        notice the so-called six \PDirac points\Q where the conduction band
        and the valence band are touching each other.
      </surround>|<pageref|auto-5>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|3>|>
        \;
      </surround>|<pageref|auto-7>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|4>|>
        Haldane's model rules for next nearest neghour coupling. If we are on
        sites <with|mode|<quote|math>|A> and the vector between two
        interacting sites is in <with|mode|<quote|math>|<around*|{|e<rsub|0>,e<rsub|1>,e<rsub|-1>|}>>
        (respectively <with|mode|<quote|math>|<around*|{|-e<rsub|0>,-e<rsub|1>,-e<rsub|-1>|}>>)
        then the strength of the interaction will be
        <with|mode|<quote|math>|t<rsub|2>> (respectively
        <with|mode|<quote|math>|<wide|t<rsub|2>|\<bar\>>>). On sites
        <with|mode|<quote|math>|B> the rule is the same but both
        <with|mode|<quote|math>|t2> and <with|mode|<quote|math>|<wide|t<rsub|2>|\<bar\>>>
        are multiplied by <with|mode|<quote|math>|-1>.
      </surround>|<pageref|auto-10>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|5>|>
        Zoom on a dirac point following a path in the Brillouin Zone, the
        dispersion relation is clearly linear (red line) for the graphene
        model. Blue and teal lines represents Haldane's model for
        <with|mode|<quote|math>|<around*|(|t=1,M=0.2|)>> and
        <with|mode|<quote|math>|<around*|(|t=1,t<rsub|2>=0.15|)>>, width of
        the gap is given by <with|mode|<quote|math>|2<around*|\||M-3<sqrt|3|>t<rsub|2>|\|>>.
      </surround>|<pageref|auto-11>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|6>|>
        First band of the Hamiltonian for the Haldane's model with a trivial
        gap on left and with time-reversal symmetry broken on right. The band
        is color-coded as a function of weight of the quasimodes on the
        atomic sublattice <with|mode|<quote|math>|A>:
        <with|mode|<quote|math>|W<rsup|A><rsub|1>>.
      </surround>|<pageref|auto-12>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|7>|>
        <with|color|<quote|red>|>Band structure of a Nanoribbon of graphene
        folowing a path defined by <with|mode|<quote|math>|<around*|{|<around*|(|k<rsub|x>,0|)>,k<rsub|x>\<in\><around*|[|0;2\<pi\>/<sqrt|3>|]>|}>>.
        On the left we added the parameter <with|mode|<quote|math>|M> that
        represents the onsite energy and on the right
        <with|mode|<quote|math>|t<rsub|2>> is a pure imaginary coefficient
        that make coupling between next nearest neighbours. Using a
        nanoribbon allows one to see the edge states that are present in the
        case <with|mode|<quote|math>|3<sqrt|2>t<rsub|2>\<gtr\>M>.
      </surround>|<pageref|auto-14>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|8>|>
        Discretized Brillouin zone used to compute Berry curvature and thus
        Chern number using the method recommanded by [Fukui et al]. The red
        points are not considered in the sum giving the chern number as they
        are repetition of other points in the Brillouin zone. Vectors
        <with|mode|<quote|math>|<rigid|<with|mode|<quote|text>|<with|font-family|<quote|rm>|font-series|<quote|bold>|font-shape|<quote|right>|u>>><rsub|x>>
        and <with|mode|<quote|math>|<rigid|<with|mode|<quote|text>|<with|font-family|<quote|rm>|font-series|<quote|bold>|font-shape|<quote|right>|u>>><rsub|y>>
        are defined to ensure \ <with|mode|<quote|math>|<around*|\<\|\|\>|<rigid|<with|mode|<quote|text>|<with|font-family|<quote|rm>|font-series|<quote|bold>|font-shape|<quote|right>|u>>><rsub|x>|\<\|\|\>>/><with|mode|<quote|math>|<around*|\<\|\|\>|<rigid|<with|mode|<quote|text>|<with|font-family|<quote|rm>|font-series|<quote|bold>|font-shape|<quote|right>|u>>><rsub|y>|\<\|\|\>>=1/<sqrt|3>>
        in the goal of having discretized point to fall exactly on the border
        of the Brillouin zone.
      </surround>|<pageref|auto-16>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|9>|>
        Berry curvature in the trivial (left) and topological case (right).
        In the trivial case Berry cuvature compensates itself at points
        <with|mode|<quote|math>|K> and <with|mode|<quote|math>|K<rprime|'>>,
        however in the topologcial case it becomes zero for
        <with|mode|<quote|math>|K<rprime|'>> points thus making the Chern
        number non zero.
      </surround>|<pageref|auto-17>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|10>|>
        This figure highlights the distinct edge structures found in a
        honeycomb lattice. These edge configurations play a crucial role in
        the appearance of edges states.
      </surround>|<pageref|auto-19>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|11>|>
        Density of states of the Haldane's model with
        <with|mode|<quote|math>|t<rsub|2>=0.2> for two systems with open
        (OBC) and periodic (PBC) boundary conditions. Note that in the OBC
        case, states are present within the gap. These states localize on the
        edges of the sample as shown by the IPR map on the right.
      </surround>|<pageref|auto-20>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|12>|>
        Chern number and Bott index computed for various values of
        <with|mode|<quote|math>|t<rsub|2>> and <with|mode|<quote|math>|M>,
        black line is <with|mode|<quote|math>|t<rsub|2>=M/<around*|(|3<sqrt|3>|)>>
        and shows the transition between the topological
        (<with|mode|<quote|math>|C=1> or <with|mode|<quote|math>|C<rsub|<with|mode|<quote|text>|B>>=1>)
        and trivial (<with|mode|<quote|math>|C=0> or
        <with|mode|<quote|math>|C<rsub|<with|mode|<quote|text>|B>>=0>) phase.
        These two figures demonstrate the equivalence between the two
        topological invariants.
      </surround>|<pageref|auto-27>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|13>|>
        Band structure for the Kane-Mele model. Four bands are present as
        each sites <with|mode|<quote|math>|A> and <with|mode|<quote|math>|B>
        have to be considered either for spin up or for spin down.\ 
      </surround>|<pageref|auto-30>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|14>|>
        Spin Chern number and Spin Bott index computed for Kane-Mele's Model.
        <with|mode|<quote|math>|N=216>
      </surround>|<pageref|auto-33>>
    </associate>
    <\associate|table>
      <tuple|normal|<\surround|<hidden-binding|<tuple>|1>|>
        \;
      </surround>|<pageref|auto-35>>
    </associate>
    <\associate|toc>
      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|R�sum�
      en Fran�ais> <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-1><vspace|0.5fn>

      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|1<space|2spc>Graphene>
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-2><vspace|0.5fn>

      <with|par-left|<quote|1tab>|1.1<space|2spc>Analysing Graphene's
      Brillouin zone <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-3>>

      <with|par-left|<quote|1tab>|1.2<space|2spc>Graphene nanoribbon
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-6>>

      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|2<space|2spc>Haldane's
      model> <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-8><vspace|0.5fn>

      <with|par-left|<quote|1tab>|2.1<space|2spc>Brillouin zone
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-9>>

      <with|par-left|<quote|1tab>|2.2<space|2spc>Nanoribbon
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-13>>

      <with|par-left|<quote|1tab>|2.3<space|2spc>Chern number
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-15>>

      <with|par-left|<quote|1tab>|2.4<space|2spc>Real space
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-18>>

      <with|par-left|<quote|1tab>|2.5<space|2spc>Bott index
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-21>>

      <with|par-left|<quote|2tab>|2.5.1<space|2spc>Bott index of two unitary
      matrices <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-22>>

      <with|par-left|<quote|2tab>|2.5.2<space|2spc>Bott index of two
      invertible matrix <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-23>>

      <with|par-left|<quote|2tab>|2.5.3<space|2spc>Vanishing of Bott index
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-24>>

      <with|par-left|<quote|2tab>|2.5.4<space|2spc>Bott index for the
      physicist <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-25>>

      <with|par-left|<quote|2tab>|2.5.5<space|2spc>Bott Index on Haldane's
      model <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-26>>

      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|3<space|2spc>Kane-Mele
      model> <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-28><vspace|0.5fn>

      <with|par-left|<quote|1tab>|3.1<space|2spc>Band structure
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-29>>

      <with|par-left|<quote|1tab>|3.2<space|2spc>Spin Chern number
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-31>>

      <with|par-left|<quote|1tab>|3.3<space|2spc>Spin Bott index
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-32>>

      <with|par-left|<quote|1tab>|3.4<space|2spc>Notation
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-34>>

      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|Bibliography>
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-36><vspace|0.5fn>
    </associate>
  </collection>
</auxiliary>